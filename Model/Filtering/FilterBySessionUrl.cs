﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Filtering.FilterBySessionUrl
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using System;

namespace HibernatingRhinos.Profiler.Client.Model.Filtering
{
  [Serializable]
  public class FilterBySessionUrl : AbstractFilter
  {
    private bool contains = true;
    private string url;

    public bool[] SupportedOperators
    {
      get
      {
        return new bool[2]
        {
          true,
          false
        };
      }
    }

    public override bool IsValid
    {
      get
      {
        return !string.IsNullOrEmpty(this.Url);
      }
    }

    public bool Contains
    {
      get
      {
        return this.contains;
      }
      set
      {
        this.contains = value;
      }
    }

    public string Url
    {
      get
      {
        return this.url;
      }
      set
      {
        this.url = value;
      }
    }

    public override string ToolTip
    {
      get
      {
        return ProfileExtensions.Translate(Profile.Current, string.Format("[[sessions]] with url contains: '{0}'.", (object) this.Url));
      }
    }

    public override bool IsValidSession(IFilterableSessionSnapshot snapshot)
    {
      if (string.IsNullOrEmpty(snapshot.Url))
        return !this.Contains;
      return snapshot.Url.IndexOf(this.Url, StringComparison.InvariantCultureIgnoreCase) != -1 == this.Contains;
    }
  }
}
