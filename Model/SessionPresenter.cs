﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.SessionPresenter
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Model.Filtering;
using HibernatingRhinos.Profiler.Client.Model.Reports;
using HibernatingRhinos.Profiler.Client.Model.Sessions;
using HibernatingRhinos.Profiler.Client.Model.Statements;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Model
{
  public class SessionPresenter : Presenter, ISupportFiltering
  {
    private readonly RecentStatementsModel recentStatements;
    private readonly FilterServiceModel filterService;
    private ISessionPresenterItem selectedSession;
    private int selectedTabIndex;
    private readonly ObservableCollection<SessionModel> sessions;

    public override string Name
    {
      get
      {
        return "[[sessions]]";
      }
    }

    public ObservableCollection<ISessionPresenterItem> Items { get; private set; }

    public ISessionPresenterItem SelectedSession
    {
      get
      {
        return this.selectedSession;
      }
      set
      {
        if (value == null)
          return;
        EnumerableExtension.ForEach<ISessionPresenterItem>((IEnumerable<ISessionPresenterItem>) this.Items, (Action<ISessionPresenterItem>) (x => x.Deactivate()));
        this.selectedSession = value;
        this.selectedSession.Activate();
      }
    }

    public int SelectedTabIndex
    {
      get
      {
        return this.selectedTabIndex;
      }
      set
      {
        this.selectedTabIndex = value;
      }
    }

    public bool FilteringSupported
    {
      get
      {
        return true;
      }
    }

    public SessionPresenter(ObservableCollection<SessionModel> sessions, RecentStatementsModel recentStatements, FilterServiceModel filterService)
    {
      this.recentStatements = recentStatements;
      this.filterService = filterService;
      this.sessions = sessions;
      this.RebuildItemsCollection();
      filterService.Filters.CollectionChanged += (NotifyCollectionChangedEventHandler) ((sender, args) => this.RebuildItemsCollection());
      this.SelectedSession = (ISessionPresenterItem) this.recentStatements;
      sessions.CollectionChanged += new NotifyCollectionChangedEventHandler(this.SessionsCollectionChanged);
    }

    private void RebuildItemsCollection()
    {
      this.Items = new ObservableCollection<ISessionPresenterItem>((IEnumerable<ISessionPresenterItem>) Enumerable.Where<SessionModel>(Enumerable.Where<SessionModel>((IEnumerable<SessionModel>) this.sessions, (Func<SessionModel, bool>) (x => this.filterService.FilterOpenSession((IFilterableSessionSnapshot) x))), (Func<SessionModel, bool>) (x =>
      {
        if (x.WasClosed)
          return this.filterService.FilterClosedSession((IFilterableSessionSnapshot) x);
        return true;
      })));
      foreach (SessionModel sessionModel in (Collection<ISessionPresenterItem>) this.Items)
      {
        if (!sessionModel.WasClosed)
          sessionModel.Closed += new Action<SessionModel>(this.SessionClosed);
      }
      this.Items.Insert(0, (ISessionPresenterItem) this.recentStatements);
    }

    private void SessionClosed(SessionModel session)
    {
      if (this.filterService.FilterClosedSession((IFilterableSessionSnapshot) session))
        return;
      this.Items.Remove((ISessionPresenterItem) session);
    }

    private void SessionsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (e.Action == NotifyCollectionChangedAction.Add)
      {
        foreach (SessionModel sessionModel in Enumerable.OfType<SessionModel>((IEnumerable) e.NewItems))
        {
          if (this.filterService.FilterOpenSession((IFilterableSessionSnapshot) sessionModel))
          {
            this.Items.Add((ISessionPresenterItem) sessionModel);
            sessionModel.Closed += new Action<SessionModel>(this.SessionClosed);
          }
        }
      }
      if (e.Action != NotifyCollectionChangedAction.Remove)
        return;
      foreach (ISessionPresenterItem sessionPresenterItem in Enumerable.OfType<SessionModel>((IEnumerable) e.OldItems))
        this.Items.Remove(sessionPresenterItem);
    }

    public void RemoveSelection()
    {
      if (!this.CanRemoveSelection())
        return;
      this.SelectedSession = this.SelectedSession.RemoveFrom((IList<ISessionPresenterItem>) this.Items) ?? (ISessionPresenterItem) this.recentStatements;
    }

    public bool CanRemoveSelection()
    {
      if (this.SelectedSession != null)
        return this.SelectedSession != this.recentStatements;
      return false;
    }

    public void SelectActions(IStatementModel statementModel)
    {
      this.SelectedTabIndex = 1;
      this.SelectedSession.StatementsModel.SelectedStatement = statementModel;
    }

    public override void Clear()
    {
      this.Items.Clear();
      this.recentStatements.Clear();
      this.Items.Add((ISessionPresenterItem) this.recentStatements);
      this.SelectedSession = (ISessionPresenterItem) this.recentStatements;
    }

    public void CallWhenFilteringSupportedChanged(Action action)
    {
    }

    public string ExportToJsonString()
    {
      foreach (SessionModel sessionModel in (Collection<SessionModel>) this.sessions)
      {
        Enumerable.First<SessionUsage>(Enumerable.OfType<SessionUsage>((IEnumerable) sessionModel.Views)).TimerTicked((object) this, EventArgs.Empty);
        sessionModel.SessionStatements.TimerTicked((object) this, EventArgs.Empty);
      }
      JArray jarray = new JArray((object) Enumerable.Select<SessionModel, JObject>((IEnumerable<SessionModel>) this.sessions, (Func<SessionModel, JObject>) (x => new JObject(new object[3]
      {
        (object) new JProperty("session", (object) ProfileExtensions.Translate(Profile.Current, x.Name)),
        (object) new JProperty("statementCount", (object) x.StatementCount),
        (object) new JProperty("queries", (object) new JArray((object) Enumerable.Select<IStatementModel, JObject>((IEnumerable<IStatementModel>) x.StatementsModel.Statements, (Func<IStatementModel, JObject>) (y => new JObject(new object[6]
        {
          (object) new JProperty("isCached", y.IsCached ? (object) true : (object) false),
          (object) new JProperty("isDdl", y.IsDDL ? (object) true : (object) false),
          (object) new JProperty("averageDuration", (object) y.Duration.Value),
          (object) new JProperty("formattedSql", (object) y.Text),
          (object) new JProperty("shortSql", (object) y.ShortText),
          (object) new JProperty("count", (object) SessionPresenter.FormatAsCommaSeparatedList((IEnumerable<int>) y.CountOfRows))
        })))))
      }))));
      StringWriter stringWriter = new StringWriter();
      JsonExtensions.WriteJson((JToken) jarray, (TextWriter) stringWriter);
      return stringWriter.GetStringBuilder().ToString();
    }

    private static string FormatAsCommaSeparatedList(IEnumerable<int> nums)
    {
      return string.Join(", ", Enumerable.ToArray<string>(Enumerable.Select<int, string>(nums, (Func<int, string>) (i => i.ToString()))));
    }

    public XElement Export()
    {
      foreach (SessionModel sessionModel in (Collection<SessionModel>) this.sessions)
      {
        Enumerable.First<SessionUsage>(Enumerable.OfType<SessionUsage>((IEnumerable) sessionModel.Views)).TimerTicked((object) this, EventArgs.Empty);
        sessionModel.SessionStatements.TimerTicked((object) this, EventArgs.Empty);
      }
      return new XElement(Report.Namespace + "sessions", (object) Enumerable.Select<SessionModel, XElement>((IEnumerable<SessionModel>) this.sessions, (Func<SessionModel, XElement>) (x => new XElement(Report.Namespace + "session", new object[3]
      {
        (object) new XAttribute((XName) "name", (object) ProfileExtensions.Translate(Profile.Current, x.Name)),
        (object) new XAttribute((XName) "statementCount", (object) x.StatementCount),
        (object) new XElement(Report.Namespace + "queries", (object) Enumerable.Select<IStatementModel, XElement>((IEnumerable<IStatementModel>) x.StatementsModel.Statements, (Func<IStatementModel, XElement>) (y => XmlExportExtensions.ToXml(y))))
      }))));
    }
  }
}
