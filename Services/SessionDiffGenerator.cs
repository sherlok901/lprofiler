﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Services.SessionDiffGenerator
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Statements;
using System.Collections.Generic;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Services
{
  public class SessionDiffGenerator
  {
    private readonly ISessionPresenterItem first;
    private readonly ISessionPresenterItem second;

    public SessionDiffGenerator(ISessionPresenterItem first, ISessionPresenterItem second)
    {
      this.first = first;
      this.second = second;
    }

    public ISessionDiff Generate()
    {
      SessionDiffGenerator.SessionDiff sessionDiff = new SessionDiffGenerator.SessionDiff()
      {
        FirstSessionName = this.first.Name,
        SecondSessionName = this.second.Name
      };
      foreach (IStatementModel statement1 in this.first.StatementsModel.Statements)
      {
        if (!statement1.IsTransaction)
        {
          bool flag = false;
          foreach (IStatementModel statement2 in this.second.StatementsModel.Statements)
          {
            if (!sessionDiff.Contains(statement2) && statement1.MatchWith(statement2))
            {
              sessionDiff.Add(statement2, this.second, DiffStatus.Matched);
              flag = true;
              break;
            }
          }
          if (!flag)
            sessionDiff.Add(statement1, this.first, DiffStatus.Removed);
        }
      }
      foreach (IStatementModel statement in this.second.StatementsModel.Statements)
      {
        if (!statement.IsTransaction && !sessionDiff.Contains(statement))
          sessionDiff.Add(statement, this.second, DiffStatus.Added);
      }
      sessionDiff.SelectedStatement = Enumerable.FirstOrDefault<IStatementModel>((IEnumerable<IStatementModel>) sessionDiff.Statements);
      return (ISessionDiff) sessionDiff;
    }

    public class SessionDiff : ISessionDiff, IExposeSelectableStatements
    {
      private List<IStatementModel> allStatements = new List<IStatementModel>();
      private readonly Dictionary<IStatementModel, DiffStatus> diffStatuses = new Dictionary<IStatementModel, DiffStatus>();
      private readonly Dictionary<IStatementModel, ISessionPresenterItem> statementSessions = new Dictionary<IStatementModel, ISessionPresenterItem>();
      private IStatementModel selectedStatement;

      public List<IStatementModel> Statements
      {
        get
        {
          return this.allStatements;
        }
        set
        {
          this.allStatements = value;
        }
      }

      public string FirstSessionName { get; set; }

      public string SecondSessionName { get; set; }

      public IStatementModel SelectedStatement
      {
        get
        {
          return this.selectedStatement;
        }
        set
        {
          if (this.selectedStatement == value)
            return;
          foreach (IStatementModel statementModel in this.Statements)
            statementModel.IsSelected = false;
          this.selectedStatement = value;
          if (this.selectedStatement == null)
            return;
          this.selectedStatement.IsSelected = true;
        }
      }

      public void Add(IStatementModel statement, ISessionPresenterItem session, DiffStatus status)
      {
        this.allStatements.Add(statement);
        this.diffStatuses.Add(statement, status);
        this.statementSessions.Add(statement, session);
      }

      public bool Contains(IStatementModel statement)
      {
        return this.diffStatuses.ContainsKey(statement);
      }

      public DiffStatus GetStatementStatus(IStatementModel statementModel)
      {
        DiffStatus diffStatus;
        if (!this.diffStatuses.TryGetValue(statementModel, out diffStatus))
          return DiffStatus.Matched;
        return diffStatus;
      }
    }
  }
}
