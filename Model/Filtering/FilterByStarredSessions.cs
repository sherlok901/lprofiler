﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Filtering.FilterByStarredSessions
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Sessions;
using HibernatingRhinos.Profiler.Client.Model.Statements;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Model.Filtering
{
  [Serializable]
  public class FilterByStarredSessions : AbstractFilter
  {
    [JsonIgnore]
    public ApplicationModel ApplicationModel { get; private set; }

    public StarFilterItem[] Stars { get; set; }

    public override bool IsValid
    {
      get
      {
        return true;
      }
    }

    public override string ToolTip
    {
      get
      {
        StringBuilder stringBuilder = new StringBuilder("Starred [[sessions]]: ");
        IEnumerable<StarFilterItem> source = Enumerable.Where<StarFilterItem>((IEnumerable<StarFilterItem>) this.Stars, (Func<StarFilterItem, bool>) (starFilterItem => starFilterItem.IsActive));
        if (Enumerable.Count<StarFilterItem>(source) == Enumerable.Count<StarFilterItem>((IEnumerable<StarFilterItem>) this.Stars))
        {
          stringBuilder.Append("all");
        }
        else
        {
          foreach (StarFilterItem starFilterItem in source)
            stringBuilder.Append(starFilterItem.ColorName.ToLowerInvariant()).Append(", ");
          stringBuilder.Remove(stringBuilder.Length - 2, 1);
        }
        return stringBuilder.ToString();
      }
    }

    public FilterByStarredSessions(ApplicationModel model)
    {
      this.ApplicationModel = model;
      this.Stars = new StarFilterItem[6]
      {
        new StarFilterItem("Yellow", true),
        new StarFilterItem("Orange", true),
        new StarFilterItem("Red", true),
        new StarFilterItem("Green", true),
        new StarFilterItem("Blue", true),
        new StarFilterItem("Gray", true)
      };
    }

    public override bool IsValidSession(IFilterableSessionSnapshot snapshot)
    {
      SessionModel sessionModel = snapshot as SessionModel;
      if (sessionModel != null)
        return Enumerable.Any<StarFilterItem>(Enumerable.Where<StarFilterItem>((IEnumerable<StarFilterItem>) this.Stars, (Func<StarFilterItem, bool>) (starFilterItem => starFilterItem.IsActive)), (Func<StarFilterItem, bool>) (activeItem => sessionModel.StarBrush.ToString() == activeItem.ColorName));
      return true;
    }

    public override bool IsValidStatement(IFilterableStatementSnapshot snapshot)
    {
      StatementModel statementModel = snapshot as StatementModel;
      if (statementModel != null)
      {
        SessionModel sessionModel = Enumerable.FirstOrDefault<SessionModel>((IEnumerable<SessionModel>) this.ApplicationModel.Sessions, (Func<SessionModel, bool>) (x => x.Id.Equals(statementModel.SessionId)));
        if (sessionModel != null)
          return this.IsValidSession((IFilterableSessionSnapshot) sessionModel);
        return true;
      }
      HibernatingRhinos.Profiler.Client.Model.Reports.QueryAggregation queryAggregation = snapshot as HibernatingRhinos.Profiler.Client.Model.Reports.QueryAggregation;
      if (queryAggregation != null)
        return Enumerable.Any<bool>(Enumerable.Select<SessionModel, bool>((IEnumerable<SessionModel>) Enumerable.ToList<SessionModel>(Enumerable.Where<SessionModel>(Enumerable.Select<RelatedSessionSpecification, SessionModel>((IEnumerable<RelatedSessionSpecification>) queryAggregation.RelatedSessions, (Func<RelatedSessionSpecification, SessionModel>) (rss => Enumerable.FirstOrDefault<SessionModel>((IEnumerable<SessionModel>) this.ApplicationModel.Sessions, (Func<SessionModel, bool>) (x => x.Id.Equals(rss.SessionId))))), (Func<SessionModel, bool>) (sessionModel => sessionModel != null))), new Func<SessionModel, bool>(((AbstractFilter) this).IsValidSession)), (Func<bool, bool>) (result => result));
      return true;
    }
  }
}
