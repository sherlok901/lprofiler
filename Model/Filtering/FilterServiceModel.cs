﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Filtering.FilterServiceModel
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Filtering
{
  public class FilterServiceModel
  {
    public ObservableCollection<IFilter> Filters { get; set; }

    public FilterServiceModel()
    {
      this.Filters = new ObservableCollection<IFilter>();
    }

    public bool FilterClosedSession(IFilterableSessionSnapshot session)
    {
      foreach (IFilter filter in Enumerable.ToArray<IFilter>((IEnumerable<IFilter>) this.Filters))
      {
        if (filter.IsActive && filter.IsValid && !filter.IsValidSession(session))
          return false;
      }
      return true;
    }

    public void PersistFilters()
    {
      UserPreferencesHolder.UserSettings.Filters.Clear();
      UserPreferencesHolder.UserSettings.Filters.AddRange((IEnumerable<IFilter>) this.Filters);
      UserPreferencesHolder.Save();
    }

    public bool FilterStatement(IFilterableStatementSnapshot statement)
    {
      foreach (IFilter filter in Enumerable.ToArray<IFilter>((IEnumerable<IFilter>) this.Filters))
      {
        if (filter.IsActive && filter.IsValid && !filter.IsValidStatement(statement))
          return false;
      }
      return true;
    }

    public bool FilterOpenSession(IFilterableSessionSnapshot session)
    {
      foreach (IFilter filter in Enumerable.ToArray<IFilter>((IEnumerable<IFilter>) this.Filters))
      {
        if (filter.IsActive && filter.IsValid && !filter.IsValidSession(session))
          return false;
      }
      return true;
    }

    public void ClearFilters()
    {
      this.Filters = new ObservableCollection<IFilter>();
      this.PersistFilters();
    }
  }
}
