﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Reports.QueriesByUrl
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Model.Filtering;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Reports
{
  [Serializable]
  public class QueriesByUrl : Report<IEnumerable<UrlQueriesSnapshot>>
  {
    [NonSerialized]
    private UrlQuery currentUrl;

    public override bool FilteringSupported
    {
      get
      {
        return true;
      }
    }

    public List<UrlQuery> StatementsByUrl { get; private set; }

    public UrlQuery CurrentUrl
    {
      get
      {
        if (this.currentUrl == null)
          this.CurrentUrl = Enumerable.FirstOrDefault<UrlQuery>((IEnumerable<UrlQuery>) this.StatementsByUrl);
        return this.currentUrl;
      }
      set
      {
        if (value == null || value == this.currentUrl)
          return;
        if (this.currentUrl != null)
          this.currentUrl.IsSelected = false;
        this.currentUrl = value;
        if (this.currentUrl == null)
          return;
        this.currentUrl.IsSelected = true;
      }
    }

    public override string JsonReportName
    {
      get
      {
        return "queriesByUrl";
      }
    }

    public QueriesByUrl(Func<IEnumerable<UrlQueriesSnapshot>> getSnapshot, FilterServiceModel filterService, ITrackingService trackingService)
      : base(trackingService, getSnapshot, filterService)
    {
      this.DisplayName = "Queries By Url";
      this.StatementsByUrl = new List<UrlQuery>();
    }

    protected override void Update(IEnumerable<UrlQueriesSnapshot> snapshots)
    {
      foreach (UrlQueriesSnapshot urlQueriesSnapshot in snapshots)
      {
        UrlQueriesSnapshot snapshot = urlQueriesSnapshot;
        UrlQuery urlQuery = Enumerable.FirstOrDefault<UrlQuery>(Enumerable.Where<UrlQuery>((IEnumerable<UrlQuery>) this.StatementsByUrl, (Func<UrlQuery, bool>) (shot => shot.Url == snapshot.Url)));
        if (urlQuery == null)
          this.StatementsByUrl.Add(UrlQuery.CreateFrom(snapshot, this.filterService, this.trackingService));
        else
          urlQuery.UpdateFrom(snapshot);
      }
    }

    public override void Clear()
    {
      foreach (UrlQuery urlQuery in this.StatementsByUrl)
        urlQuery.Clear();
      this.StatementsByUrl.Clear();
      this.currentUrl = (UrlQuery) null;
    }

    public override XElement ExportReport()
    {
      return new XElement(Report.Namespace + "queries-by-url", new object[2]
      {
        (object) new XAttribute((XName) "url-count", (object) this.StatementsByUrl.Count),
        (object) Enumerable.Select<UrlQuery, XElement>((IEnumerable<UrlQuery>) this.StatementsByUrl, (Func<UrlQuery, XElement>) (x => new XElement(Report.Namespace + "url", new object[3]
        {
          (object) new XAttribute((XName) "url", (object) x.Url),
          (object) new XAttribute((XName) "statement-count", (object) x.Statements.Count),
          (object) Enumerable.Select<QueryAggregation, XElement>((IEnumerable<QueryAggregation>) x.Statements, (Func<QueryAggregation, XElement>) (q => new XElement(Report.Namespace + "query", new object[7]
          {
            (object) XmlExportExtensions.ToXml(q.Duration),
            (object) XmlExportExtensions.ToXml(q.AverageDuration, "average-duration"),
            (object) new XElement(Report.Namespace + "count", (object) q.Count),
            (object) new XAttribute((XName) "is-cached", q.IsCached ? (object) true : (object) false),
            (object) new XAttribute((XName) "is-ddl", q.IsDDL ? (object) true : (object) false),
            (object) new XElement(Report.Namespace + "short-sql", (object) q.ShortSql),
            (object) new XElement(Report.Namespace + "formatted-sql", (object) q.FormattedSql)
          })))
        })))
      });
    }

    [CLSCompliant(false)]
    protected override JContainer ExportReportToJson()
    {
      return (JContainer) new JArray((object) Enumerable.Select<UrlQuery, JObject>((IEnumerable<UrlQuery>) this.StatementsByUrl, (Func<UrlQuery, JObject>) (x => new JObject(new object[3]
      {
        (object) new JProperty("url", (object) x.Url),
        (object) new JProperty("statementCount", (object) x.Statements.Count),
        (object) new JProperty("queries", (object) new JArray((object) Enumerable.Select<QueryAggregation, JObject>((IEnumerable<QueryAggregation>) x.Statements, (Func<QueryAggregation, JObject>) (q => new JObject(new object[6]
        {
          (object) new JProperty("averageDuration", (object) q.AverageDuration),
          (object) new JProperty("count", (object) q.Count),
          (object) new JProperty("isCached", q.IsCached ? (object) true : (object) false),
          (object) new JProperty("isDdl", q.IsDDL ? (object) true : (object) false),
          (object) new JProperty("shortSql", (object) q.ShortSql),
          (object) new JProperty("formattedSql", (object) q.FormattedSql)
        })))))
      }))));
    }
  }
}
