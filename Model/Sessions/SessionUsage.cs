﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sessions.SessionUsage
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Sorting;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Sessions
{
  public class SessionUsage : PollingBase<SessionUsageSnapshot>
  {
    private readonly SessionModel session;
    private readonly ITrackingService trackingService;
    private IDictionary<AlertInformation, int> aggregatedAlerts;
    private TimeSpan duration;
    private int entitiesLoaded;
    private readonly Sorter<KeyValuePair<AlertInformation, int>> sorter;
    private readonly ColumnResolversRegistry<KeyValuePair<AlertInformation, int>> columnResolvers;
    private IDictionary<AlertInformation, int> unsortedAlerts;

    public string OpenedAtText
    {
      get
      {
        return string.Format(ProfileExtensions.Translate(Profile.Current, "This [[session]] was opened at {0:hh:mm:ss.FFFFFFF tt} on {0:dddd, MMMM dd, yyyy}."), (object) this.Timestamp);
      }
    }

    public TimeSpan Duration
    {
      get
      {
        return this.duration;
      }
      set
      {
        this.duration = value;
      }
    }

    public int EntitiesLoaded
    {
      get
      {
        return this.entitiesLoaded;
      }
      set
      {
        this.entitiesLoaded = value;
      }
    }

    public IEnumerable<AggregatedAlertInformation> AggregatedAlerts
    {
      get
      {
        return Enumerable.Select<KeyValuePair<AlertInformation, int>, AggregatedAlertInformation>((IEnumerable<KeyValuePair<AlertInformation, int>>) this.aggregatedAlerts, (Func<KeyValuePair<AlertInformation, int>, AggregatedAlertInformation>) (pair => new AggregatedAlertInformation(pair)));
      }
    }

    public int TotalNumberOfAlerts
    {
      get
      {
        return Enumerable.Sum<AggregatedAlertInformation>(this.AggregatedAlerts, (Func<AggregatedAlertInformation, int>) (x => x.Count));
      }
    }

    public string Url
    {
      get
      {
        return this.session.Url;
      }
    }

    public DateTime Timestamp
    {
      get
      {
        return this.session.Timestamp;
      }
    }

    public SortSpecification<KeyValuePair<AlertInformation, int>> CurrentSort { get; set; }

    public SessionUsage(SessionModel session, ITrackingService trackingService, Func<Guid, SessionUsageSnapshot> getSnapshot)
      : base(getSnapshot, session.Id)
    {
      this.session = session;
      this.trackingService = trackingService;
      this.sorter = new Sorter<KeyValuePair<AlertInformation, int>>();
      this.columnResolvers = new ColumnResolversRegistry<KeyValuePair<AlertInformation, int>>();
      this.columnResolvers.Register("Count", (Func<KeyValuePair<AlertInformation, int>, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("Count", (object) x.Value)));
      this.columnResolvers.Register("Alerts", (Func<KeyValuePair<AlertInformation, int>, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("Alerts", (object) x.Key.Title)));
      this.CurrentSort = new SortSpecification<KeyValuePair<AlertInformation, int>>(trackingService)
      {
        Direction = ListSortDirection.Descending
      };
    }

    protected override void Update(SessionUsageSnapshot snapshot)
    {
      if (snapshot == null)
        return;
      this.Duration = snapshot.Duration;
      this.EntitiesLoaded = snapshot.EntitiesLoaded;
      this.aggregatedAlerts = (IDictionary<AlertInformation, int>) snapshot.AggregatedAlerts;
      this.unsortedAlerts = (IDictionary<AlertInformation, int>) snapshot.AggregatedAlerts;
    }

    protected override void OnActivate()
    {
      this.trackingService.Track("Sessions", "Usage", (string) null, new int?());
    }

    public void Sort(string columnName)
    {
      this.CurrentSort.SortColumnResolver = this.columnResolvers.Resolve(columnName);
      this.CurrentSort.ColumnName = columnName;
      this.aggregatedAlerts = (IDictionary<AlertInformation, int>) Enumerable.ToDictionary<KeyValuePair<AlertInformation, int>, AlertInformation, int>((IEnumerable<KeyValuePair<AlertInformation, int>>) this.sorter.Sort((IList<KeyValuePair<AlertInformation, int>>) Enumerable.ToList<KeyValuePair<AlertInformation, int>>((IEnumerable<KeyValuePair<AlertInformation, int>>) this.aggregatedAlerts), this.CurrentSort), (Func<KeyValuePair<AlertInformation, int>, AlertInformation>) (x => x.Key), (Func<KeyValuePair<AlertInformation, int>, int>) (x => x.Value));
    }

    public void CancelSorting()
    {
      this.CurrentSort.IsUndefined = true;
      this.CurrentSort.ColumnName = "";
      this.aggregatedAlerts = this.unsortedAlerts;
    }
  }
}
