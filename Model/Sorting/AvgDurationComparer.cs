﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sorting.AvgDurationComparer
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System.Collections;
using System.ComponentModel;

namespace HibernatingRhinos.Profiler.Client.Model.Sorting
{
  public class AvgDurationComparer : IComparer
  {
    private ListSortDirection direction;

    public AvgDurationComparer(ListSortDirection direction)
    {
      this.direction = direction;
    }

    public int Compare(object x, object y)
    {
      double? nullable1 = new double?(0.0);
      double? nullable2 = new double?(0.0);
      if (x == null && y == null)
        return 0;
      if (x == null)
      {
        nullable1 = new double?(this.direction == ListSortDirection.Ascending ? (double) int.MaxValue : -1.0);
        nullable2 = y as double?;
      }
      else if (y == null)
      {
        nullable2 = new double?(this.direction == ListSortDirection.Ascending ? (double) int.MaxValue : -1.0);
        nullable1 = x as double?;
      }
      else
      {
        nullable1 = x as double?;
        nullable2 = y as double?;
      }
      double? nullable3 = nullable1;
      double? nullable4 = nullable2;
      return (int) (nullable3.HasValue & nullable4.HasValue ? new double?(nullable3.GetValueOrDefault() - nullable4.GetValueOrDefault()) : new double?()).Value;
    }
  }
}
