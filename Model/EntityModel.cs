﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.EntityModel
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using System;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Client.Model
{
  [Serializable]
  public class EntityModel : PropertyChangedBase
  {
    private IList<string> identifiers;
    private bool isSelected;

    public string Name { get; private set; }

    public IList<string> Identifiers
    {
      get
      {
        return this.identifiers;
      }
      set
      {
        this.identifiers = value;
        this.NotifyOfPropertyChange("Identifiers");
      }
    }

    public bool IsSelected
    {
      get
      {
        return this.isSelected;
      }
      set
      {
        this.isSelected = value;
        this.NotifyOfPropertyChange("IsSelected");
      }
    }

    public EntityModel(EntitySnapshot entitySnapshot)
    {
      this.Name = entitySnapshot.Name;
      this.Identifiers = (IList<string>) new List<string>((IEnumerable<string>) entitySnapshot.Identifiers);
    }
  }
}
