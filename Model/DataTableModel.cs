﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.DataTableModel
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Infrastructure;
using System.Data;

namespace HibernatingRhinos.Profiler.Client.Model
{
  public class DataTableModel : SelectionBase
  {
    private DataTable _innerTable;

    public DataTable InnerTable
    {
      get
      {
        return this._innerTable;
      }
      set
      {
        this._innerTable = value;
        this.NotifyOfPropertyChange("InnerTable");
      }
    }

    public DataTableModel(DataTable table)
    {
      this.InnerTable = table;
    }
  }
}
