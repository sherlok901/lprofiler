﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sorting.Sorter`1
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Client.Model.Sorting
{
  public class Sorter<T>
  {
    public List<T> Sort(IList<T> statements, SortSpecification<T> specification)
    {
      return SortExtensions.Sort<T>(statements, new StatementsComparer<T>(specification.SortColumnResolver, specification.Direction));
    }
  }
}
