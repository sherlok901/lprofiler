﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Host.ErrorReporting.ReportingUserInfoView
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace HibernatingRhinos.Profiler.Client.Host.ErrorReporting
{
  public class ReportingUserInfoView : Form
  {
    private bool isFormValid;
    private IContainer components;
    private Label label1;
    private Label label2;
    private Button btnSend;
    private ErrorProvider errorProvider1;
    private BindingSource bindingSource1;
    private TextBox txtEmail;

    protected bool IsFormValid
    {
      get
      {
        return this.isFormValid;
      }
      set
      {
        this.isFormValid = value;
      }
    }

    public string Email
    {
      get
      {
        return this.txtEmail.Text;
      }
      set
      {
        this.txtEmail.Text = value;
      }
    }

    public ReportingUserInfoView(string title, string email)
    {
      this.InitializeComponent();
      this.IsFormValid = false;
      this.Text = title;
      this.Email = email;
    }

    private void button1_Click(object sender, EventArgs e)
    {
      if (!this.IsFormValid)
        return;
      this.DialogResult = DialogResult.OK;
    }

    private void txtEmail_Validating(object sender, CancelEventArgs e)
    {
      if (ModelValidator.EmptyOrNullValidator(this.txtEmail.Text))
      {
        e.Cancel = true;
        this.txtEmail.Select(0, this.txtEmail.Text.Length);
        this.errorProvider1.SetError((Control) this.txtEmail, "Please enter your email address.");
      }
      else
      {
        if (!ModelValidator.EmailValidator(this.txtEmail.Text))
          return;
        e.Cancel = true;
        this.errorProvider1.SetError((Control) this.txtEmail, "Please provide a valid email address.");
      }
    }

    private void txtEmail_Validated(object sender, EventArgs e)
    {
      this.errorProvider1.SetError((Control) this.txtEmail, "");
      this.IsFormValid = true;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (ReportingUserInfoView));
      this.label1 = new Label();
      this.label2 = new Label();
      this.btnSend = new Button();
      this.errorProvider1 = new ErrorProvider(this.components);
      this.bindingSource1 = new BindingSource(this.components);
      this.txtEmail = new TextBox();
      ((ISupportInitialize) this.errorProvider1).BeginInit();
      ((ISupportInitialize) this.bindingSource1).BeginInit();
      this.SuspendLayout();
      this.label1.AutoSize = true;
      this.label1.Location = new Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new Size(342, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Please provide your e-mail address in order to help us resolve the issue.";
      this.label2.AutoSize = true;
      this.label2.Location = new Point(12, 33);
      this.label2.Name = "label2";
      this.label2.Size = new Size(35, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Email:";
      this.btnSend.Location = new Point(280, 56);
      this.btnSend.Name = "btnSend";
      this.btnSend.Size = new Size(75, 23);
      this.btnSend.TabIndex = 3;
      this.btnSend.Text = "Send Report";
      this.btnSend.UseVisualStyleBackColor = true;
      this.btnSend.Click += new EventHandler(this.button1_Click);
      this.errorProvider1.ContainerControl = (ContainerControl) this;
      this.txtEmail.Location = new Point(53, 30);
      this.txtEmail.Name = "txtEmail";
      this.txtEmail.Size = new Size(301, 20);
      this.txtEmail.TabIndex = 4;
      this.txtEmail.Validating += new CancelEventHandler(this.txtEmail_Validating);
      this.txtEmail.Validated += new EventHandler(this.txtEmail_Validated);
      this.AcceptButton = (IButtonControl) this.btnSend;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(367, 91);
      this.Controls.Add((Control) this.txtEmail);
      this.Controls.Add((Control) this.btnSend);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.label1);
      this.FormBorderStyle = FormBorderStyle.FixedDialog;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "ReportingUserInfoView";
      this.Text = "ReportingUserInfoView";
      this.TopMost = true;
      ((ISupportInitialize) this.errorProvider1).EndInit();
      ((ISupportInitialize) this.bindingSource1).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
