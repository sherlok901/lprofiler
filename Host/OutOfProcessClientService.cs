﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Host.OutOfProcessClientService
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Exceptions;
using HibernatingRhinos.Profiler.BackEnd.Infrastructure.Connections;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client;
using HibernatingRhinos.Profiler.Client.Extensions;
using HibernatingRhinos.Profiler.Client.Host.ErrorReporting;
using HibernatingRhinos.Profiler.Client.Infrastructure.AutoUpdate;
using HibernatingRhinos.Profiler.Client.Messages;
using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Services;
using HibernatingRhinos.Profiler.Client.Services.QueryPlan;
using HibernatingRhinos.Profiler.Client.Startup;
using NAppUpdate.Framework;
using Rhino.Licensing;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Host
{
  public class OutOfProcessClientService : ClientService
  {
    private HttpListener listener;
    private WindowWrapper windowWrapper;

    public WindowWrapper WindowWrapper
    {
      get
      {
        return this.windowWrapper ?? (this.windowWrapper = new WindowWrapper(SilverlightLauncher.GetSilverlightGuiHandle()));
      }
    }

    public OutOfProcessClientService()
    {
      this.listener = new HttpListener();
      AppDomain.CurrentDomain.UnhandledException += (UnhandledExceptionEventHandler) ((sender, args) => OutOfProcessClientService.UnhandledException((Exception) args.ExceptionObject));
      Application.ThreadException += (ThreadExceptionEventHandler) ((sender, args) => OutOfProcessClientService.UnhandledException(args.Exception));
    }

    public bool AttemptToStartBackEnd(IBackendBridge backend)
    {
      try
      {
        backend.Start(UserPreferencesHolder.UserSettings.ListenPort, StartupParser.CurrentArguments.ContextFile);
        PollingBase.Start();
        return true;
      }
      catch (CouldNotActivateAnotherApplicationInstanceException ex)
      {
        int num = (int) MessageBox.Show(string.Concat(new object[4]
        {
          (object) "Could not start listening to notifications from profiled applications.",
          (object) Environment.NewLine,
          (object) "This error may be caused by another application listening to port ",
          (object) UserPreferencesHolder.UserSettings.ListenPort
        }), Profile.CurrentProfileDisplayName + " - Could not open socket for listening", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        this.CloseAfterAppInit();
      }
      catch (AnotherApplicationIsAlreadyRunningAndControlWasMovedToItException ex)
      {
        IntPtr silverlightGuiHandle = SilverlightLauncher.GetSilverlightGuiHandle();
        if (silverlightGuiHandle != IntPtr.Zero)
        {
          Win32.ShowWindow(silverlightGuiHandle, Win32.ShowWindowCommands.Show);
          Win32.SetForegroundWindow(silverlightGuiHandle);
        }
        else
        {
          new SilverlightLauncher().OpenSilverlightInterface().Wait();
          int num = (int) MessageBox.Show("The application is already running.", Profile.CurrentProfileDisplayName + " - is already running", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
        this.CloseAfterAppInit();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Could not start listening to notifications from profiled applications." + (object) Environment.NewLine + "This error may be caused by a firewall or a virus scanner that blocks " + Profile.CurrentProfileDisplayName + " from listening to a socket." + Environment.NewLine + Environment.NewLine + (string) (object) ex, Profile.CurrentProfileDisplayName + " - Could not open socket for listening", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        this.CloseAfterAppInit();
      }
      return false;
    }

    protected override void SaveToFile()
    {
      Schedulers.ExecuteOnTheUIThread((Action) (() =>
      {
        SaveFileDialog saveFileDialog = new SaveFileDialog()
        {
          Filter = "Profiler Output|*." + Profile.Current.ShortName,
          Title = Profile.CurrentProfileDisplayName
        };
        if (saveFileDialog.ShowDialog((IWin32Window) this.WindowWrapper) != DialogResult.OK)
          return;
        this.backendBridge.SaveToFile(saveFileDialog.FileName);
      }));
    }

    protected override OpenFileInfo StartLoadFromFile()
    {
      return Schedulers.ExecuteOnTheUIThread<OpenFileInfo>((Func<OpenFileInfo>) (() =>
      {
        OpenFileDialog openFileDialog = new OpenFileDialog()
        {
          CheckFileExists = true,
          Filter = "Profiler Output|*." + Profile.Current.ShortName,
          Title = Profile.CurrentProfileDisplayName
        };
        DialogResult dialogResult = openFileDialog.ShowDialog((IWin32Window) this.WindowWrapper);
        OpenFileInfo openFileInfo = new OpenFileInfo();
        if (dialogResult == DialogResult.OK)
        {
          openFileInfo.FileName = openFileDialog.FileName;
          openFileInfo.Token = this.backendBridge.StartLoadFromFile(openFileDialog.FileName);
        }
        return openFileInfo;
      })).Result;
    }

    protected override ExternalConnectionResponse BrowseExternalConnectionAssembly(IExternalConnectionDefinition target)
    {
      string specificFilter = string.Format("{0} ({1})|{1}", (object) target.DisplayName, (object) (target.AssemblyName + ".dll"));
      return Schedulers.ExecuteOnTheUIThread<ExternalConnectionResponse>((Func<ExternalConnectionResponse>) (() =>
      {
        OpenFileDialog openFileDialog = new OpenFileDialog()
        {
          Filter = specificFilter + "|Assemblies (*.dll)|*.dll|All files (*.*)|*.*",
          Title = "Load Assembly",
          RestoreDirectory = true
        };
        if (openFileDialog.ShowDialog((IWin32Window) this.WindowWrapper) != DialogResult.OK)
          return new ExternalConnectionResponse()
          {
            Cancelled = true
          };
        string fileName = openFileDialog.FileName;
        ExternalConnectionResponse response = new ExternalConnectionResponse()
        {
          FullPathToAssembly = fileName,
          Successful = true
        };
        LoadAssemblyUtil.VerifyThatAssemblyCanBeLoaded(fileName, (Action<Exception>) (e =>
        {
          response.Successful = false;
          response.Exception = e;
        }));
        return response;
      })).Result;
    }

    protected override QueryPlanStep LoadQueryPlanFromFile()
    {
      return Schedulers.ExecuteOnTheUIThread<QueryPlanStep>((Func<QueryPlanStep>) (() =>
      {
        SqlServerShowPlanService serverShowPlanService = new SqlServerShowPlanService(string.Empty, (IDbConnection) null);
        OpenFileDialog openFileDialog = new OpenFileDialog()
        {
          CheckFileExists = true,
          Filter = "SQL Query Plan|*.sqlplan",
          Title = Profile.CurrentProfileDisplayName
        };
        if (openFileDialog.ShowDialog((IWin32Window) this.WindowWrapper) != DialogResult.OK)
          return (QueryPlanStep) null;
        XDocument doc;
        using (Stream stream = openFileDialog.OpenFile())
          doc = XDocument.Load((TextReader) new StreamReader(stream));
        return serverShowPlanService.LoadPlanFrom(doc);
      })).Result;
    }

    public override void Dispose()
    {
      this.listener.Stop();
      base.Dispose();
    }

    public void StartWithOutOfProcessListener(string contextFile)
    {
      this.Start();
      Thread thread = new Thread(new ThreadStart(OutOfProcessClientService.SelfVerification))
      {
        IsBackground = true
      };
      thread.SetApartmentState(ApartmentState.STA);
      thread.Start();
      this.AttemptToStartBackEnd((IBackendBridge) this.backendBridge);
      NonAdminHttp.EnsureCanListenToWhenInNonAdminContext(17560);
      this.listener.Prefixes.Add("http://+:" + (object) 17560 + "/");
      ClientService.Log.Debug((object) "Attempt to start HTTP listener");
      this.listener.Start();
      ClientService.Log.Debug((object) "HTTP listener started successfully");
      this.profilerAutoUpdate.Configure();
      this.licensingService.OnLicenseValidated += (Action) (() => this.profilerAutoUpdate.CheckForUpdates(false));
      if (!string.IsNullOrEmpty(contextFile))
      {
        Guid guid = this.backendBridge.StartLoadFromFile(contextFile);
        this.backendBridge.Notifications.RaiseBringApplicationToFront(new OpenFileInfo()
        {
          FileName = contextFile,
          Token = guid
        });
      }
      FileAssociation.SetupAssociation();
      this.HandleRequests();
      Application.Idle += new EventHandler(this.OnStartUp);
      Application.Run();
      UpdateManager.Instance.Abort(true);
      try
      {
        ProfilerAutoUpdate.ApplyUpdates(this.applyUpdate);
      }
      catch (LicenseUpgradeRequiredException ex)
      {
        ClientService.Log.Info((object) "you need to upgrade your license.", (Exception) ex);
      }
      UpdateManager.Instance.CleanUp();
      if (!this.restart)
        return;
      SilverlightLauncher.GetSilverlightProcess().Kill();
      Application.Restart();
    }

    private static void SelfVerification()
    {
      Thread.Sleep(TimeSpan.FromSeconds((double) new Random().Next(10, 25)));
      Assembly assembly = typeof (BuildInfo).Assembly;
      byte[] numArray = Convert.FromBase64String("ACQAAASAAACUAAAABgIAAAAkAABSU0ExAAQAAAEAAQDBLTP+od21ZEnX3WXgZVRX1adQQHusYawlMrB4Mnz5vH3GcynLb4CvG7zwjJiYsit/xgN28VmeeQJ5PSdcXH1aB54Qnm4TMa+HcRFxcnGLQQfzwKa/rQIufkQ+Du1hERNZRdERss/wpbnyF2mcNsmGDa09Y+x0264LLK/sK4SIog==");
      byte[] publicKey = assembly.GetName().GetPublicKey();
      for (int index = 0; index < publicKey.Length; ++index)
      {
        if ((int) publicKey[index] != (int) numArray[index])
          throw new InvalidOleVariantTypeException("pk");
      }

        //igor crack
      //bool pfWasVerified = false;
      //if (!HostProgram.StrongNameSignatureVerificationEx(assembly.Location, true, ref pfWasVerified))
      //  throw new InvalidOleVariantTypeException("sn");
    }

    private void OnStartUp(object sender, EventArgs eventArgs)
    {
      Schedulers.UIThread = TaskScheduler.FromCurrentSynchronizationContext();
      Application.Idle -= new EventHandler(this.OnStartUp);
    }

    private void HandleRequests()
    {
      Task.Factory.StartNew((Action) (() =>
      {
        while (!this.shouldClose)
        {
          HttpListenerContext context = this.listener.GetContext();
          if (string.Equals(context.Request.RawUrl, "/ClientAccessPolicy.xml", StringComparison.InvariantCultureIgnoreCase))
          {
            using (StreamWriter streamWriter = new StreamWriter(context.Response.OutputStream))
              streamWriter.Write("<?xml version='1.0' encoding='utf-8'?>\r\n<access-policy>\r\n <cross-domain-access>\r\n   <policy>\r\n\t <allow-from http-methods='*' http-request-headers='*'>\r\n\t   <domain uri='*' />\r\n\t </allow-from>\r\n\t <grant-to>\r\n\t   <resource include-subpaths='true' path='/' />\r\n\t </grant-to>\r\n   </policy>\r\n </cross-domain-access>\r\n</access-policy>");
            context.Response.Close();
          }
          else
          {
            IBackendRequest request;
            try
            {
              request = MessageSerializer.Deserialize<IBackendRequest>(context.Request.InputStream);
            }
            catch (HttpListenerException ex)
            {
              ClientService.Log.Info((object) "Got a bad request. Chance taht this is an internal HTTP connection error and can be ignored.", (Exception) ex);
              continue;
            }
            catch (Exception ex)
            {
              ClientService.Log.Info((object) "Got a bad request", ex);
              continue;
            }
            try
            {
              MessageSerializer.Serialize(this.RelayToBridge(request), context.Response.OutputStream);
            }
            catch (Exception ex)
            {
              ClientService.Log.Warn((object) ("Could not process request: " + context.Request.RawUrl), ex);
              continue;
            }
            try
            {
              context.Response.Close();
            }
            catch (Exception ex)
            {
              ClientService.Log.Info((object) "Could not close request", ex);
            }
          }
        }
        Application.Exit();
      }));
    }

    private static void TryReportErrortoUser(HttpListenerContext ctx, Exception e)
    {
      try
      {
        ctx.Response.StatusCode = 500;
        using (StreamWriter streamWriter = new StreamWriter(ctx.Response.OutputStream))
          streamWriter.Write(e.ToString());
        ctx.Response.Close();
      }
      catch (Exception ex)
      {
      }
      OutOfProcessClientService.ReportError(e);
    }

    private static void ReportError(Exception e)
    {
      if (MessageBox.Show("Backend exception occurred: " + Environment.NewLine + e.Message + Environment.NewLine + "Report error to support?", Profile.CurrentProfileDisplayName + ": Unhandled exception", MessageBoxButtons.YesNo) == DialogResult.No)
        return;
      string result = Schedulers.ExecuteOnTheUIThread<string>((Func<string>) (() =>
      {
        ReportingUserInfoView reportingUserInfoView = new ReportingUserInfoView(Profile.CurrentProfileDisplayName + ": Unhandled exception", UserPreferencesHolder.UserSettings.EmailAddress);
        if (reportingUserInfoView.ShowDialog() != DialogResult.OK)
          return (string) null;
        return reportingUserInfoView.Email;
      })).Result;
      if (result == null)
        return;
      UserPreferencesHolder.UserSettings.EmailAddress = result;
      UserPreferencesHolder.Save();
      int num = (int) MessageBox.Show("Successfully reported unhandled exception: " + new HibernatingRhinos.Profiler.Client.Services.ErrorReporter().ReportError(UserPreferencesHolder.UserSettings, e));
    }

    private void CloseAfterAppInit()
    {
      Environment.Exit(0);
    }

    private static void UnhandledException(Exception exceptionObject)
    {
      try
      {
        if (OutOfProcessClientService.HandleUltraMonError(exceptionObject))
          Environment.Exit(1);
        ClientService.Log.Fatal((object) "Unhandled exception ruthlessly killed application", exceptionObject);
        if (MessageBox.Show("Unhandled exception occurred: " + Environment.NewLine + exceptionObject.Message + Environment.NewLine + "Report error to support?", Profile.CurrentProfileDisplayName + ": Unhandled exception", MessageBoxButtons.YesNo) == DialogResult.No)
          return;
        ReportingUserInfoView reportingUserInfoView = new ReportingUserInfoView(Profile.CurrentProfileDisplayName + ": Unhandled exception", UserPreferencesHolder.UserSettings.EmailAddress);
        if (reportingUserInfoView.ShowDialog() != DialogResult.OK)
          return;
        if (reportingUserInfoView.Email != null)
          UserPreferencesHolder.UserSettings.EmailAddress = reportingUserInfoView.Email;
        int num = (int) MessageBox.Show("Successfully reported unhandled exception: " + new HibernatingRhinos.Profiler.Client.Services.ErrorReporter().ReportError(UserPreferencesHolder.UserSettings, exceptionObject));
        Environment.Exit(1);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Failed to report unhandled exception: " + (object) Environment.NewLine + (string) (object) exceptionObject + Environment.NewLine + "Could not report the exception because: " + ex.Message);
      }
    }

    private static bool HandleUltraMonError(Exception exception)
    {
      if (!Enumerable.Any<Process>(Enumerable.Where<Process>((IEnumerable<Process>) Process.GetProcesses(), (Func<Process, bool>) (process => process.ProcessName.StartsWith("UltraMon")))) || !(exception is OverflowException) && !(exception.InnerException is OverflowException))
        return false;
      ClientService.Log.Error((object) "UltraMon handled exception", exception);
      int num = (int) MessageBox.Show("An error occurred in " + Profile.CurrentProfileDisplayName + " which is known to be caused by using UltraMon on your computer." + Environment.NewLine + "Please follow the instructions in the following URL in order to solve this issue: " + Profile.Current.GetLearnTopic("faq", "UltraMon"));
      return true;
    }
  }
}
