﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Host.Schedulers
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using log4net;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace HibernatingRhinos.Profiler.Client.Host
{
  public static class Schedulers
  {
    private static readonly ILog Log = LogManager.GetLogger(typeof (Schedulers));

    public static TaskScheduler UIThread { get; set; }

    public static Task ExecuteOnTheUIThread(Action action)
    {
      return Task.Factory.StartNew((Action) (() =>
      {
        try
        {
          action();
        }
        catch (Exception ex)
        {
          Schedulers.Log.Error((object) "Failed to run a task on the UI thread", ex);
          throw;
        }
      }), CancellationToken.None, TaskCreationOptions.None, Schedulers.UIThread);
    }

    public static Task<TResult> ExecuteOnTheUIThread<TResult>(Func<TResult> action)
    {
      return Task.Factory.StartNew<TResult>((Func<TResult>) (() =>
      {
        try
        {
          return action();
        }
        catch (Exception ex)
        {
          Schedulers.Log.Error((object) "Failed to run a task on the UI thread", ex);
          throw;
        }
      }), CancellationToken.None, TaskCreationOptions.None, Schedulers.UIThread);
    }
  }
}
