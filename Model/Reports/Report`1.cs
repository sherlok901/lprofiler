﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Reports.Report`1
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Filtering;
using HibernatingRhinos.Profiler.Client.Model.Sorting;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Reports
{
  public abstract class Report<T> : Report
  {
    private readonly Func<T> getSnapshot;
    protected readonly Sorter<QueryAggregation> sorter;
    protected readonly ColumnResolversRegistry<QueryAggregation> columnResolvers;
    protected readonly FilterServiceModel filterService;

    public SortSpecification<QueryAggregation> CurrentSort { get; set; }

    public bool IsGoToSessionMenuVisible
    {
      get
      {
        return Enumerable.Count<FilterByStarredSessions>(Enumerable.OfType<FilterByStarredSessions>((IEnumerable) this.filterService.Filters)) == 0;
      }
    }

    protected Report(ITrackingService trackingService, Func<T> getSnapshot, FilterServiceModel filterService)
      : base(trackingService)
    {
      this.getSnapshot = getSnapshot;
      this.filterService = filterService;
      this.sorter = new Sorter<QueryAggregation>();
      this.columnResolvers = new ColumnResolversRegistry<QueryAggregation>();
      this.columnResolvers.Register("ShortSql", (Func<QueryAggregation, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("ShortSql", (object) x.ShortSql)));
      this.columnResolvers.Register("AvgDuration", (Func<QueryAggregation, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("AvgDuration", (object) x.AverageDuration)));
      this.columnResolvers.Register("QueryCount", (Func<QueryAggregation, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("QueryCount", (object) x.Count)));
      this.CurrentSort = new SortSpecification<QueryAggregation>(trackingService)
      {
        Direction = ListSortDirection.Descending
      };
    }

    public override void TimerTicked(object sender, EventArgs e)
    {
      if (this.state == PollingState.Finalized)
      {
        PollingBase.Timer.Tick -= new EventHandler(((PollingBase) this).TimerTicked);
      }
      else
      {
        this.Update(this.getSnapshot());
        if (this.state != PollingState.NotFinalized)
          return;
        this.state = PollingState.Finalized;
      }
    }

    protected abstract void Update(T snapshot);

    public void ForceUpdate()
    {
      this.Update(this.getSnapshot());
      if (this.state != PollingState.NotFinalized)
        return;
      this.state = PollingState.Finalized;
    }

    public virtual void CancelSorting()
    {
      this.CurrentSort.IsUndefined = true;
      this.CurrentSort.ColumnName = "";
    }
  }
}
