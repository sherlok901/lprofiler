﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Commands.SelectSessionStatements
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Sessions;
using HibernatingRhinos.Profiler.Client.Model.Statements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Commands
{
  public class SelectSessionStatements
  {
    private readonly ApplicationModel applicationModel;

    public SelectSessionStatements(ApplicationModel applicationModel)
    {
      this.applicationModel = applicationModel;
    }

    public void Execute(IExposeSelectableStatements model, IList selectedItems)
    {
      IOrderedEnumerable<IStatementModel> orderedEnumerable = Enumerable.OrderBy<IStatementModel, int>(Enumerable.OfType<IStatementModel>((IEnumerable) selectedItems), (Func<IStatementModel, int>) (x => this.GetStatementIndex(model, x)));
      this.SetSelectedStatements(model, Enumerable.ToArray<IStatementModel>((IEnumerable<IStatementModel>) orderedEnumerable));
    }

    public int GetStatementIndex(IExposeSelectableStatements model, IStatementModel statementModel)
    {
      return model.Statements.IndexOf(statementModel);
    }

    public void SetSelectedStatements(IExposeSelectableStatements model, IStatementModel[] selection)
    {
      if (model.SelectedStatement != null)
        model.SelectedStatement.UnrootStatement();
      this.applicationModel.TrackingService.Track("Main", "Selection", "Statements", new int?(selection.Length));
      switch (selection.Length)
      {
        case 0:
          model.SelectedStatement = (IStatementModel) null;
          break;
        case 1:
          model.SelectedStatement = selection[0];
          break;
        default:
          if (selection.Length <= 1)
            break;
          model.SelectedStatement = (IStatementModel) new StatementModel(new StatementSnapshot()
          {
            FormattedSql = StatementsExtensions.Aggregate((IEnumerable<IStatementModel>) selection, (Func<IStatementModel, string>) (x => x.Text)),
            ShortSql = StatementsExtensions.Aggregate((IEnumerable<IStatementModel>) selection, (Func<IStatementModel, string>) (x => x.ShortText)),
            RawSql = StatementsExtensions.Aggregate((IEnumerable<IStatementModel>) selection, (Func<IStatementModel, string>) (x => x.RawSql)),
            CanExecuteQuery = false,
            IsSelectStatement = false,
            Url = selection[0].Url,
            Alerts = (IEnumerable<AlertInformation>) new AlertInformation[0],
            Duration = StatementsExtensions.Aggregate((IEnumerable<IStatementModel>) selection, (Func<IStatementModel, QueryDuration>) (x => x.Duration)),
            CountOfRows = StatementsExtensions.Aggregate((IEnumerable<IStatementModel>) selection, (Func<IStatementModel, int[]>) (x => x.CountOfRows))
          })
          {
            ApplicationModel = this.applicationModel,
            CanJumpToSession = false,
            Children = (IEnumerable<IStatementModel>) selection,
            MultiSelection = true
          };
          foreach (IStatementModel statementModel in selection)
            statementModel.IsSelected = true;
          break;
      }
    }
  }
}
