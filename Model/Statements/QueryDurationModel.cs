﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Statements.QueryDurationModel
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Model.Statements
{
  public class QueryDurationModel
  {
    public QueryDuration Duration { get; set; }

    public bool HasValue
    {
      get
      {
        return this.Duration.Value.HasValue;
      }
    }

    public string ShortDescription
    {
      get
      {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.Duration.InDatabase.HasValue)
          stringBuilder.Append((object) this.Duration.InDatabase).Append(" ms");
        if (this.Duration.InNHibernate.HasValue)
        {
          if (this.Duration.InDatabase.HasValue)
            stringBuilder.Append(" / ");
          stringBuilder.Append((object) this.Duration.InNHibernate).Append(" ms");
        }
        return stringBuilder.ToString();
      }
    }

    public string LongDescription
    {
      get
      {
        StringBuilder stringBuilder = new StringBuilder().AppendLine("Query duration:");
        if (this.Duration.InDatabase.HasValue)
          stringBuilder.Append("  - Database only: ").Append((object) this.Duration.InDatabase).Append(" ms").AppendLine();
        if (this.Duration.InNHibernate.HasValue)
          stringBuilder.Append("  - Total: ").Append((object) this.Duration.InNHibernate).Append(" ms").AppendLine();
        return stringBuilder.Remove(stringBuilder.Length - 2, 2).ToString();
      }
    }

    public QueryDurationModel(QueryDuration duration)
    {
      this.Duration = duration;
    }
  }
}
