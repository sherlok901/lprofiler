﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sorting.SortExtensions
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;
using System.Collections.Generic;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Sorting
{
  public static class SortExtensions
  {
    public static List<T> Sort<T>(this IList<T> collection, StatementsComparer<T> comparer)
    {
      return Enumerable.ToList<T>((IEnumerable<T>) Enumerable.OrderBy<T, T>((IEnumerable<T>) collection, (Func<T, T>) (x => x), (IComparer<T>) comparer));
    }
  }
}
