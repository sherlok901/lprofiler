﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sorting.StatementsComparer`1
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace HibernatingRhinos.Profiler.Client.Model.Sorting
{
  public class StatementsComparer<T> : IComparer<T>
  {
    public Func<T, KeyValuePair<string, object>> SortColumnResolver { get; set; }

    public ListSortDirection Direction { get; set; }

    public ComparatorsRegistry Comparators { get; set; }

    public StatementsComparer(Func<T, KeyValuePair<string, object>> resolver, ListSortDirection direction)
    {
      this.SortColumnResolver = resolver;
      this.Direction = direction;
      this.Comparators = new ComparatorsRegistry();
      this.Comparators.Register("RowCount", (IComparer) new CountOfRowsComparer(direction));
      this.Comparators.Register("Duration", (IComparer) new DurationComparer(direction));
      this.Comparators.Register("Alerts", (IComparer) new AlertsComparer());
      this.Comparators.Register("AvgDuration", (IComparer) new AvgDurationComparer(direction));
    }

    public int Compare(T x, T y)
    {
      object x1 = this.SortColumnResolver(x).Value;
      object y1 = this.SortColumnResolver(y).Value;
      IComparable comparable1 = x1 as IComparable;
      IComparable comparable2 = y1 as IComparable;
      if (comparable1 != null && comparable2 != null)
      {
        if (this.Direction != ListSortDirection.Ascending)
          return -comparable1.CompareTo((object) comparable2);
        return comparable1.CompareTo((object) comparable2);
      }
      IComparer comparer = this.Comparators.Resolve(this.SortColumnResolver(x).Key);
      if (this.Direction != ListSortDirection.Ascending)
        return -comparer.Compare(x1, y1);
      return comparer.Compare(x1, y1);
    }
  }
}
