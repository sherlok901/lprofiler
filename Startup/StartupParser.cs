﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.StartupParser
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  public class StartupParser
  {
    private static bool wasAttachedToConsole;
    public static ProfArguments CurrentArguments;

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern bool AttachConsole(int dwProcessId);

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern bool AllocConsole();

    [DllImport("kernel32.dll", SetLastError = true)]
    private static extern bool FreeConsole();

    public static IStartupCommand ParseCommandLineArguments(string[] args)
    {
      try
      {
        StartupParser.CurrentArguments = new ProfArguments();
        if (!Parser.ParseArguments(args, (object) StartupParser.CurrentArguments, new ErrorReporter(StartupParser.WriteLineToConsole)))
        {
          StartupParser.WriteLineToConsole(Parser.ArgumentsUsage(typeof (ProfArguments)));
          return (IStartupCommand) new ExitCommand();
        }
        if (StartupParser.CurrentArguments.GuiMode)
          return (IStartupCommand) new SilverlightGuiCommand();
        if (StartupParser.CurrentArguments.CmdLineMode)
          return (IStartupCommand) new CommandLineCommand(StartupParser.CurrentArguments.SpecifiedPortOrDefaultPort, StartupParser.CurrentArguments.File, StartupParser.CurrentArguments.InputFile, StartupParser.CurrentArguments.LicensePath, (IEnumerable<ReportFormat>) StartupParser.CurrentArguments.ReportFormat);
        if (StartupParser.CurrentArguments.Shutdown)
          return (IStartupCommand) new ShutdownCommand(StartupParser.CurrentArguments.SpecifiedPortOrDefaultPort);
        return (IStartupCommand) new SilverlightGuiCommand();
      }
      finally
      {
        StartupParser.FreeConsoleIfNeeded();
      }
    }

    public static void FreeConsoleIfNeeded()
    {
      if (!StartupParser.wasAttachedToConsole)
        return;
      StartupParser.FreeConsole();
      StartupParser.wasAttachedToConsole = false;
    }

    public static void WriteLineToConsole(string message)
    {
      if (!StartupParser.wasAttachedToConsole)
      {
        if (!StartupParser.AttachConsole(-1))
          StartupParser.AllocConsole();
        StartupParser.wasAttachedToConsole = true;
        Console.WriteLine();
      }
      Console.WriteLine(message);
    }

    public static void WriteToConsole(string message)
    {
      if (!StartupParser.wasAttachedToConsole)
      {
        if (!StartupParser.AttachConsole(-1))
          StartupParser.AllocConsole();
        StartupParser.wasAttachedToConsole = true;
        Console.WriteLine();
      }
      Console.Write(message);
    }
  }
}
