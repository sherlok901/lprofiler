﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Services.StackTraceExplorer
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using EnvDTE;
using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;

namespace HibernatingRhinos.Profiler.Client.Services
{
  public class StackTraceExplorer
  {
    private static readonly ILog log = LogManager.GetLogger(typeof (StackTraceExplorer));

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool SetForegroundWindow(IntPtr hWnd);

    [DllImport("user32.dll")]
    private static extern IntPtr GetForegroundWindow();

    [DllImport("ole32.dll")]
    public static extern int GetRunningObjectTable(int reserved, out IRunningObjectTable prot);

    [DllImport("ole32.dll")]
    public static extern int CreateBindCtx(int reserved, out IBindCtx ppbc);

    public static void ShowToUser(StackTraceFrame frame)
    {
      Task.Factory.StartNew((Action) (() =>
      {
        try
        {
          StackTraceExplorer.ShowToUserBackground(frame);
        }
        catch (Exception ex)
        {
          StackTraceExplorer.log.Error((object) ("Could not show " + frame.FullFilename + " to the user"), ex);
        }
      }));
    }

    private static void ShowToUserBackground(StackTraceFrame frame)
    {
      if (StringExtensions.Empty(frame.FullFilename) || !File.Exists(frame.FullFilename))
        return;
      StackTraceExplorer.log.DebugFormat("Need to display in VS: {0}", (object) frame.FullFilename);
      DTE visualStudioInstance = StackTraceExplorer.FindSuitableVisualStudioInstance(frame.FullFilename);
      if (visualStudioInstance == null)
      {
        StackTraceExplorer.log.Debug((object) "Could not find a running version of VS, starting a new one");
        visualStudioInstance = StackTraceExplorer.CreateVisualStudioInstance(frame);
        if (visualStudioInstance == null)
        {
          StackTraceExplorer.log.Warn((object) "Could not start a new version of VS");
          return;
        }
      }
      Window win = visualStudioInstance.ItemOperations.OpenFile(frame.FullFilename, "{7651A703-06E5-11D1-8EBD-00A0C90F26EA}");
      StackTraceExplorer.TryReallyHardToShowSelection(frame, win, 0);
    }

    private static DTE FindSuitableVisualStudioInstance(string fileName)
    {
      IList<DTE> visualStudioInstances = StackTraceExplorer.FindVisualStudioInstances();
      foreach (DTE dte in (IEnumerable<DTE>) visualStudioInstances)
      {
        if (dte.get_IsOpenFile("{FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF}", fileName))
          return dte;
      }
      StackTraceExplorer.log.Debug((object) "File is not opened yet so I'm going to scan solutions");
      foreach (DTE dte in (IEnumerable<DTE>) visualStudioInstances)
      {
        if (StackTraceExplorer.IsSuitableToShowFile(dte, fileName))
          return dte;
      }
      if (visualStudioInstances.Count <= 0)
        return (DTE) null;
      StackTraceExplorer.log.Debug((object) "I've found any suitable VS instance so I'm returning first on list");
      return visualStudioInstances[0];
    }

    private static IList<DTE> FindVisualStudioInstances()
    {
      IMoniker[] rgelt = new IMoniker[1];
      IntPtr pceltFetched = IntPtr.Zero;
      IList<DTE> list = (IList<DTE>) new List<DTE>();
      IBindCtx ppbc;
      StackTraceExplorer.CreateBindCtx(0, out ppbc);
      IRunningObjectTable pprot;
      ppbc.GetRunningObjectTable(out pprot);
      IEnumMoniker ppenumMoniker;
      pprot.EnumRunning(out ppenumMoniker);
      ppenumMoniker.Reset();
      while (ppenumMoniker.Next(1, rgelt, pceltFetched) == 0)
      {
        string ppszDisplayName;
        rgelt[0].GetDisplayName(ppbc, (IMoniker) null, out ppszDisplayName);
        object ppunkObject;
        pprot.GetObject(rgelt[0], out ppunkObject);
        DTE dte = ppunkObject as DTE;
        if (dte != null && ppszDisplayName.StartsWith("!VisualStudio.DTE"))
          list.Add(dte);
      }
      return list;
    }

    private static bool IsSuitableToShowFile(DTE dte, string fileName)
    {
      if (dte.Solution == null)
        return false;
      try
      {
        foreach (Project project in dte.Solution.Projects)
        {
          if (StackTraceExplorer.IsSuitableToShowFile(project, fileName))
            return true;
        }
      }
      catch (COMException ex)
      {
        StackTraceExplorer.log.Warn((object) "Error in project scanning.", (Exception) ex);
      }
      return false;
    }

    private static bool IsSuitableToShowFile(Project project, string fileName)
    {
      foreach (ProjectItem projectItem in project.ProjectItems)
      {
        if (projectItem.Kind == "{EA6618E8-6E24-4528-94BE-6889FE16485C}")
        {
          Project project1 = projectItem.Object as Project;
          if (project1 != null)
            return StackTraceExplorer.IsSuitableToShowFile(project1, fileName);
        }
        else if (projectItem.Kind == "{6BB5F8EE-4483-11D3-8BCF-00C04F8EC28C}" && projectItem.get_FileNames((short) 0) == fileName)
          return true;
      }
      return false;
    }

    private static void TryReallyHardToShowSelection(StackTraceFrame frame, Window win, int tries)
    {
      try
      {
        StackTraceExplorer.ShowSelection(frame, win);
      }
      catch (COMException ex)
      {
        if (tries < 4 && ex.Message.Contains("RPC_E_SERVERCALL_RETRYLATER"))
        {
          StackTraceExplorer.log.Warn((object) "Got RPC_E_SERVERCALL_RETRYLATER error when trying to show item, will retry in 0.5 sec");
          System.Threading.Thread.Sleep(500);
          StackTraceExplorer.TryReallyHardToShowSelection(frame, win, tries + 1);
        }
        else
          throw;
      }
    }

    private static void ShowSelection(StackTraceFrame frame, Window win)
    {
      ((TextSelection) win.Document.Selection).MoveTo(frame.Line, frame.Column, false);
      win.DTE.MainWindow.Activate();
      System.Threading.Thread.Sleep(500);
      if (StackTraceExplorer.GetForegroundWindow().ToInt32() == win.HWnd)
        return;
      StackTraceExplorer.log.Warn((object) "Trying to ask nicely from VS to activate has failed, trying to use SetForegroundWindow for the task");
      bool flag = StackTraceExplorer.SetForegroundWindow(new IntPtr(win.HWnd));
      StackTraceExplorer.log.DebugFormat("SetForegroundWindow returned: {0}", (object) (flag ? 1 : 0));
    }

    private static DTE CreateVisualStudioInstance(StackTraceFrame frame)
    {
      System.Diagnostics.Process process;
      try
      {
        StackTraceExplorer.log.Debug((object) "Trying to start a new instance of devenv");
        process = System.Diagnostics.Process.Start("devenv", frame.FullFilename);
      }
      catch (Exception ex1)
      {
        try
        {
          StackTraceExplorer.log.DebugFormat("Trying to open the file '{0}' directly", (object) frame.FullFilename);
          process = System.Diagnostics.Process.Start(frame.FullFilename);
        }
        catch (Exception ex2)
        {
          StackTraceExplorer.log.WarnFormat("Could not open {0}", (object) frame.FullFilename);
          return (DTE) null;
        }
      }
      process.WaitForInputIdle(5000);
      return StackTraceExplorer.FindSuitableVisualStudioInstance(frame.FullFilename);
    }
  }
}
