﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.AttachedApplications.AttachedApplication
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Model.AttachedApplications
{
  public class AttachedApplication : PropertyChangedBase
  {
    private IList<AttachedApplicationInstance> registeredInstances = (IList<AttachedApplicationInstance>) new List<AttachedApplicationInstance>();
    private ApplicationModel applicationModel;

    public string Name { get; private set; }

    public string DisplayName
    {
      get
      {
        string str = this.Name;
        if (this.registeredInstances.Count > 1)
          str = string.Concat(new object[4]
          {
            (object) str,
            (object) "[#",
            (object) this.registeredInstances.Count,
            (object) "]"
          });
        return str;
      }
    }

    public ICollection<AttachedApplicationInstance> Instances
    {
      get
      {
        return (ICollection<AttachedApplicationInstance>) this.registeredInstances;
      }
    }

    public string Details
    {
      get
      {
        StringBuilder stringBuilder = new StringBuilder();
        foreach (AttachedApplicationInstance applicationInstance in (IEnumerable<AttachedApplicationInstance>) this.registeredInstances)
          stringBuilder.Append(applicationInstance.ToString());
        return stringBuilder.ToString();
      }
    }

    public AttachedApplication(ApplicationModel applicationModel, string name)
    {
      this.Name = name;
      this.applicationModel = applicationModel;
    }

    public void RegisterNewInstance(Guid instanceIdentifier)
    {
      AttachedApplicationInstance applicationInstance = new AttachedApplicationInstance()
      {
        Id = instanceIdentifier
      };
      if (this.registeredInstances.Contains(applicationInstance))
        return;
      this.registeredInstances.Add(applicationInstance);
      this.RefreshDisplay();
    }

    public void UnregisterInstance(Guid instanceIdentifier)
    {
      AttachedApplicationInstance applicationInstance = new AttachedApplicationInstance()
      {
        Id = instanceIdentifier
      };
      if (!this.registeredInstances.Contains(applicationInstance))
        return;
      this.registeredInstances.Remove(applicationInstance);
      if (this.registeredInstances.Count == 0)
        this.applicationModel.AttachedApplications.Remove(this);
      this.RefreshDisplay();
    }

    private void RefreshDisplay()
    {
      this.NotifyOfPropertyChange((Func<object>) (() => (object) this.DisplayName));
      this.NotifyOfPropertyChange((Func<object>) (() => (object) this.Details));
    }
  }
}
