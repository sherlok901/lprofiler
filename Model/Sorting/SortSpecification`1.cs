﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sorting.SortSpecification`1
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace HibernatingRhinos.Profiler.Client.Model.Sorting
{
  public class SortSpecification<T>
  {
    private readonly ITrackingService trackingService;
    private Func<T, KeyValuePair<string, object>> sortColumnResolver;
    private string columnName;

    public Func<T, KeyValuePair<string, object>> SortColumnResolver
    {
      get
      {
        return this.sortColumnResolver;
      }
      set
      {
        if (value != null)
          this.IsUndefined = false;
        if (this.sortColumnResolver == value)
        {
          this.ChangeDirection();
        }
        else
        {
          this.sortColumnResolver = value;
          this.Direction = ListSortDirection.Descending;
        }
      }
    }

    public string ColumnName
    {
      get
      {
        return this.columnName;
      }
      set
      {
        this.columnName = value;
        if (string.IsNullOrEmpty(this.columnName) || this.IsUndefined)
          return;
        this.trackingService.Track("Sorting", this.columnName, (string) null, new int?());
      }
    }

    public ListSortDirection Direction { get; set; }

    public bool IsUndefined { get; set; }

    public bool? IsSortedDescendingByShortSql
    {
      get
      {
        if (!this.ColumnName.Equals("ShortSql"))
          return new bool?();
        return new bool?(this.ColumnName.Equals("ShortSql") && this.Direction == ListSortDirection.Descending);
      }
    }

    public bool? IsSortedDescendingByQueryCount
    {
      get
      {
        if (!this.ColumnName.Equals("QueryCount"))
          return new bool?();
        return new bool?(this.ColumnName.Equals("QueryCount") && this.Direction == ListSortDirection.Descending);
      }
    }

    public bool? IsSortedDescendingByAvgDuration
    {
      get
      {
        if (!this.ColumnName.Equals("AvgDuration"))
          return new bool?();
        return new bool?(this.ColumnName.Equals("AvgDuration") && this.Direction == ListSortDirection.Descending);
      }
    }

    public bool? IsSortedDescendingByRowCount
    {
      get
      {
        if (!this.ColumnName.Equals("RowCount"))
          return new bool?();
        return new bool?(this.ColumnName.Equals("RowCount") && this.Direction == ListSortDirection.Descending);
      }
    }

    public bool? IsSortedDescendingByDuration
    {
      get
      {
        if (!this.ColumnName.Equals("Duration"))
          return new bool?();
        return new bool?(this.ColumnName.Equals("Duration") && this.Direction == ListSortDirection.Descending);
      }
    }

    public bool? IsSortedDescendingByAlerts
    {
      get
      {
        if (!this.ColumnName.Equals("Alerts"))
          return new bool?();
        return new bool?(this.ColumnName.Equals("Alerts") && this.Direction == ListSortDirection.Descending);
      }
    }

    public bool? IsSortedDescendingByCount
    {
      get
      {
        if (!this.ColumnName.Equals("Count"))
          return new bool?();
        return new bool?(this.ColumnName.Equals("Count") && this.Direction == ListSortDirection.Descending);
      }
    }

    public bool IsSortedByShortSql
    {
      get
      {
        return this.ColumnName.Equals("ShortSql");
      }
    }

    public bool IsSortedByQueryCount
    {
      get
      {
        return this.ColumnName.Equals("QueryCount");
      }
    }

    public bool IsSortedByAvgDuration
    {
      get
      {
        return this.ColumnName.Equals("AvgDuration");
      }
    }

    public bool IsSortedByRowCount
    {
      get
      {
        return this.ColumnName.Equals("RowCount");
      }
    }

    public bool IsSortedByDuration
    {
      get
      {
        return this.ColumnName.Equals("Duration");
      }
    }

    public bool IsSortedByAlerts
    {
      get
      {
        return this.ColumnName.Equals("Alerts");
      }
    }

    public bool? IsSortedByCount
    {
      get
      {
        return new bool?(this.ColumnName.Equals("Count"));
      }
    }

    public SortSpecification(ITrackingService trackingService)
    {
      this.trackingService = trackingService;
      this.ColumnName = "";
      this.IsUndefined = true;
    }

    public void ChangeDirection()
    {
      if (this.Direction == ListSortDirection.Ascending)
        this.Direction = ListSortDirection.Descending;
      else
        this.Direction = ListSortDirection.Ascending;
    }
  }
}
