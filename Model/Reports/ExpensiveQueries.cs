﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Reports.ExpensiveQueries
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Model.Filtering;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Reports
{
  [Serializable]
  public class ExpensiveQueries : Report<IEnumerable<QueryAggregationSnapshot>>
  {
    private ObservableCollection<QueryAggregation> unfilteredQueries = new ObservableCollection<QueryAggregation>();
    private QueryAggregation currentQueryAggregation;

    public QueryAggregation CurrentQueryAggregation
    {
      get
      {
        if (this.currentQueryAggregation == null)
        {
          this.currentQueryAggregation = Enumerable.FirstOrDefault<QueryAggregation>((IEnumerable<QueryAggregation>) this.FilteredQueries);
          if (this.currentQueryAggregation != null)
            this.currentQueryAggregation.IsSelected = true;
        }
        return this.currentQueryAggregation;
      }
      set
      {
        if (this.currentQueryAggregation != null)
          this.currentQueryAggregation.IsSelected = false;
        this.currentQueryAggregation = value;
        if (this.currentQueryAggregation == null)
          return;
        this.currentQueryAggregation.IsSelected = true;
      }
    }

    public ObservableCollection<QueryAggregation> FilteredQueries
    {
      get
      {
        if (this.CurrentSort.IsUndefined)
          return new ObservableCollection<QueryAggregation>((IEnumerable<QueryAggregation>) Enumerable.OrderByDescending<QueryAggregation, double?>(Enumerable.Where<QueryAggregation>((IEnumerable<QueryAggregation>) this.unfilteredQueries, (Func<QueryAggregation, bool>) (x => this.filterService.FilterStatement((IFilterableStatementSnapshot) x))), (Func<QueryAggregation, double?>) (x => x.AverageDuration)));
        return new ObservableCollection<QueryAggregation>(Enumerable.Where<QueryAggregation>((IEnumerable<QueryAggregation>) this.unfilteredQueries, (Func<QueryAggregation, bool>) (x => this.filterService.FilterStatement((IFilterableStatementSnapshot) x))));
      }
    }

    public override bool FilteringSupported
    {
      get
      {
        return true;
      }
    }

    public override string JsonReportName
    {
      get
      {
        return "expensiveQueries";
      }
    }

    public ExpensiveQueries(Func<IEnumerable<QueryAggregationSnapshot>> getSnapshot, FilterServiceModel filterservice, ITrackingService trackingService)
      : base(trackingService, getSnapshot, filterservice)
    {
      this.DisplayName = "Expensive Queries";
      this.unfilteredQueries.CollectionChanged += new NotifyCollectionChangedEventHandler(this.UnfilteredQueriesOnCollectionChanged);
    }

    private void UnfilteredQueriesOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
    {
      this.SortBySelectedColumn();
    }

    public override void Clear()
    {
      this.unfilteredQueries.Clear();
      this.CurrentQueryAggregation = (QueryAggregation) null;
    }

    protected override void Update(IEnumerable<QueryAggregationSnapshot> snapshots)
    {
      this.RemoveNoLongerPresent(snapshots);
      foreach (QueryAggregationSnapshot aggregationSnapshot in snapshots)
      {
        QueryAggregationSnapshot snapshot = aggregationSnapshot;
        QueryAggregation queryAggregation = Enumerable.FirstOrDefault<QueryAggregation>(Enumerable.Where<QueryAggregation>((IEnumerable<QueryAggregation>) this.unfilteredQueries, (Func<QueryAggregation, bool>) (query => query.RawSql == snapshot.RawSql)));
        if (queryAggregation != null)
        {
          int count = queryAggregation.Count;
          double? averageDuration1 = queryAggregation.AverageDuration;
          queryAggregation.UpdateFrom(snapshot);
          if (count == queryAggregation.Count)
          {
            double? nullable = averageDuration1;
            double? averageDuration2 = queryAggregation.AverageDuration;
            if ((nullable.GetValueOrDefault() != averageDuration2.GetValueOrDefault() ? 1 : (nullable.HasValue != averageDuration2.HasValue ? 1 : 0)) == 0)
              continue;
          }
          this.SortBySelectedColumn();
        }
        else
          this.unfilteredQueries.Add(QueryAggregation.CreateFrom(snapshot));
      }
    }

    private void RemoveNoLongerPresent(IEnumerable<QueryAggregationSnapshot> snapshots)
    {
      IList<QueryAggregation> list = (IList<QueryAggregation>) new List<QueryAggregation>();
      foreach (QueryAggregation queryAggregation in (Collection<QueryAggregation>) this.unfilteredQueries)
      {
        QueryAggregation query = queryAggregation;
        if (Enumerable.FirstOrDefault<QueryAggregationSnapshot>(Enumerable.Where<QueryAggregationSnapshot>(snapshots, (Func<QueryAggregationSnapshot, bool>) (snapshot => snapshot.RawSql == query.RawSql))) == null)
          list.Add(query);
      }
      foreach (QueryAggregation queryAggregation in (IEnumerable<QueryAggregation>) list)
        this.unfilteredQueries.Remove(queryAggregation);
    }

    public override XElement ExportReport()
    {
      return new XElement(Report.Namespace + "expensive-queries", (object) Enumerable.Select<QueryAggregation, XElement>((IEnumerable<QueryAggregation>) this.unfilteredQueries, (Func<QueryAggregation, XElement>) (x => new XElement(Report.Namespace + "query", new object[7]
      {
        (object) new XAttribute((XName) "count", (object) x.Count),
        (object) XmlExportExtensions.ToXml(x.Duration),
        (object) XmlExportExtensions.ToXml(x.AverageDuration, "average-duration"),
        (object) new XAttribute((XName) "is-cached", x.IsCached ? (object) true : (object) false),
        (object) new XAttribute((XName) "is-ddl", x.IsDDL ? (object) true : (object) false),
        (object) new XElement(Report.Namespace + "short-sql", (object) x.ShortSql),
        (object) new XElement(Report.Namespace + "formatted-sql", (object) x.FormattedSql)
      }))));
    }

    [CLSCompliant(false)]
    protected override JContainer ExportReportToJson()
    {
      return (JContainer) new JArray((object) Enumerable.Select<QueryAggregation, JObject>((IEnumerable<QueryAggregation>) this.unfilteredQueries, (Func<QueryAggregation, JObject>) (x => new JObject(new object[6]
      {
        (object) new JProperty("averageDuration", (object) x.AverageDuration),
        (object) new JProperty("count", (object) x.Count),
        (object) new JProperty("isCached", x.IsCached ? (object) true : (object) false),
        (object) new JProperty("isDdl", x.IsDDL ? (object) true : (object) false),
        (object) new JProperty("shortSql", (object) x.ShortSql),
        (object) new JProperty("formattedSql", (object) x.FormattedSql)
      }))));
    }

    public void Sort(string columnName)
    {
      this.CurrentSort.SortColumnResolver = this.columnResolvers.Resolve(columnName);
      this.CurrentSort.ColumnName = columnName;
      this.unfilteredQueries = new ObservableCollection<QueryAggregation>(this.sorter.Sort((IList<QueryAggregation>) this.unfilteredQueries, this.CurrentSort));
      this.unfilteredQueries.CollectionChanged += new NotifyCollectionChangedEventHandler(this.UnfilteredQueriesOnCollectionChanged);
    }

    public void SortBySelectedColumn()
    {
      if (this.CurrentSort.IsUndefined)
        return;
      this.unfilteredQueries = new ObservableCollection<QueryAggregation>(this.sorter.Sort((IList<QueryAggregation>) this.unfilteredQueries, this.CurrentSort));
      this.unfilteredQueries.CollectionChanged += new NotifyCollectionChangedEventHandler(this.UnfilteredQueriesOnCollectionChanged);
    }
  }
}
