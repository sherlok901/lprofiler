﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Statements.IStatementModel
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace HibernatingRhinos.Profiler.Client.Model.Statements
{
  public interface IStatementModel : IFilterableStatementSnapshot, INotifyPropertyChanged
  {
    Guid SessionId { get; }

    string Url { get; }

    bool HasWarnings { get; }

    bool HasSuggestions { get; }

    string Prefix { get; }

    string Text { get; }

    string ShortText { get; }

    bool IsSelected { get; set; }

    StackTraceInfo StackTrace { get; }

    QueryDurationModel DurationModel { get; }

    bool IsSelectStatement { get; }

    int[] CountOfRows { get; }

    string RowCountDescription { get; }

    string ShortUrl { get; set; }

    IEnumerable<DisplayParameter> Parameters { get; }

    ObservableCollection<StatementAlert> Alerts { get; }

    Guid Id { get; }

    bool IsAggregate { get; set; }

    DateTime Timestamp { get; }

    bool UpdateWith(StatementSnapshot statementSnapshot);

    bool MatchWith(IStatementModel statement);

    void UnrootStatement();

    void RootStatement(IStatementModel statement);

    void CopyToClipboard();

    void ShortenUrl(int maxLength);
  }
}
