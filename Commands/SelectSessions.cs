﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Commands.SelectSessions
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Sessions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Commands
{
  public class SelectSessions
  {
    private IBackendBridge backendBridge;
    private readonly ApplicationModel applicationModel;

    public SelectSessions(IBackendBridge backendBridge, ApplicationModel applicationModel)
    {
      this.backendBridge = backendBridge;
      this.applicationModel = applicationModel;
    }

    public void Execute(SessionPresenter model, IList selectedItems)
    {
      IOrderedEnumerable<ISessionPresenterItem> orderedEnumerable = Enumerable.OrderBy<ISessionPresenterItem, int>(Enumerable.OfType<ISessionPresenterItem>((IEnumerable) selectedItems), (Func<ISessionPresenterItem, int>) (x => this.GetStatementIndex(model, x)));
      this.SetSelectedSessions(model, Enumerable.ToArray<ISessionPresenterItem>((IEnumerable<ISessionPresenterItem>) orderedEnumerable));
    }

    public int GetStatementIndex(SessionPresenter model, ISessionPresenterItem statementModel)
    {
      return model.Items.IndexOf(statementModel);
    }

    public void SetSelectedSessions(SessionPresenter model, ISessionPresenterItem[] selection)
    {
      this.applicationModel.TrackingService.Track("Main", "Selection", "Sessions", new int?(selection.Length));
      switch (selection.Length)
      {
        case 0:
          model.SelectedSession = (ISessionPresenterItem) null;
          break;
        case 1:
          model.SelectedSession = selection[0];
          break;
        default:
          if (selection.Length <= 1)
            break;
          SessionModel[] sessions = Enumerable.ToArray<SessionModel>(Enumerable.OfType<SessionModel>((IEnumerable) selection));
          model.SelectedSession = (ISessionPresenterItem) new MultiSessionSelection(this.backendBridge, sessions, this.applicationModel);
          foreach (ISessionPresenterItem sessionPresenterItem in selection)
            sessionPresenterItem.Activate();
          break;
      }
    }
  }
}
