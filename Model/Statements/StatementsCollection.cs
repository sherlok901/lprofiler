﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Statements.StatementsCollection
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model;
using System.Collections.Generic;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Statements
{
  public class StatementsCollection : PropertyChangedBase, IExposeSelectableStatements
  {
    private IStatementModel selectedStatement;

    public List<IStatementModel> Statements { get; set; }

    public string DisplayName { get; set; }

    public IStatementModel SelectedStatement
    {
      get
      {
        return this.selectedStatement;
      }
      set
      {
        this.selectedStatement = value;
        this.NotifyOfPropertyChange("SelectedStatement");
      }
    }

    public int StatementsCount
    {
      get
      {
        return this.Statements.Count;
      }
    }

    public StatementsCollection(string name, IEnumerable<IStatementModel> statementModels)
    {
      this.Statements = new List<IStatementModel>(statementModels);
      this.DisplayName = string.Concat(new object[4]
      {
        (object) name,
        (object) " ( ",
        (object) this.Statements.Count,
        (object) " )"
      });
      this.SelectedStatement = Enumerable.FirstOrDefault<IStatementModel>((IEnumerable<IStatementModel>) this.Statements);
    }
  }
}
