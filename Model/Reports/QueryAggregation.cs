﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Reports.QueryAggregation
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Client.Model.Reports
{
  public class QueryAggregation : SelectionBase, IFilterableStatementSnapshot
  {
    public int Count { get; set; }

    public double? AverageDuration { get; set; }

    public string RawSql { get; set; }

    public QueryDuration Duration
    {
      get
      {
        QueryDuration queryDuration1 = new QueryDuration();
        QueryDuration queryDuration2 = queryDuration1;
        double? averageDuration1 = this.AverageDuration;
        int? nullable1 = averageDuration1.HasValue ? new int?((int) averageDuration1.GetValueOrDefault()) : new int?();
        queryDuration2.InDatabase = nullable1;
        QueryDuration queryDuration3 = queryDuration1;
        double? averageDuration2 = this.AverageDuration;
        int? nullable2 = averageDuration2.HasValue ? new int?((int) averageDuration2.GetValueOrDefault()) : new int?();
        queryDuration3.InNHibernate = nullable2;
        return queryDuration1;
      }
    }

    public bool IsCached { get; set; }

    public bool IsDDL { get; set; }

    public bool IsTransaction { get; set; }

    public int SessionStatementCount
    {
      get
      {
        return -1;
      }
    }

    public string ShortSql { get; set; }

    public string FormattedSql { get; set; }

    public IList<RelatedSessionSpecification> RelatedSessions { get; set; }

    public void UpdateFrom(QueryAggregationSnapshot statement)
    {
      this.Count = statement.Count;
      this.AverageDuration = statement.AverageDuration;
      this.RelatedSessions = (IList<RelatedSessionSpecification>) statement.RelatedSessions;
    }

    public static QueryAggregation CreateFrom(QueryAggregationSnapshot statement)
    {
      return new QueryAggregation()
      {
        AverageDuration = statement.AverageDuration,
        Count = statement.Count,
        FormattedSql = statement.FormattedSql,
        RawSql = statement.RawSql,
        ShortSql = statement.ShortSql,
        IsCached = statement.Options.HasFlag((Enum) SqlStatementOptions.Cached),
        IsDDL = statement.Options.HasFlag((Enum) SqlStatementOptions.DDL),
        IsTransaction = statement.Options.HasFlag((Enum) SqlStatementOptions.Transaction) || statement.Options.HasFlag((Enum) SqlStatementOptions.DtcTransaction),
        RelatedSessions = (IList<RelatedSessionSpecification>) statement.RelatedSessions
      };
    }
  }
}
