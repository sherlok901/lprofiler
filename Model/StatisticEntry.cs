﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.StatisticEntry
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client.Model
{
  public class StatisticEntry : PropertyChangedBase
  {
    private object value;

    public string Label { get; private set; }

    public object Value
    {
      get
      {
        return this.value;
      }
      set
      {
        this.value = value;
        this.NotifyOfPropertyChange((Func<object>) (() => this.Value));
      }
    }

    public StatisticEntry(string label)
    {
      this.Label = label;
    }
  }
}
