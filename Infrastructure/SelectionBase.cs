﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.SelectionBase
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model;
using System;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public abstract class SelectionBase : PropertyChangedBase
  {
    [NonSerialized]
    private bool isSelected;
    [NonSerialized]
    private string displayName;

    public bool IsSelected
    {
      get
      {
        return this.isSelected;
      }
      set
      {
        this.isSelected = value;
        this.NotifyOfPropertyChange((Func<object>) (() => (object) (this.IsSelected ? 1 : 0)));
      }
    }

    public string DisplayName
    {
      get
      {
        return this.displayName;
      }
      set
      {
        this.displayName = value;
        this.NotifyOfPropertyChange((Func<object>) (() => (object) this.DisplayName));
      }
    }
  }
}
