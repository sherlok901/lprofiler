﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Reports.JsonExtensions
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;

namespace HibernatingRhinos.Profiler.Client.Model.Reports
{
  public static class JsonExtensions
  {
    [CLSCompliant(false)]
    public static void WriteJson(this JToken obj, TextWriter writer)
    {
      JsonTextWriter jsonTextWriter1 = new JsonTextWriter(writer);
      jsonTextWriter1.Indentation = 1;
      jsonTextWriter1.IndentChar = '\t';
      jsonTextWriter1.Formatting = Formatting.Indented;
      JsonTextWriter jsonTextWriter2 = jsonTextWriter1;
      obj.WriteTo((JsonWriter) jsonTextWriter2);
      jsonTextWriter2.Flush();
      writer.Flush();
    }
  }
}
