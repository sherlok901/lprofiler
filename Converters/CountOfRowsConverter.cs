﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Converters.CountOfRowsConverter
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;
using System.Globalization;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Converters
{
  public class CountOfRowsConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return (object) CountOfRowsConverter.Write((int[]) value);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }

    public static string Write(int[] list)
    {
      if (list == null || list.Length == 0)
        return "";
      if (list.Length == 1)
        return list[0].ToString((IFormatProvider) CultureInfo.InvariantCulture);
      StringBuilder stringBuilder = new StringBuilder();
      foreach (int num in list)
        stringBuilder.Append(num).Append(" / ");
      stringBuilder.Length = stringBuilder.Length - 3;
      return stringBuilder.ToString();
    }
  }
}
