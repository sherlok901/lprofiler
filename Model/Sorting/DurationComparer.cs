﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sorting.DurationComparer
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using System;
using System.Collections;
using System.ComponentModel;

namespace HibernatingRhinos.Profiler.Client.Model.Sorting
{
  public class DurationComparer : IComparer
  {
    private ListSortDirection direction;

    public DurationComparer(ListSortDirection direction)
    {
      this.direction = direction;
    }

    public int Compare(object x, object y)
    {
      QueryDuration queryDuration1 = x as QueryDuration;
      QueryDuration queryDuration2 = y as QueryDuration;
      if (queryDuration1 == null || queryDuration2 == null)
        throw new InvalidOperationException("Object type must be QueryDuration");
      return (queryDuration1.InDatabase.HasValue || queryDuration1.InNHibernate.HasValue ? (!queryDuration1.InNHibernate.HasValue ? queryDuration1.InDatabase ?? 0 : queryDuration1.InNHibernate.Value) : (this.direction == ListSortDirection.Ascending ? int.MaxValue : -1)) - (queryDuration2.InDatabase.HasValue || queryDuration2.InNHibernate.HasValue ? (!queryDuration2.InNHibernate.HasValue ? queryDuration2.InDatabase ?? 0 : queryDuration2.InNHibernate.Value) : (this.direction == ListSortDirection.Ascending ? int.MaxValue : -1));
    }
  }
}
