﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Commands.RemoveSession
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model;

namespace HibernatingRhinos.Profiler.Client.Commands
{
  public class RemoveSession
  {
    private readonly ApplicationModel _applicationModel;

    public RemoveSession(ApplicationModel applicationModel)
    {
      this._applicationModel = applicationModel;
    }

    public void Execute(bool affectUI)
    {
      this._applicationModel.TrackingService.Track("Sessions", "Remove", (string) null, new int?());
      this._applicationModel.SessionPresenter.RemoveSelection();
    }

    public bool CanExecute(bool affectUI)
    {
      if (affectUI)
        return this._applicationModel.SessionPresenter.CanRemoveSelection();
      return true;
    }
  }
}
