﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sessions.MultiSessionSelection
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.Client.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Sessions
{
  public class MultiSessionSelection : ISessionPresenterItem, IDiffableSelection
  {
    private readonly AggregateStatements view;
    private readonly SessionModel[] sessions;

    public string Name
    {
      get
      {
        return "Selection";
      }
    }

    public IList<PollingBase> Views
    {
      get
      {
        return (IList<PollingBase>) new List<PollingBase>((IEnumerable<PollingBase>) new AggregateStatements[1]
        {
          this.view
        });
      }
    }

    public PollingBase CurrentView
    {
      get
      {
        return (PollingBase) this.view;
      }
      set
      {
      }
    }

    public IExposeSelectableStatements StatementsModel
    {
      get
      {
        return (IExposeSelectableStatements) this.view;
      }
    }

    public DateTime Timestamp
    {
      get
      {
        return Enumerable.First<SessionModel>((IEnumerable<SessionModel>) this.sessions).Timestamp;
      }
    }

    public bool CanDiff
    {
      get
      {
        return this.sessions.Length == 2;
      }
    }

    public ISessionPresenterItem First
    {
      get
      {
        return (ISessionPresenterItem) this.sessions[0];
      }
    }

    public ISessionPresenterItem Second
    {
      get
      {
        return (ISessionPresenterItem) this.sessions[1];
      }
    }

    public MultiSessionSelection(IBackendBridge bridge, SessionModel[] sessions, ApplicationModel applicationModel)
    {
      this.sessions = sessions;
      this.view = new AggregateStatements(applicationModel, Enumerable.ToArray<Session>(Enumerable.Select<SessionModel, Session>((IEnumerable<SessionModel>) sessions, (Func<SessionModel, Session>) (x => x.Session))));
      this.view.Activate();
    }

    public void Activate()
    {
    }

    public void Deactivate()
    {
    }

    public ISessionPresenterItem RemoveFrom(IList<ISessionPresenterItem> items)
    {
      foreach (SessionModel sessionModel in this.sessions)
        items.Remove((ISessionPresenterItem) sessionModel);
      return (ISessionPresenterItem) null;
    }
  }
}
