﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sorting.AlertsComparer
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model.Statements;
using System;
using System.Collections;
using System.Collections.ObjectModel;

namespace HibernatingRhinos.Profiler.Client.Model.Sorting
{
  public class AlertsComparer : IComparer
  {
    public int Compare(object x, object y)
    {
      ObservableCollection<StatementAlert> observableCollection1 = x as ObservableCollection<StatementAlert>;
      ObservableCollection<StatementAlert> observableCollection2 = y as ObservableCollection<StatementAlert>;
      if (observableCollection1 == null && observableCollection2 == null)
        throw new InvalidOperationException("Object type must by ObservableCollection<StatementAlert>");
      int num1 = 0;
      foreach (StatementAlert statementAlert in (Collection<StatementAlert>) observableCollection1)
        num1 = (int) statementAlert.Severity;
      int num2 = 0;
      foreach (StatementAlert statementAlert in (Collection<StatementAlert>) observableCollection2)
        num2 = (int) statementAlert.Severity;
      return num1 - num2;
    }
  }
}
