﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sessions.AggregateStatements
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Sorting;
using HibernatingRhinos.Profiler.Client.Model.Statements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Sessions
{
  public class AggregateStatements : SessionPollingBase<IEnumerable<StatementSnapshot>>, IExposeSelectableStatements
  {
    private IStatementModel selectedStatement;
    private readonly ApplicationModel applicationModel;
    private readonly Sorter<IStatementModel> sorter;
    private readonly ColumnResolversRegistry<IStatementModel> columnResolvers;
    private readonly List<IStatementModel> unfilteredStatements;

    public List<IStatementModel> Statements
    {
      get
      {
        IEnumerable<IStatementModel> enumerable = Enumerable.Where<IStatementModel>((IEnumerable<IStatementModel>) this.unfilteredStatements, (Func<IStatementModel, bool>) (x => this.applicationModel.FilterService.FilterStatement((IFilterableStatementSnapshot) x)));
        if (this.CurrentSort.IsUndefined)
          return new List<IStatementModel>(enumerable);
        return new List<IStatementModel>((IEnumerable<IStatementModel>) this.sorter.Sort((IList<IStatementModel>) Enumerable.ToList<IStatementModel>(enumerable), this.CurrentSort));
      }
    }

    public IStatementModel SelectedStatement
    {
      get
      {
        if (this.selectedStatement == null && this.Statements.Count > 0)
          this.SelectedStatement = this.Statements[0];
        return this.selectedStatement;
      }
      set
      {
        if (this.selectedStatement == value)
          return;
        foreach (IStatementModel statementModel in this.Statements)
          statementModel.IsSelected = false;
        this.selectedStatement = value;
        if (this.selectedStatement == null)
          return;
        this.selectedStatement.IsSelected = true;
      }
    }

    public SortSpecification<IStatementModel> CurrentSort { get; set; }

    public AggregateStatements(ApplicationModel applicationModel, Session[] sessions)
      : base((Func<Session, IEnumerable<StatementSnapshot>>) (session => (IEnumerable<StatementSnapshot>) session.GetStatementsSnapshots()), sessions)
    {
      this.CurrentSort = new SortSpecification<IStatementModel>(applicationModel.TrackingService)
      {
        Direction = ListSortDirection.Descending
      };
      this.unfilteredStatements = new List<IStatementModel>();
      this.applicationModel = applicationModel;
      this.sorter = new Sorter<IStatementModel>();
      this.columnResolvers = new ColumnResolversRegistry<IStatementModel>();
      this.columnResolvers.Register("ShortSql", (Func<IStatementModel, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("ShortSql", (object) x.ShortText)));
      this.columnResolvers.Register("RowCount", (Func<IStatementModel, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("RowCount", (object) x.CountOfRows)));
      this.columnResolvers.Register("Duration", (Func<IStatementModel, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("Duration", (object) x.Duration)));
      this.columnResolvers.Register("Alerts", (Func<IStatementModel, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("Alerts", (object) x.Alerts)));
      this.DonePolling();
    }

    protected override void Update(IEnumerable<StatementSnapshot> snapshots)
    {
      foreach (StatementSnapshot statementSnapshot1 in snapshots)
      {
        StatementSnapshot statementSnapshot = statementSnapshot1;
        IStatementModel statementModel = Enumerable.FirstOrDefault<IStatementModel>(Enumerable.Where<IStatementModel>((IEnumerable<IStatementModel>) this.Statements, (Func<IStatementModel, bool>) (model => model.Id == statementSnapshot.StatementId)));
        if (statementModel == null)
          this.unfilteredStatements.Add((IStatementModel) new StatementModel(statementSnapshot)
          {
            ApplicationModel = this.applicationModel,
            CanJumpToSession = false
          });
        else
          statementModel.UpdateWith(statementSnapshot);
      }
    }

    protected override void OnActivate()
    {
      this.applicationModel.TrackingService.Track("Sessions", "Aggregate Sessions", (string) null, new int?());
    }

    public void Sort(string columnName)
    {
      this.CurrentSort.SortColumnResolver = this.columnResolvers.Resolve(columnName);
      this.CurrentSort.ColumnName = columnName;
    }
  }
}
