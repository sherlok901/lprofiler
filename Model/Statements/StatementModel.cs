﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Statements.StatementModel
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Appender.StackTraces;
using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Messages;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Converters;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Model.Statements
{
  [DebuggerDisplay("{Text}")]
  [Serializable]
  public class StatementModel : SelectionBase, IStatementModel, IFilterableStatementSnapshot, INotifyPropertyChanged
  {
    private IEnumerable<IStatementModel> children;
    private IStatementModel rootStatement;
    private StatementSnapshot snapshot;
    private string shortUrl;
    private bool multiSelection;

    public IEnumerable<IStatementModel> Children
    {
      set
      {
        if (this.children != null)
          EnumerableExtension.ForEach<IStatementModel>(this.children, (Action<IStatementModel>) (x => x.UnrootStatement()));
        this.children = value;
        if (this.children == null)
          return;
        EnumerableExtension.ForEach<IStatementModel>(this.children, (Action<IStatementModel>) (x => x.RootStatement((IStatementModel) this)));
      }
    }

    public Guid SessionId
    {
      get
      {
        return this.snapshot.SessionId;
      }
    }

    public bool CanJumpToSession { get; set; }

    public bool IsAggregate { get; set; }

    public Guid Id
    {
      get
      {
        return this.snapshot.StatementId;
      }
    }

    public int[] CountOfRows
    {
      get
      {
        return this.snapshot.CountOfRows;
      }
    }

    public string RowCountDescription
    {
      get
      {
        return CountOfRowsConverter.Write(this.CountOfRows);
      }
    }

    public string CountOfRowsText
    {
      get
      {
        if (Profile.Current.Supports(SupportedFeatures.RowCount))
          return string.Format("See the {0} row(s) resulting from this statement.", new CountOfRowsConverter().Convert((object) this.CountOfRows, typeof (string), (object) null, CultureInfo.CurrentCulture));
        return "See the row(s) resulting from this statement.";
      }
    }

    public IEnumerable<DisplayParameter> Parameters
    {
      get
      {
        return Enumerable.Select<Parameter, DisplayParameter>(Enumerable.Where<Parameter>(this.snapshot.Parameters, (Func<Parameter, bool>) (x => x != null)), (Func<Parameter, DisplayParameter>) (x => new DisplayParameter()
        {
          Name = x.Name,
          Value = x.Value,
          Type = x.Type
        }));
      }
    }

    public ObservableCollection<StatementAlert> Alerts { get; private set; }

    public DateTime Timestamp
    {
      get
      {
        return this.snapshot.Timestamp;
      }
    }

    public bool HasWarnings
    {
      get
      {
        return Enumerable.Any<StatementAlert>((IEnumerable<StatementAlert>) this.Alerts, (Func<StatementAlert, bool>) (x => x.Severity == Severity.Warning));
      }
    }

    public bool HasSuggestions
    {
      get
      {
        return Enumerable.Any<StatementAlert>((IEnumerable<StatementAlert>) this.Alerts, (Func<StatementAlert, bool>) (x => x.Severity == Severity.Suggestion));
      }
    }

    public int AlertsSeverity
    {
      get
      {
        return Enumerable.Sum<StatementAlert>((IEnumerable<StatementAlert>) this.Alerts, (Func<StatementAlert, int>) (statementAlert => (int) statementAlert.Severity));
      }
    }

    public StackTraceInfo StackTrace
    {
      get
      {
        return this.snapshot.StackTrace;
      }
    }

    public QueryDurationModel DurationModel
    {
      get
      {
        return new QueryDurationModel(this.snapshot.Duration);
      }
    }

    public string Text
    {
      get
      {
        if (string.IsNullOrEmpty(this.Prefix))
          return this.FormattedSql;
        return this.Prefix + Environment.NewLine + this.FormattedSql;
      }
    }

    public string FormattedSql
    {
      get
      {
        return this.snapshot.FormattedSql;
      }
    }

    public string ShortText
    {
      get
      {
        return this.Prefix + this.snapshot.ShortSql;
      }
    }

    public string Url
    {
      get
      {
        return this.snapshot.Url;
      }
    }

    public bool CanExecuteQuery
    {
      get
      {
        return this.snapshot.CanExecuteQuery;
      }
    }

    public bool IsSelectStatement
    {
      get
      {
        return this.snapshot.IsSelectStatement;
      }
    }

    public string RawSql
    {
      get
      {
        return this.snapshot.RawSql;
      }
    }

    public QueryDuration Duration
    {
      get
      {
        return this.snapshot.Duration;
      }
    }

    public bool IsCached
    {
      get
      {
        return this.snapshot.IsCached;
      }
    }

    public bool IsDDL
    {
      get
      {
        return this.snapshot.IsDDL;
      }
    }

    public StatementSnapshot Snapshot
    {
      get
      {
        return this.snapshot;
      }
    }

    public bool IsTransaction
    {
      get
      {
        return this.snapshot.IsTransaction;
      }
    }

    public int SessionStatementCount
    {
      get
      {
        return this.snapshot.SessionStatementCount;
      }
    }

    public string OrmError
    {
      get
      {
        return this.snapshot.OrmError;
      }
    }

    public SolutionItem ErrorSolution
    {
      get
      {
        return this.snapshot.ErrorSolution;
      }
    }

    public string ShortUrl
    {
      get
      {
        if (!string.IsNullOrEmpty(this.shortUrl))
          return this.shortUrl;
        return this.Url;
      }
      set
      {
        this.shortUrl = value;
        this.NotifyOfPropertyChange("ShortUrl");
      }
    }

    public string Prefix
    {
      get
      {
        return this.snapshot.Prefix;
      }
    }

    public bool MultiSelection
    {
      get
      {
        return this.multiSelection;
      }
      set
      {
        this.multiSelection = value;
        this.NotifyOfPropertyChange("MultiSelection");
      }
    }

    public ApplicationModel ApplicationModel { get; set; }

    public StatementModel(StatementSnapshot snapshot)
    {
      this.Alerts = new ObservableCollection<StatementAlert>();
      this.UpdateWith(snapshot);
    }

    public bool UpdateWith(StatementSnapshot statementSnapshot)
    {
      if (statementSnapshot.Equals(this.snapshot))
        return false;
      this.snapshot = statementSnapshot;
      this.Alerts.Clear();
      foreach (AlertInformation alert in this.snapshot.Alerts)
        this.Alerts.Add(new StatementAlert(alert, this));
      this.NotifyOfPropertyChange("");
      return true;
    }

    public bool MatchWith(IStatementModel statement)
    {
      if (this.RawSql != statement.RawSql || this.IsCached != statement.IsCached)
        return false;
      DisplayParameter[] displayParameterArray1 = Enumerable.ToArray<DisplayParameter>(this.Parameters);
      DisplayParameter[] displayParameterArray2 = Enumerable.ToArray<DisplayParameter>(statement.Parameters);
      if (displayParameterArray1.Length != displayParameterArray2.Length)
        return false;
      for (int index = 0; index < displayParameterArray1.Length; ++index)
      {
        if (displayParameterArray1[index].Name != displayParameterArray2[index].Name || displayParameterArray1[index].Value != displayParameterArray2[index].Value)
          return false;
      }
      return true;
    }

    public void UnrootStatement()
    {
      this.rootStatement = (IStatementModel) null;
      if (this.children == null)
        return;
      EnumerableExtension.ForEach<IStatementModel>(this.children, (Action<IStatementModel>) (x => x.UnrootStatement()));
    }

    public void RootStatement(IStatementModel statement)
    {
      this.rootStatement = statement;
    }

    public void CopyToClipboard()
    {
      if (this.rootStatement != null)
        this.rootStatement.CopyToClipboard();
      else
        SafeClipboard.SetText(this.Text);
    }

    public void CopyParametersToClipboard()
    {
      if (this.Parameters == null || !Enumerable.Any<DisplayParameter>(this.Parameters))
        return;
      string str = string.Empty;
      foreach (DisplayParameter displayParameter in this.Parameters)
        str = str + displayParameter.Name + ": " + displayParameter.Value + Environment.NewLine;
      SafeClipboard.SetText(str.Remove(str.LastIndexOf(Environment.NewLine)));
    }

    public void ShortenUrl(int maxLength)
    {
      if (this.Url.Length > maxLength)
      {
        string[] strArray = this.Url.Split('/');
        StringBuilder stringBuilder1 = new StringBuilder("...");
        StringBuilder stringBuilder2 = new StringBuilder();
        StringBuilder stringBuilder3 = new StringBuilder();
        int count = strArray.Length / 2;
        List<string> list1 = Enumerable.ToList<string>(Enumerable.Take<string>((IEnumerable<string>) strArray, count));
        List<string> list2 = Enumerable.ToList<string>(Enumerable.Reverse<string>(Enumerable.Skip<string>((IEnumerable<string>) strArray, count)));
        for (int index = 0; index < Math.Max(list1.Count, list2.Count); ++index)
        {
          if (index < list2.Count && stringBuilder1.Length + list2[index].Length <= maxLength)
          {
            stringBuilder1.Append("/");
            stringBuilder1.Append(list2[index]);
            stringBuilder3.Insert(0, list2[index]);
            stringBuilder3.Insert(0, "/");
          }
          if (index < list1.Count && stringBuilder1.Length + list1[index].Length <= maxLength)
          {
            stringBuilder1.Append("/");
            stringBuilder1.Append(list1[index]);
            stringBuilder2.Append(list1[index]);
            stringBuilder2.Append("/");
          }
        }
        this.ShortUrl = stringBuilder2.Append("...").Append((object) stringBuilder3).ToString();
      }
      else
        this.ShortUrl = this.Url;
    }

    public void JumpToSession()
    {
      this.ApplicationModel.JumpTo((IStatementModel) this);
    }
  }
}
