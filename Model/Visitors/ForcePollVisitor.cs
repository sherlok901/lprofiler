﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Visitors.ForcePollVisitor
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Reports;
using HibernatingRhinos.Profiler.Client.Model.Sessions;
using HibernatingRhinos.Profiler.Client.Model.Statements;
using System;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Client.Model.Visitors
{
  public class ForcePollVisitor : AbstractVisitor
  {
    protected override void Visit(RecentStatementsModel recentStatements)
    {
      base.Visit(recentStatements);
      foreach (PollingBase pollingBase in (IEnumerable<PollingBase>) recentStatements.Views)
        pollingBase.TimerTicked((object) this, EventArgs.Empty);
    }

    protected override void Visit(SessionModel session)
    {
      base.Visit(session);
      foreach (PollingBase pollingBase in (IEnumerable<PollingBase>) session.Views)
        pollingBase.TimerTicked((object) this, EventArgs.Empty);
    }

    protected override void Visit(StatisticsModel statistics)
    {
      statistics.TimerTicked((object) this, EventArgs.Empty);
    }

    protected override void Visit(Report report)
    {
      report.TimerTicked((object) this, EventArgs.Empty);
    }
  }
}
