﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sessions.SessionEntities
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Sessions
{
  public class SessionEntities : PollingBase
  {
    private readonly Session session;
    private readonly ITrackingService trackingService;
    private EntityModel selectedItem;
    private bool updated;

    public ObservableCollection<EntityModel> EntityModels { get; set; }

    public bool DoesNotHaveEntities
    {
      get
      {
        if (this.updated)
          return this.EntityModels.Count < 1;
        return false;
      }
    }

    public EntityModel SelectedItem
    {
      get
      {
        return this.selectedItem;
      }
      set
      {
        if (this.selectedItem != null)
          this.selectedItem.IsSelected = false;
        this.selectedItem = value;
        if (this.selectedItem == null)
          return;
        this.selectedItem.IsSelected = true;
      }
    }

    public SessionEntities(Session session, ITrackingService trackingService)
    {
      this.session = session;
      this.trackingService = trackingService;
      this.EntityModels = new ObservableCollection<EntityModel>();
    }

    protected override void OnActivate()
    {
      this.trackingService.Track("Sessions", "Entities", (string) null, new int?());
    }

    public override void TimerTicked(object sender, EventArgs e)
    {
      foreach (EntitySnapshot entitySnapshot1 in this.session.EntitySnapshots.ToArray())
      {
        EntitySnapshot entitySnapshot = entitySnapshot1;
        EntityModel entityModel = Enumerable.FirstOrDefault<EntityModel>((IEnumerable<EntityModel>) this.EntityModels, (Func<EntityModel, bool>) (model => model.Name == entitySnapshot.Name));
        if (entityModel == null)
          this.EntityModels.Add(new EntityModel(entitySnapshot));
        else
          entityModel.Identifiers = (IList<string>) new List<string>((IEnumerable<string>) entitySnapshot.Identifiers);
      }
      this.updated = true;
    }
  }
}
