﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sessions.SessionStatements
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Filtering;
using HibernatingRhinos.Profiler.Client.Model.Sorting;
using HibernatingRhinos.Profiler.Client.Model.Statements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Sessions
{
  public class SessionStatements : SessionPollingBase<IEnumerable<StatementSnapshot>>, IExposeSelectableStatements
  {
    private readonly IDictionary<Guid, IStatementModel> statementsById = (IDictionary<Guid, IStatementModel>) new Dictionary<Guid, IStatementModel>();
    private readonly FilterServiceModel filter;
    private readonly ApplicationModel applicationModel;
    private readonly IList<IStatementModel> unfilteredStatements;
    private readonly Sorter<IStatementModel> sorter;
    private readonly ColumnResolversRegistry<IStatementModel> columnResolvers;
    private IStatementModel selectedStatement;
    private List<IStatementModel> filteredView;
    private List<IStatementModel> unsortedfilteredView;

    public int StatementCount { get; private set; }

    public List<IStatementModel> Statements
    {
      get
      {
        if (this.filteredView == null)
          this.BuildFilteredView();
        return new List<IStatementModel>((IEnumerable<IStatementModel>) this.filteredView);
      }
    }

    public IStatementModel SelectedStatement
    {
      get
      {
        if (this.selectedStatement == null && this.unfilteredStatements.Count > 0)
          this.SelectedStatement = this.unfilteredStatements[0];
        return this.selectedStatement;
      }
      set
      {
        if (!this.IsSelected || this.selectedStatement == value)
          return;
        foreach (IStatementModel statementModel in (IEnumerable<IStatementModel>) this.unfilteredStatements)
          statementModel.IsSelected = false;
        this.selectedStatement = value;
        if (this.selectedStatement == null)
          return;
        this.selectedStatement.IsSelected = true;
      }
    }

    public SortSpecification<IStatementModel> CurrentSort { get; set; }

    public event Action<List<IStatementModel>> StatementsChanged = param0 => {};

    public SessionStatements(Session session, FilterServiceModel filter, ApplicationModel applicationModel, Func<Session, IEnumerable<StatementSnapshot>> getSnapshot)
      : base(getSnapshot, session)
    {
      this.filter = filter;
      this.applicationModel = applicationModel;
      this.unfilteredStatements = (IList<IStatementModel>) new List<IStatementModel>();
      this.sorter = new Sorter<IStatementModel>();
      this.columnResolvers = new ColumnResolversRegistry<IStatementModel>();
      this.columnResolvers.Register("ShortSql", (Func<IStatementModel, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("ShortSql", (object) x.ShortText)));
      this.columnResolvers.Register("RowCount", (Func<IStatementModel, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("RowCount", (object) x.CountOfRows)));
      this.columnResolvers.Register("Duration", (Func<IStatementModel, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("Duration", (object) x.Duration)));
      this.columnResolvers.Register("Alerts", (Func<IStatementModel, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("Alerts", (object) x.Alerts)));
      this.CurrentSort = new SortSpecification<IStatementModel>(applicationModel.TrackingService)
      {
        Direction = ListSortDirection.Descending
      };
    }

    protected override void OnActivate()
    {
      this.applicationModel.TrackingService.Track("Sessions", "Statements", (string) null, new int?());
    }

    private void HandleStatementsOrFiltersChanging()
    {
      this.BuildFilteredView();
      this.StatementsChanged(this.Statements);
    }

    private void BuildFilteredView()
    {
      this.filteredView = Enumerable.ToList<IStatementModel>(Enumerable.Where<IStatementModel>((IEnumerable<IStatementModel>) this.unfilteredStatements, (Func<IStatementModel, bool>) (x => this.filter.FilterStatement((IFilterableStatementSnapshot) x))));
      this.StatementCount = this.filteredView.Count;
    }

    protected override void Update(IEnumerable<StatementSnapshot> snapshots)
    {
      bool flag = false;
      foreach (StatementSnapshot statementSnapshot in snapshots)
      {
        IStatementModel statementModel1;
        this.statementsById.TryGetValue(statementSnapshot.StatementId, out statementModel1);
        if (statementModel1 == null)
        {
          flag = true;
          StatementModel statementModel2 = new StatementModel(statementSnapshot)
          {
            ApplicationModel = this.applicationModel,
            CanJumpToSession = false
          };
          this.unfilteredStatements.Add((IStatementModel) statementModel2);
          this.statementsById.Add(statementSnapshot.StatementId, (IStatementModel) statementModel2);
        }
        else
          flag |= statementModel1.UpdateWith(statementSnapshot);
      }
      if (!flag)
        return;
      this.HandleStatementsOrFiltersChanging();
    }

    public void Sort(string columnName)
    {
      if (this.CurrentSort.IsUndefined)
        this.unsortedfilteredView = this.filteredView;
      this.CurrentSort.SortColumnResolver = this.columnResolvers.Resolve(columnName);
      this.CurrentSort.ColumnName = columnName;
      this.filteredView = this.sorter.Sort((IList<IStatementModel>) this.filteredView, this.CurrentSort);
      this.StatementsChanged(this.Statements);
    }

    public void CancelSorting()
    {
      this.CurrentSort.IsUndefined = true;
      this.CurrentSort.ColumnName = "";
      this.filteredView = this.unsortedfilteredView;
      this.StatementsChanged(this.Statements);
    }
  }
}
