﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Filtering.FilterBySessionWithStatements
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using System;

namespace HibernatingRhinos.Profiler.Client.Model.Filtering
{
  [Serializable]
  public class FilterBySessionWithStatements : AbstractFilter
  {
    public override bool IsValid
    {
      get
      {
        return true;
      }
    }

    public override string ToolTip
    {
      get
      {
        return "[[sessions]] with statements.";
      }
    }

    public override bool IsValidSession(IFilterableSessionSnapshot snapshot)
    {
      return snapshot.StatementCount > 0;
    }

    public override bool IsValidStatement(IFilterableStatementSnapshot snapshot)
    {
      if (snapshot.SessionStatementCount == -1)
        return true;
      return snapshot.SessionStatementCount > 0;
    }
  }
}
