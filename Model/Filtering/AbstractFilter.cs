﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Filtering.AbstractFilter
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using Newtonsoft.Json;
using System;

namespace HibernatingRhinos.Profiler.Client.Model.Filtering
{
  [Serializable]
  public abstract class AbstractFilter : IFilter
  {
    private bool isActive = true;

    [JsonIgnore]
    public abstract bool IsValid { get; }

    [JsonIgnore]
    public abstract string ToolTip { get; }

    public bool IsActive
    {
      get
      {
        return this.isActive;
      }
      set
      {
        this.isActive = value;
      }
    }

    public string IsActiveTooltip
    {
      get
      {
        return string.Format("{0} filter", this.IsActive ? (object) "Disable" : (object) "Enable");
      }
    }

    public virtual bool IsValidStatement(IFilterableStatementSnapshot snapshot)
    {
      return true;
    }

    public virtual bool IsValidSession(IFilterableSessionSnapshot snapshot)
    {
      return true;
    }
  }
}
