﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.XmlExportExtensions
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.Client.Model.Reports;
using HibernatingRhinos.Profiler.Client.Model.Statements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public static class XmlExportExtensions
  {
    public static XElement ToXml(this QueryDuration queryDuration)
    {
      XElement xelement = new XElement(Report.Namespace + "duration");
      if (queryDuration.InDatabase.HasValue)
        xelement.Add((object) new XAttribute((XName) "database-only", (object) queryDuration.InDatabase));
      if (queryDuration.InNHibernate.HasValue)
        xelement.Add((object) new XAttribute((XName) "with-materialization", (object) queryDuration.InNHibernate));
      return xelement;
    }

    public static XElement ToXml(this IStatementModel y)
    {
      XElement xelement = new XElement(Report.Namespace + "query", new object[6]
      {
        (object) new XAttribute((XName) "is-cached", (object)  (y.IsCached ? 1 : 0)),
        (object) new XAttribute((XName) "is-ddl", (object)  (y.IsDDL ? 1 : 0)),
        (object) XmlExportExtensions.ToXml(y.Duration),
        (object) new XElement(Report.Namespace + "formatted-sql", (object) y.Text),
        (object) new XElement(Report.Namespace + "short-sql", (object) y.ShortText),
        (object) new XElement(Report.Namespace + "alerts", (object) Enumerable.Select<StatementAlert, XElement>((IEnumerable<StatementAlert>) y.Alerts, (Func<StatementAlert, XElement>) (z => XmlExportExtensions.ToXml(z))))
      });
      if (y.CountOfRows.Length != 0)
        xelement.Add((object) new XElement(Report.Namespace + "row-counts", (object) Enumerable.Select<int, XElement>((IEnumerable<int>) y.CountOfRows, (Func<int, XElement>) (i => new XElement(Report.Namespace + "row-count", (object) i)))));
      return xelement;
    }

    public static XElement ToXml(this double? y, string element)
    {
      XElement xelement = new XElement(Report.Namespace + element);
      if (y.HasValue)
        xelement.Add((object) y.ToString());
      return xelement;
    }

    public static XElement ToXml(this StatementAlert z)
    {
      return new XElement(Report.Namespace + "alert", new object[3]
      {
        (object) new XAttribute((XName) "title", (object) z.Title),
        (object) new XAttribute((XName) "severity", (object) z.Severity),
        (object) new XAttribute((XName) "help-topic", (object) z.HelpTopic)
      });
    }
  }
}
