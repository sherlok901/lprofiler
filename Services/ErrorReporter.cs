﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Services.ErrorReporter
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client;
using HibernatingRhinos.Profiler.Client.Host;
using HibernatingRhinos.Profiler.Client.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;

namespace HibernatingRhinos.Profiler.Client.Services
{
  public class ErrorReporter
  {
    private readonly string baseUrl = Profile.Current.ErrorUrl;

    public string ReportError(UserPreferences userSettings, Exception e)
    {
      FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(typeof (ErrorReporter).Assembly.Location);
      Dictionary<string, string> dictionary = new Dictionary<string, string>()
      {
        {
          "UserId",
          userSettings.UserId.ToString()
        },
        {
          "Error",
          (string) (object) e + (object) Environment.NewLine + "----- EOF report from client."
        },
        {
          "UserName",
          Environment.UserName
        },
        {
          "OperationSystem",
          Environment.OSVersion.ToString()
        },
        {
          "ProcessorCount",
          Environment.ProcessorCount.ToString((IFormatProvider) CultureInfo.InvariantCulture)
        },
        {
          "UserEmail",
          this.GetEmail(userSettings.EmailAddress)
        },
        {
          "FrameworkVersion",
          Environment.Version.ToString()
        },
        {
          "Build",
          BuildInfo.GetCurrentBuild() + (object) ":" + (string) (object) versionInfo
        },
        {
          "Reporter",
          "Client"
        },
        {
          "Profile",
          Profile.CurrentProfile
        }
      };
      StringWriter stringWriter = new StringWriter();
      foreach (KeyValuePair<string, string> keyValuePair in dictionary)
      {
        stringWriter.Write(Uri.EscapeDataString(keyValuePair.Key ?? "Null_Key"));
        stringWriter.Write("=");
        stringWriter.Write(Uri.EscapeDataString(keyValuePair.Value ?? "Null_Value"));
        stringWriter.Write("&");
      }
      string str = stringWriter.GetStringBuilder().ToString();
      HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(this.baseUrl);
      httpWebRequest.Method = "POST";
      httpWebRequest.ContentType = "application/x-www-form-urlencoded";
      using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
      {
        streamWriter.Write(str);
        streamWriter.Flush();
      }
      using (WebResponse response = httpWebRequest.GetResponse())
        return new StreamReader(response.GetResponseStream()).ReadToEnd();
    }

    private string GetEmail(string emailAddress)
    {
      if (string.IsNullOrWhiteSpace(emailAddress))
        return "no-provided-email@profiler.com";
      if (!ModelValidator.EmailValidator(emailAddress))
        return "no-valid-email@profiler.com";
      return emailAddress;
    }
  }
}
