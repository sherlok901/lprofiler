﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sorting.CountOfRowsComparer
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;
using System.Collections;
using System.ComponentModel;

namespace HibernatingRhinos.Profiler.Client.Model.Sorting
{
  public class CountOfRowsComparer : IComparer
  {
    private ListSortDirection direction;

    public CountOfRowsComparer(ListSortDirection direction)
    {
      this.direction = direction;
    }

    public int Compare(object x, object y)
    {
      int[] numArray1 = x as int[];
      int[] numArray2 = y as int[];
      if (numArray1 == null || numArray2 == null)
        throw new InvalidOperationException("Object type must be int[]");
      int num1 = 0;
      if (numArray1.Length == 0)
      {
        num1 = this.direction == ListSortDirection.Ascending ? int.MaxValue : -1;
      }
      else
      {
        foreach (int num2 in numArray1)
          num1 += num2;
      }
      int num3 = 0;
      if (numArray2.Length == 0)
      {
        num3 = this.direction == ListSortDirection.Ascending ? int.MaxValue : -1;
      }
      else
      {
        foreach (int num2 in numArray2)
          num3 += num2;
      }
      return num1 - num3;
    }
  }
}
