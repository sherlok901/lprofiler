﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Reports.QueriesByIsolationLevel
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Model.Filtering;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Reports
{
  [Serializable]
  public class QueriesByIsolationLevel : Report<IEnumerable<IsolationLevelQueriesSnapshot>>
  {
    [NonSerialized]
    private IsolationLevelQuery currentIsolationLevel;

    public List<IsolationLevelQuery> StatementsByIsolationLevel { get; private set; }

    public IsolationLevelQuery CurrentIsolationLevel
    {
      get
      {
        if (this.currentIsolationLevel == null)
        {
          this.currentIsolationLevel = Enumerable.FirstOrDefault<IsolationLevelQuery>((IEnumerable<IsolationLevelQuery>) this.StatementsByIsolationLevel);
          if (this.currentIsolationLevel != null)
            this.currentIsolationLevel.IsSelected = true;
        }
        return this.currentIsolationLevel;
      }
      set
      {
        if (value == null || this.currentIsolationLevel == value)
          return;
        if (this.currentIsolationLevel != null)
          this.currentIsolationLevel.IsSelected = false;
        this.currentIsolationLevel = value;
        if (this.currentIsolationLevel == null)
          return;
        this.currentIsolationLevel.IsSelected = true;
      }
    }

    public override bool FilteringSupported
    {
      get
      {
        return true;
      }
    }

    public override string JsonReportName
    {
      get
      {
        return "queriesByIsolationLevel";
      }
    }

    public QueriesByIsolationLevel(Func<IEnumerable<IsolationLevelQueriesSnapshot>> getSnapshot, FilterServiceModel filterService, ITrackingService trackingService)
      : base(trackingService, getSnapshot, filterService)
    {
      this.DisplayName = "Queries by Isolation Level";
      this.StatementsByIsolationLevel = new List<IsolationLevelQuery>();
    }

    protected override void Update(IEnumerable<IsolationLevelQueriesSnapshot> snapshots)
    {
      foreach (IsolationLevelQueriesSnapshot levelQueriesSnapshot in snapshots)
      {
        IsolationLevelQueriesSnapshot snapshot = levelQueriesSnapshot;
        IsolationLevelQuery isolationLevelQuery = Enumerable.FirstOrDefault<IsolationLevelQuery>(Enumerable.Where<IsolationLevelQuery>((IEnumerable<IsolationLevelQuery>) this.StatementsByIsolationLevel, (Func<IsolationLevelQuery, bool>) (shot => shot.IsolationLevel == snapshot.IsolationLevel)));
        if (isolationLevelQuery == null)
          this.StatementsByIsolationLevel.Add(IsolationLevelQuery.CreateFrom(snapshot, this.filterService, this.trackingService));
        else
          isolationLevelQuery.UpdateFrom(snapshot);
      }
    }

    public override void Clear()
    {
      foreach (IsolationLevelQuery isolationLevelQuery in this.StatementsByIsolationLevel)
        isolationLevelQuery.Clear();
      this.StatementsByIsolationLevel.Clear();
      this.currentIsolationLevel = (IsolationLevelQuery) null;
    }

    public override XElement ExportReport()
    {
      int queryCount = 0;
      Dictionary<QueryAggregation, int> indexes = new Dictionary<QueryAggregation, int>();
      Func<QueryAggregation, int> getId = (Func<QueryAggregation, int>) (queryAgg =>
      {
        int num1;
        if (indexes.TryGetValue(queryAgg, out num1))
          return num1;
        Dictionary<QueryAggregation, int> dictionary = indexes;
        QueryAggregation index = queryAgg;
        int num2 = queryCount++;
        int num3;
        int num4 = num3 = num2;
        dictionary[index] = num3;
        return num4;
      });
      return new XElement(Report.Namespace + "queries-by-isolation-level", (object) Enumerable.Select<IsolationLevelQuery, XElement>((IEnumerable<IsolationLevelQuery>) this.StatementsByIsolationLevel, (Func<IsolationLevelQuery, XElement>) (x => new XElement(Report.Namespace + "isolation-level", new object[3]
      {
        (object) new XAttribute((XName) "isolation-level", (object) x.IsolationLevel),
        (object) new XAttribute((XName) "statement-count", (object) x.Statements.Count),
        (object) Enumerable.Select<QueryAggregation, XElement>((IEnumerable<QueryAggregation>) x.Statements, (Func<QueryAggregation, XElement>) (q => new XElement(Report.Namespace + "query", new object[8]
        {
          (object) new XAttribute((XName) "query-id", (object) getId(q)),
          (object) XmlExportExtensions.ToXml(q.Duration),
          (object) XmlExportExtensions.ToXml(q.AverageDuration, "average-duration"),
          (object) new XElement(Report.Namespace + "count", (object) q.Count),
          (object) new XAttribute((XName) "is-cached", (object)  (q.IsCached ? 1 : 0)),
          (object) new XAttribute((XName) "is-ddl", (object)  (q.IsDDL ? 1 : 0)),
          (object) new XElement(Report.Namespace + "short-sql", (object) q.ShortSql),
          (object) new XElement(Report.Namespace + "formatted-sql", (object) q.FormattedSql)
        })))
      }))));
    }

    [CLSCompliant(false)]
    protected override JContainer ExportReportToJson()
    {
      int queryCount = 0;
      Dictionary<QueryAggregation, int> indexes = new Dictionary<QueryAggregation, int>();
      Func<QueryAggregation, int> getId = (Func<QueryAggregation, int>) (queryAgg =>
      {
        int num1;
        if (indexes.TryGetValue(queryAgg, out num1))
          return num1;
        Dictionary<QueryAggregation, int> dictionary = indexes;
        QueryAggregation index = queryAgg;
        int num2 = queryCount++;
        int num3;
        int num4 = num3 = num2;
        dictionary[index] = num3;
        return num4;
      });
      return (JContainer) new JArray((object) Enumerable.Select<IsolationLevelQuery, JObject>((IEnumerable<IsolationLevelQuery>) this.StatementsByIsolationLevel, (Func<IsolationLevelQuery, JObject>) (x => new JObject(new object[3]
      {
        (object) new JProperty("isolationLevel", (object) x.IsolationLevel),
        (object) new JProperty("statementCount", (object) x.Statements.Count),
        (object) new JProperty("queries", (object) new JArray((object) Enumerable.Select<QueryAggregation, JObject>((IEnumerable<QueryAggregation>) x.Statements, (Func<QueryAggregation, JObject>) (q => new JObject(new object[7]
        {
          (object) new JProperty("queryId", (object) getId(q)),
          (object) new JProperty("averageDuration", (object) q.AverageDuration),
          (object) new JProperty("count", (object) q.Count),
          (object) new JProperty("isCached", (object) (q.IsCached ? 1 : 0)),
          (object) new JProperty("isDdl", (object) (q.IsDDL ? 1 : 0)),
          (object) new JProperty("shortSql", (object) q.ShortSql),
          (object) new JProperty("formattedSql", (object) q.FormattedSql)
        })))))
      }))));
    }
  }
}
