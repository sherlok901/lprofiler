﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.PollingBase`1
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client.Model
{
  public abstract class PollingBase<T> : PollingBase
  {
    private readonly Func<Guid, T> getSnapshot;
    private readonly Guid[] sessionIds;

    protected PollingBase(Func<Guid, T> getSnapshot, params Guid[] sessionIds)
    {
      this.sessionIds = sessionIds;
      this.getSnapshot = getSnapshot;
    }

    public override void TimerTicked(object sender, EventArgs e)
    {
      if (this.state == PollingState.Finalized)
      {
        PollingBase.Timer.Tick -= new EventHandler(((PollingBase) this).TimerTicked);
      }
      else
      {
        foreach (Guid guid in this.sessionIds)
          this.Update(this.getSnapshot(guid));
        if (this.state != PollingState.NotFinalized)
          return;
        this.state = PollingState.Finalized;
      }
    }

    protected abstract void Update(T snapshot);

    public void ForceUpdate()
    {
      foreach (Guid guid in this.sessionIds)
        this.Update(this.getSnapshot(guid));
      if (this.state != PollingState.NotFinalized)
        return;
      this.state = PollingState.Finalized;
    }
  }
}
