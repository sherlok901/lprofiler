﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Reports.OverallUsage
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Model.Filtering;
using HibernatingRhinos.Profiler.Client.Model.Sorting;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Reports
{
  [Serializable]
  public class OverallUsage : Report<OverallUsageSnapshot>
  {
    private readonly Sorter<IAggregatedAlertSnapshot> sorter;
    private readonly ColumnResolversRegistry<IAggregatedAlertSnapshot> columnResolvers;
    private IEnumerable<IAggregatedAlertSnapshot> unsortedAlerts;

    public SortSpecification<IAggregatedAlertSnapshot> CurrentSort { get; set; }

    public IDictionary<string, TotalAndAverage> AggregatedEntities { get; set; }

    public IEnumerable<IAggregatedAlertSnapshot> AggregatedAlerts { get; set; }

    public double AverageEntitiesLoadedPerSession { get; set; }

    public int NumberOfEntitiesTypes { get; set; }

    public int NumberOfAlerts { get; set; }

    public double? AverageQueryRowCount { get; set; }

    public int NumberOfTransactionRollbacks { get; set; }

    public int NumberOfTransactionCommits { get; set; }

    public int NumberOfTransactions { get; set; }

    public double AverageStatementsPerSession { get; set; }

    public int CountOfSessions { get; set; }

    public override string JsonReportName
    {
      get
      {
        return "overallUsage";
      }
    }

    public OverallUsage(Func<OverallUsageSnapshot> getSnapshot, ITrackingService trackingService)
      : base(trackingService, getSnapshot, (FilterServiceModel) null)
    {
      this.DisplayName = "Overall Usage";
      this.AggregatedAlerts = (IEnumerable<IAggregatedAlertSnapshot>) new IAggregatedAlertSnapshot[0];
      this.unsortedAlerts = (IEnumerable<IAggregatedAlertSnapshot>) new IAggregatedAlertSnapshot[0];
      this.AggregatedEntities = (IDictionary<string, TotalAndAverage>) new Dictionary<string, TotalAndAverage>();
      this.sorter = new Sorter<IAggregatedAlertSnapshot>();
      this.columnResolvers = new ColumnResolversRegistry<IAggregatedAlertSnapshot>();
      this.columnResolvers.Register("Count", (Func<IAggregatedAlertSnapshot, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("Count", (object) x.Count)));
      this.columnResolvers.Register("Alerts", (Func<IAggregatedAlertSnapshot, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("Alerts", (object) x.Alert.Title)));
      this.CurrentSort = new SortSpecification<IAggregatedAlertSnapshot>(trackingService)
      {
        Direction = ListSortDirection.Descending
      };
    }

    protected override void Update(OverallUsageSnapshot snapshot)
    {
      this.unsortedAlerts = this.AggregatedAlerts = (IEnumerable<IAggregatedAlertSnapshot>) (snapshot.AggregatedAlerts ?? new IAggregatedAlertSnapshot[0]);
      this.AggregatedEntities = (IDictionary<string, TotalAndAverage>) (snapshot.AggregatedEntities ?? new Dictionary<string, TotalAndAverage>());
      this.AverageEntitiesLoadedPerSession = snapshot.AverageEntitiesLoadedPerSession;
      this.AverageQueryRowCount = snapshot.AverageQueryRowCount;
      this.AverageStatementsPerSession = snapshot.AverageStatementsPerSession;
      this.CountOfSessions = snapshot.CountOfSessions;
      this.NumberOfAlerts = snapshot.NumberOfAlerts;
      this.NumberOfEntitiesTypes = snapshot.NumberOfEntitiesTypes;
      this.NumberOfTransactionCommits = snapshot.NumberOfTransactionCommits;
      this.NumberOfTransactionRollbacks = snapshot.NumberOfTransactionRollbacks;
      this.NumberOfTransactions = snapshot.NumberOfTransactions;
      this.SortBySelectedColumn();
    }

    public override void Clear()
    {
    }

    public override XElement ExportReport()
    {
      XElement xelement = new XElement(Report.Namespace + "overall-usage", new object[10]
      {
        (object) new XElement(Report.Namespace + "aggregated-alerts", (object) Enumerable.Select<IAggregatedAlertSnapshot, XElement>(this.AggregatedAlerts, (Func<IAggregatedAlertSnapshot, XElement>) (x => new XElement(Report.Namespace + "alert", new object[4]
        {
          (object) new XAttribute((XName) "title", (object) x.Alert.Title),
          (object) new XAttribute((XName) "help-topic", (object) x.Alert.HelpTopic),
          (object) new XAttribute((XName) "severity", (object) x.Alert.Severity),
          (object) new XAttribute((XName) "count", (object) x.Count)
        })))),
        (object) new XElement(Report.Namespace + "aggregated-entities", (object) Enumerable.Select<KeyValuePair<string, TotalAndAverage>, XElement>((IEnumerable<KeyValuePair<string, TotalAndAverage>>) this.AggregatedEntities, (Func<KeyValuePair<string, TotalAndAverage>, XElement>) (x => new XElement(Report.Namespace + "entity", new object[3]
        {
          (object) new XAttribute((XName) "name", (object) x.Key),
          (object) new XAttribute((XName) "total", (object) x.Value.Total),
          (object) new XAttribute((XName) "average-per-session", (object) x.Value.Average)
        })))),
        (object) new XElement(Report.Namespace + "average-entities-loaded-per-session", (object) this.AverageEntitiesLoadedPerSession),
        (object) new XElement(Report.Namespace + "average-statements-per-session", (object) this.AverageStatementsPerSession),
        (object) new XElement(Report.Namespace + "count-of-sessions", (object) this.CountOfSessions),
        (object) new XElement(Report.Namespace + "number-of-alerts", (object) this.NumberOfAlerts),
        (object) new XElement(Report.Namespace + "number-of-entities-types", (object) this.NumberOfEntitiesTypes),
        (object) new XElement(Report.Namespace + "number-of-transaction-commits", (object) this.NumberOfTransactionCommits),
        (object) new XElement(Report.Namespace + "number-of-transaction-rollbacks", (object) this.NumberOfTransactionRollbacks),
        (object) new XElement(Report.Namespace + "number-of-transactions", (object) this.NumberOfTransactions)
      });
      if (this.AverageQueryRowCount.HasValue)
        xelement.Add((object) new XElement(Report.Namespace + "average-query-row-count", (object) this.AverageQueryRowCount));
      return xelement;
    }

    [CLSCompliant(false)]
    protected override JContainer ExportReportToJson()
    {
      return (JContainer) new JObject(new object[11]
      {
        (object) new JProperty("aggregatedAlerts", (object) new JArray((object) Enumerable.Select<IAggregatedAlertSnapshot, JObject>(this.AggregatedAlerts, (Func<IAggregatedAlertSnapshot, JObject>) (x => new JObject(new object[3]
        {
          (object) new JProperty("alert", (object) ProfileExtensions.Translate(Profile.Current, x.Alert.Title)),
          (object) new JProperty("helpTopic", (object) x.Alert.HelpTopic),
          (object) new JProperty("count", (object) x.Count)
        }))))),
        (object) new JProperty("aggregatedEntities", (object) new JArray((object) Enumerable.Select<KeyValuePair<string, TotalAndAverage>, JObject>((IEnumerable<KeyValuePair<string, TotalAndAverage>>) this.AggregatedEntities, (Func<KeyValuePair<string, TotalAndAverage>, JObject>) (x => new JObject(new object[3]
        {
          (object) new JProperty("name", (object) x.Key),
          (object) new JProperty("total", (object) x.Value.Total),
          (object) new JProperty("averagePerSession", (object) Math.Round(x.Value.Average, 2))
        }))))),
        (object) new JProperty("averageEntitiesLoadedPerSession", (object) this.AverageEntitiesLoadedPerSession),
        (object) new JProperty("averageQueryRowCount", (object) Math.Round(this.AverageQueryRowCount ?? 0.0, 2)),
        (object) new JProperty("averageStatementsPerSession", (object) Math.Round(this.AverageStatementsPerSession, 2)),
        (object) new JProperty("countOfSessions", (object) this.CountOfSessions),
        (object) new JProperty("numberOfAlerts", (object) this.NumberOfAlerts),
        (object) new JProperty("numberOfEntitiesTypes", (object) this.NumberOfEntitiesTypes),
        (object) new JProperty("numberOfTransactionCommits", (object) this.NumberOfTransactionCommits),
        (object) new JProperty("numberOfTransactionRollbacks", (object) this.NumberOfTransactionRollbacks),
        (object) new JProperty("numberOfTransactions", (object) this.NumberOfTransactions)
      });
    }

    public void Sort(string columnName)
    {
      this.CurrentSort.SortColumnResolver = this.columnResolvers.Resolve(columnName);
      this.CurrentSort.ColumnName = columnName;
      this.AggregatedAlerts = (IEnumerable<IAggregatedAlertSnapshot>) this.sorter.Sort((IList<IAggregatedAlertSnapshot>) Enumerable.ToList<IAggregatedAlertSnapshot>(this.AggregatedAlerts), this.CurrentSort);
    }

    public void SortBySelectedColumn()
    {
      if (this.CurrentSort.IsUndefined)
        return;
      this.AggregatedAlerts = (IEnumerable<IAggregatedAlertSnapshot>) this.sorter.Sort((IList<IAggregatedAlertSnapshot>) Enumerable.ToList<IAggregatedAlertSnapshot>(this.AggregatedAlerts), this.CurrentSort);
    }

    public override void CancelSorting()
    {
      this.AggregatedAlerts = this.unsortedAlerts;
      this.CurrentSort.IsUndefined = true;
      this.CurrentSort.ColumnName = "";
    }
  }
}
