﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sessions.SessionModel
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Filtering;
using HibernatingRhinos.Profiler.Client.Model.Statements;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Sessions
{
  [Serializable]
  public class SessionModel : SelectionBase, ISessionPresenterItem, IFilterableSessionSnapshot
  {
    private readonly SessionStatisticsModel statisticsModel = new SessionStatisticsModel();
    private IList<StarColor> starBrushes = (IList<StarColor>) new List<StarColor>();
    private readonly IBackendBridge backend;
    private readonly FilterServiceModel filter;
    private readonly SessionStatements sessionStatements;
    private PollingBase currentView;
    private readonly StarColor defaultBrush;
    private StarColor starBrush;
    private StarColor lastBrush;
    private int? statementCount;
    private string nameForUi;

    public Session Session { get; private set; }

    public bool HasActiveFilter
    {
      get
      {
        return this.filter.Filters.Count > 0;
      }
    }

    public SessionStatements SessionStatements
    {
      get
      {
        return this.sessionStatements;
      }
    }

    public SessionStatisticsModel Statistics
    {
      get
      {
        return this.statisticsModel;
      }
    }

    public string ShortUrl { get; set; }

    public string Url { get; set; }

    public int StatementCount
    {
      get
      {
        return this.statementCount ?? Enumerable.Count<IStatementModel>((IEnumerable<IStatementModel>) this.StatementsModel.Statements, (Func<IStatementModel, bool>) (x => !x.IsTransaction));
      }
      private set
      {
        this.statementCount = new int?(value);
      }
    }

    public bool HasSuggestions { get; private set; }

    public bool HasWarnings { get; private set; }

    public TimeSpan Duration { get; set; }

    public bool Starred { get; set; }

    public bool CanBeStarred { get; set; }

    public StarColor StarBrush
    {
      get
      {
        return this.starBrush;
      }
      set
      {
        this.starBrush = value;
        this.Starred = this.starBrush != this.defaultBrush;
      }
    }

    public IList<StarColor> StarBrushes
    {
      get
      {
        return this.starBrushes;
      }
      set
      {
        this.starBrushes = value;
      }
    }

    public Guid Id
    {
      get
      {
        return this.Session.InternalId;
      }
    }

    public IDictionary<StatementAlert, int> AggregatedAlerts
    {
      get
      {
        Dictionary<StatementAlert, int> dictionary = new Dictionary<StatementAlert, int>();
        foreach (StatementAlert key in Enumerable.SelectMany<IStatementModel, StatementAlert>((IEnumerable<IStatementModel>) this.sessionStatements.Statements, (Func<IStatementModel, IEnumerable<StatementAlert>>) (x => (IEnumerable<StatementAlert>) x.Alerts)))
        {
          int num;
          if (!dictionary.TryGetValue(key, out num))
            num = 0;
          dictionary[key] = num + 1;
        }
        return (IDictionary<StatementAlert, int>) dictionary;
      }
    }

    public DateTime Timestamp
    {
      get
      {
        return this.Session.Id.Timestamp;
      }
    }

    public IList<PollingBase> Views { get; set; }

    public PollingBase CurrentView
    {
      get
      {
        return this.currentView;
      }
      set
      {
        if (!this.IsSelected)
          return;
        if (this.currentView != null)
          this.currentView.Deactivate();
        this.currentView = value;
        if (this.currentView == null)
          return;
        this.currentView.Activate();
      }
    }

    public IExposeSelectableStatements StatementsModel
    {
      get
      {
        return (IExposeSelectableStatements) this.sessionStatements;
      }
    }

    public string Name
    {
      get
      {
        return this.nameForUi ?? this.DisplayName;
      }
    }

    public string ShortDescription
    {
      get
      {
        if (this.Url == null)
          return this.Name;
        return this.Name + " [" + this.ShortUrl + "]";
      }
    }

    public bool WasClosed { get; private set; }

    public event Action<SessionModel> Closed = param0 => {};

    public SessionModel(IBackendBridge backend, FilterServiceModel filter, Session session, ApplicationModel applicationModel)
    {
      this.backend = backend;
      this.filter = filter;
      this.Session = session;
      Uri result;
      this.ShortUrl = Uri.TryCreate(session.Url, UriKind.Absolute, out result) ? result.PathAndQuery : session.Url;
      this.Url = session.Url;
      if (string.IsNullOrEmpty(this.Url))
      {
        this.Url = (string) null;
        this.ShortUrl = (string) null;
      }
      SessionStatements sessionStatements = new SessionStatements(session, filter, applicationModel, (Func<Session, IEnumerable<StatementSnapshot>>) (session1 => (IEnumerable<StatementSnapshot>) session1.GetStatementsSnapshots()));
      sessionStatements.DisplayName = "Statements";
      this.sessionStatements = sessionStatements;
      List<PollingBase> list1 = new List<PollingBase>()
      {
        (PollingBase) this.sessionStatements
      };
      if (Profile.Current.Supports(SupportedFeatures.Entities))
      {
        List<PollingBase> list2 = list1;
        SessionEntities sessionEntities1 = new SessionEntities(session, applicationModel.TrackingService);
        sessionEntities1.DisplayName = "Entities";
        SessionEntities sessionEntities2 = sessionEntities1;
        list2.Add((PollingBase) sessionEntities2);
      }
      List<PollingBase> list3 = list1;
      SessionUsage sessionUsage1 = new SessionUsage(this, applicationModel.TrackingService, new Func<Guid, SessionUsageSnapshot>(backend.Notifications.GetSessionUsageSnapshot));
      sessionUsage1.DisplayName = ProfileExtensions.Translate(Profile.Current, "[[session]] Usage");
      SessionUsage sessionUsage2 = sessionUsage1;
      list3.Add((PollingBase) sessionUsage2);
      this.Views = (IList<PollingBase>) list1;
      this.CurrentView = (PollingBase) this.sessionStatements;
      this.CanBeStarred = true;
      this.defaultBrush = StarColor.Default;
      this.StarBrushes = (IList<StarColor>) new List<StarColor>()
      {
        StarColor.Yellow,
        StarColor.Orange,
        StarColor.Red,
        StarColor.Green,
        StarColor.Blue,
        StarColor.Gray
      };
      this.StarBrush = this.defaultBrush;
      this.lastBrush = this.StarBrushes[0];
      this.Update(session);
    }

    public void RenameByUser(string newName)
    {
      this.nameForUi = newName;
    }

    public void Activate()
    {
      this.IsSelected = true;
      if (this.currentView == null)
        this.currentView = this.Views[0];
      this.currentView.Activate();
    }

    public void Deactivate()
    {
      this.IsSelected = false;
      if (this.currentView == null)
        return;
      this.currentView.Deactivate();
    }

    public ISessionPresenterItem RemoveFrom(IList<ISessionPresenterItem> items)
    {
      int index = items.IndexOf((ISessionPresenterItem) this);
      items.Remove((ISessionPresenterItem) this);
      if (index - 1 >= 0)
        return items[index - 1];
      if (index < items.Count)
        return items[index];
      return (ISessionPresenterItem) null;
    }

    public void Update(Session updatedSession)
    {
      this.DisplayName = updatedSession.Name;
      this.Duration = updatedSession.Duration.GetValueOrDefault();
      SessionStatistics sessionStatistics = updatedSession.CreateSessionStatistics();
      this.StatementCount = sessionStatistics.NumberOfStatements;
      this.Statistics.UpdateFrom(sessionStatistics, this);
      this.WasClosed = !updatedSession.IsOpen;
      if (!this.WasClosed)
        return;
      foreach (PollingBase pollingBase in (IEnumerable<PollingBase>) this.Views)
        pollingBase.DonePolling();
      this.Closed(this);
    }

    public void SetWarnings(bool warnings)
    {
      this.HasWarnings = warnings;
    }

    public void SetSuggestions(bool suggestions)
    {
      this.HasSuggestions = suggestions;
    }

    public void ToggleStarring()
    {
      this.Starred = !this.Starred;
      this.StarBrush = this.Starred ? this.lastBrush : this.defaultBrush;
    }

    public void SetColorOfStar(StarColor brush)
    {
      this.StarBrush = brush;
      this.lastBrush = brush;
    }

    public void CopySessionName()
    {
      SafeClipboard.SetText(ProfileExtensions.Translate(Profile.Current, this.Name));
    }

    public void CopySessionUrl()
    {
      SafeClipboard.SetText(this.Url);
    }
  }
}
