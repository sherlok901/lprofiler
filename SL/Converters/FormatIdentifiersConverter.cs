﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.SL.Converters.FormatIdentifiersConverter
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.SL.Converters
{
  public class FormatIdentifiersConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      IList<string> list = value as IList<string>;
      if (list != null)
        return (object) string.Join(", ", Enumerable.ToArray<string>((IEnumerable<string>) list));
      return value;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
