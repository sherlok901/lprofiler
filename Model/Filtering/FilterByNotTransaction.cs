﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Filtering.FilterByNotTransaction
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using System;

namespace HibernatingRhinos.Profiler.Client.Model.Filtering
{
  [Serializable]
  public class FilterByNotTransaction : AbstractFilter
  {
    public override bool IsValid
    {
      get
      {
        return true;
      }
    }

    public override string ToolTip
    {
      get
      {
        return "The statement is not a transaction.";
      }
    }

    public override bool IsValidStatement(IFilterableStatementSnapshot snapshot)
    {
      return !snapshot.IsTransaction;
    }
  }
}
