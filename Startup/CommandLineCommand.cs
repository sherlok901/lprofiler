﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.CommandLineCommand
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client;
using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Visitors;
using HibernatingRhinos.Profiler.Client.Services;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  public class CommandLineCommand : IStartupCommand
  {
    private readonly int port;
    private readonly string file;
    private readonly string inputFile;
    private readonly string licensePath;
    private readonly IEnumerable<ReportFormat> formats;
    private ApplicationModel model;
    private BackendBridge bridge;
    private ManualResetEvent waitUntilToldToShutDown;
    private Guid stopListeningId;

    public CommandLineCommand(int port, string file, string inputFile, string licensePath, IEnumerable<ReportFormat> formats)
    {
      if (file == null)
        throw new ArgumentNullException("file");
      this.port = port;
      this.file = file;
      this.inputFile = inputFile;
      this.licensePath = licensePath;
      this.formats = formats;
    }

    public int Execute()
    {
      this.bridge = new BackendBridge()
      {
        Configuration = UserPreferencesHolder.UserSettings.Configuration
      };
      using (this.bridge)
      {
        this.bridge.Initialize();
        this.model = new ApplicationModel((IBackendBridge) this.bridge);
        this.model.Initialize(TrackingService.Create());
        LicensingService licensingService = new LicensingService();
        if (!string.IsNullOrWhiteSpace(this.licensePath))
          LicensingDescription.CustomLicensePath = this.licensePath;
        if (!licensingService.ConfirmLicense().IsValidLicense)
        {
          StartupParser.WriteLineToConsole("Invalid license, cannot start profiling");
          StartupParser.FreeConsoleIfNeeded();
          return -5;
        }
        this.waitUntilToldToShutDown = new ManualResetEvent(false);
        if (!string.IsNullOrEmpty(this.inputFile))
        {
          StartupParser.WriteLineToConsole("Started to read: " + this.inputFile);
          Guid loadFileId = this.bridge.StartLoadFromFile(this.inputFile);
          LoadFileStatus loadFileStatus;
          while (true)
          {
            loadFileStatus = this.bridge.GetLoadFileStatus(loadFileId);
            if (!loadFileStatus.Complete)
            {
              StartupParser.WriteToConsole(string.Format("\rProgress: {0}", (object) loadFileStatus.NumberOfLoadedMessages));
              Thread.Sleep(150);
            }
            else
              break;
          }
          if (loadFileStatus.ExceptionMessage != null)
            StartupParser.WriteLineToConsole(loadFileStatus.ExceptionMessage);
        }
        else
        {
          this.stopListeningId = this.bridge.Start(this.port, StartupParser.CurrentArguments.ContextFile);
          this.bridge.Notifications.ShutdownAndOutputReport += new Action(this.HandleShutdownAndOutputReport);
          StartupParser.WriteLineToConsole("Started to listen to profiling events");
          StartupParser.FreeConsoleIfNeeded();
          this.waitUntilToldToShutDown.WaitOne();
        }
        this.bridge.EventsObserver.WaitForAllMessages((HibernatingRhinos.Profiler.BackEnd.Bridge.CancellationToken) null);
        new ForcePollVisitor().Visit(this.model);
        string str = this.file + ".~tmp";
        foreach (ReportFormat reportFormat in this.formats)
        {
          switch (reportFormat)
          {
            case ReportFormat.Xml:
              this.model.ExportModelAsXml(str);
              continue;
            case ReportFormat.Html:
              this.model.ExportModelAsHtml(str);
              continue;
            default:
              throw new ArgumentOutOfRangeException(reportFormat.ToString());
          }
        }
        this.model.Dispose();
        File.Delete(this.file);
        File.Move(str, this.file);
      }
      return 0;
    }

    private static bool BridgeOnBringApplicationToFront()
    {
      int num = (int) MessageBox.Show("Stopped another instance of the profiler from running because" + Environment.NewLine + "the profiler is currently running in command line mode." + Environment.NewLine + "Execute the profiler with the shutdown flag to stop the command line mode and" + Environment.NewLine + "the report", "Profiler is running in command line mode.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      return false;
    }

    private void HandleShutdownAndOutputReport()
    {
      this.bridge.DisposeOf(this.stopListeningId);
      this.waitUntilToldToShutDown.Set();
    }
  }
}
