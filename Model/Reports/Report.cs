﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Reports.Report
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Extensions;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Reports
{
  [Serializable]
  public abstract class Report : PollingBase
  {
    protected readonly ITrackingService trackingService;

    public virtual bool FilteringSupported
    {
      get
      {
        return false;
      }
    }

    public abstract string JsonReportName { get; }

    public static XNamespace Namespace
    {
      get
      {
        return XNamespace.Get("http://reports.hibernatingrhinos.com/" + Profile.CurrentProfile + ".Profiler/2009/10");
      }
    }

    protected Report(ITrackingService trackingService)
    {
      this.trackingService = trackingService;
    }

    public abstract void Clear();

    public abstract XElement ExportReport();

    public virtual void ExportXml(string file)
    {
      new XDocument(new object[1]
      {
        (object) this.ExportReport()
      }).Save(file);
    }

    [CLSCompliant(false)]
    protected virtual JContainer ExportReportToJson()
    {
      throw new NotSupportedException("Exporting to JSON is not supported by this report");
    }

    protected override void OnActivate()
    {
      this.trackingService.Track("Reports", StringExtensions.MakeStatementFromPascalCase(this.GetType().Name), (string) null, new int?());
    }

    public string ExportReportToJsonString()
    {
      JContainer jcontainer = this.ExportReportToJson();
      StringWriter stringWriter = new StringWriter();
      JsonExtensions.WriteJson((JToken) jcontainer, (TextWriter) stringWriter);
      return stringWriter.GetStringBuilder().ToString();
    }

    public virtual void ExportJson(string file)
    {
      using (StreamWriter text = File.CreateText(file))
        JsonExtensions.WriteJson((JToken) this.ExportReportToJson(), (TextWriter) text);
    }
  }
}
