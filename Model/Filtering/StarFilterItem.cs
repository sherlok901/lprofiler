﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Filtering.StarFilterItem
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model;
using System;

namespace HibernatingRhinos.Profiler.Client.Model.Filtering
{
  [Serializable]
  public class StarFilterItem : PropertyChangedBase
  {
    private bool isActive;

    public string ColorName { get; private set; }

    public bool IsActive
    {
      get
      {
        return this.isActive;
      }
      set
      {
        this.isActive = value;
        this.NotifyOfPropertyChange((Func<object>) (() => (object) (this.IsActive )));
      }
    }

    public StarFilterItem(string colorName, bool isActive)
    {
      this.ColorName = colorName;
      this.IsActive = isActive;
    }
  }
}
