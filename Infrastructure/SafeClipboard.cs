﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Infrastructure.SafeClipboard
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace HibernatingRhinos.Profiler.Client.Infrastructure
{
  public static class SafeClipboard
  {
    public static void SetText(string sql)
    {
      for (int index = 0; index < 15; ++index)
      {
        try
        {
          Clipboard.SetText(sql);
          break;
        }
        catch (COMException ex)
        {
          Thread.Sleep(10);
        }
      }
    }
  }
}
