﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Services.ISessionDiff
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model.Statements;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Client.Services
{
  public interface ISessionDiff
  {
    List<IStatementModel> Statements { get; }

    string FirstSessionName { get; set; }

    string SecondSessionName { get; set; }

    DiffStatus GetStatementStatus(IStatementModel statementModel);
  }
}
