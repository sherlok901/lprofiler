﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Reports.QueriesByMethod
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Model.Filtering;
using HibernatingRhinos.Profiler.Client.Tracking;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Reports
{
  [Serializable]
  public class QueriesByMethod : Report<IEnumerable<MethodQueriesSnapshot>>
  {
    [NonSerialized]
    private MethodQuery currentMethod;

    public List<MethodQuery> StatementsByMethod { get; private set; }

    public MethodQuery CurrentMethod
    {
      get
      {
        if (this.currentMethod == null)
        {
          this.currentMethod = Enumerable.FirstOrDefault<MethodQuery>((IEnumerable<MethodQuery>) this.StatementsByMethod);
          if (this.currentMethod != null)
            this.currentMethod.IsSelected = true;
        }
        return this.currentMethod;
      }
      set
      {
        if (value == null || this.currentMethod == value)
          return;
        if (this.currentMethod != null)
          this.currentMethod.IsSelected = false;
        this.currentMethod = value;
        if (this.currentMethod == null)
          return;
        this.currentMethod.IsSelected = true;
      }
    }

    public override bool FilteringSupported
    {
      get
      {
        return true;
      }
    }

    public override string JsonReportName
    {
      get
      {
        return "queriesByMethod";
      }
    }

    public QueriesByMethod(Func<IEnumerable<MethodQueriesSnapshot>> getSnapshot, FilterServiceModel filterService, ITrackingService trackingService)
      : base(trackingService, getSnapshot, filterService)
    {
      this.DisplayName = "Queries By Method";
      this.StatementsByMethod = new List<MethodQuery>();
    }

    protected override void Update(IEnumerable<MethodQueriesSnapshot> snapshots)
    {
      foreach (MethodQueriesSnapshot methodQueriesSnapshot in snapshots)
      {
        MethodQueriesSnapshot snapshot = methodQueriesSnapshot;
        MethodQuery methodQuery = Enumerable.FirstOrDefault<MethodQuery>(Enumerable.Where<MethodQuery>((IEnumerable<MethodQuery>) this.StatementsByMethod, (Func<MethodQuery, bool>) (shot => shot.FullName == snapshot.FullName)));
        if (methodQuery == null)
          this.StatementsByMethod.Add(MethodQuery.CreateFrom(snapshot, this.filterService, this.trackingService));
        else
          methodQuery.UpdateFrom(snapshot);
      }
    }

    public override void Clear()
    {
      foreach (MethodQuery methodQuery in this.StatementsByMethod)
        methodQuery.Clear();
      this.StatementsByMethod.Clear();
      this.currentMethod = (MethodQuery) null;
    }

    public override XElement ExportReport()
    {
      int queryCount = 0;
      Dictionary<QueryAggregation, int> indexes = new Dictionary<QueryAggregation, int>();
      Func<QueryAggregation, int> getId = (Func<QueryAggregation, int>) (queryAgg =>
      {
        int num1;
        if (indexes.TryGetValue(queryAgg, out num1))
          return num1;
        Dictionary<QueryAggregation, int> dictionary = indexes;
        QueryAggregation index = queryAgg;
        int num2 = queryCount++;
        int num3;
        int num4 = num3 = num2;
        dictionary[index] = num3;
        return num4;
      });
      return new XElement(Report.Namespace + "queries-by-method", (object) Enumerable.Select<MethodQuery, XElement>((IEnumerable<MethodQuery>) this.StatementsByMethod, (Func<MethodQuery, XElement>) (x => new XElement(Report.Namespace + "method", new object[3]
      {
        (object) new XAttribute((XName) "full-name", (object) x.FullName),
        (object) new XAttribute((XName) "statement-count", (object) x.Statements.Count),
        (object) Enumerable.Select<QueryAggregation, XElement>((IEnumerable<QueryAggregation>) x.Statements, (Func<QueryAggregation, XElement>) (q => new XElement(Report.Namespace + "query", new object[8]
        {
          (object) new XAttribute((XName) "query-id", (object) getId(q)),
          (object) XmlExportExtensions.ToXml(q.Duration),
          (object) XmlExportExtensions.ToXml(q.AverageDuration, "average-duration"),
          (object) new XElement(Report.Namespace + "count", (object) q.Count),
          (object) new XAttribute((XName) "is-cached", (object)  (q.IsCached ? 1 : 0)),
          (object) new XAttribute((XName) "is-ddl", (object)  (q.IsDDL ? 1 : 0)),
          (object) new XElement(Report.Namespace + "short-sql", (object) q.ShortSql),
          (object) new XElement(Report.Namespace + "formatted-sql", (object) q.FormattedSql)
        })))
      }))));
    }

    [CLSCompliant(false)]
    protected override JContainer ExportReportToJson()
    {
      int queryCount = 0;
      Dictionary<QueryAggregation, int> indexes = new Dictionary<QueryAggregation, int>();
      Func<QueryAggregation, int> getId = (Func<QueryAggregation, int>) (queryAgg =>
      {
        int num1;
        if (indexes.TryGetValue(queryAgg, out num1))
          return num1;
        Dictionary<QueryAggregation, int> dictionary = indexes;
        QueryAggregation index = queryAgg;
        int num2 = queryCount++;
        int num3;
        int num4 = num3 = num2;
        dictionary[index] = num3;
        return num4;
      });
      return (JContainer) new JArray((object) Enumerable.Select<MethodQuery, JObject>((IEnumerable<MethodQuery>) this.StatementsByMethod, (Func<MethodQuery, JObject>) (x => new JObject(new object[3]
      {
        (object) new JProperty("fullName", (object) x.FullName),
        (object) new JProperty("statementCount", (object) x.Statements.Count),
        (object) new JProperty("queries", (object) new JArray((object) Enumerable.Select<QueryAggregation, JObject>((IEnumerable<QueryAggregation>) x.Statements, (Func<QueryAggregation, JObject>) (q => new JObject(new object[7]
        {
          (object) new JProperty("queryId", (object) getId(q)),
          (object) new JProperty("averageDuration", (object) q.AverageDuration),
          (object) new JProperty("count", (object) q.Count),
          (object) new JProperty("isCached", (object)  (q.IsCached ? 1 : 0)),
          (object) new JProperty("isDdl", (object) (q.IsDDL ? 1 : 0)),
          (object) new JProperty("shortSql", (object) q.ShortSql),
          (object) new JProperty("formattedSql", (object) q.FormattedSql)
        })))))
      }))));
    }
  }
}
