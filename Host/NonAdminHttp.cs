﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Host.NonAdminHttp
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;
using System.Diagnostics;
using System.Net;
using System.Security.Principal;
using System.Windows.Forms;

namespace HibernatingRhinos.Profiler.Client.Host
{
  public static class NonAdminHttp
  {
    public static void EnsureCanListenToWhenInNonAdminContext(int port)
    {
      if (NonAdminHttp.CanStartHttpListener(port))
        return;
      int num1 = NonAdminHttp.TryGrantingHttpPrivileges(port);
      if (NonAdminHttp.CanStartHttpListener(port))
        return;
      int num2 = (int) MessageBox.Show("Failed to grant rights for listening to http, exit code: " + (object) num1);
    }

    private static void GetArgsForHttpAclCmd(int port, out string args, out string cmd)
    {
      if (Environment.OSVersion.Version.Major > 5)
      {
        cmd = "netsh";
        args = string.Format("http add urlacl url=http://+:{0}/ user=\"{1}\"", (object) port, (object) WindowsIdentity.GetCurrent().Name);
      }
      else
      {
        cmd = "httpcfg";
        args = string.Format("set urlacl /u http://+:{0}/ /a D:(A;;GX;;;\"{1}\")", (object) port, (object) WindowsIdentity.GetCurrent().User);
      }
    }

    private static bool CanStartHttpListener(int port)
    {
      try
      {
        HttpListener httpListener = new HttpListener();
        httpListener.Prefixes.Add("http://+:" + (object) port + "/");
        httpListener.Start();
        httpListener.Stop();
        return true;
      }
      catch (HttpListenerException ex)
      {
        if (ex.ErrorCode != 5)
          throw;
      }
      return false;
    }

    private static int TryGrantingHttpPrivileges(int port)
    {
      string args;
      string cmd;
      NonAdminHttp.GetArgsForHttpAclCmd(port, out args, out cmd);
      try
      {
        Process process = Process.Start(new ProcessStartInfo()
        {
          Verb = "runas",
          Arguments = args,
          FileName = cmd
        });
        process.WaitForExit();
        return process.ExitCode;
      }
      catch (Exception ex)
      {
        return -144;
      }
    }
  }
}
