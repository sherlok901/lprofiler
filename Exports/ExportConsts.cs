﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Exports.ExportConsts
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

namespace HibernatingRhinos.Profiler.Client.Exports
{
  public class ExportConsts
  {
    public const string ReportCss = "/*{{report-css}}*/";
    public const string ReportName = "{{report-name}}";
    public const string ReportTitle = "{{report-title}}";
    public const string ReportDate = "{{report-date}}";
    public const string ReportRootUrl = "{{report-root-url}}";
    public const string ReportsAsJson = "{{reports-as-json}}";
  }
}
