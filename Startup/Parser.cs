﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.Parser
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  public sealed class Parser
  {
    public const string NewLine = "\r\n";
    private const int STD_OUTPUT_HANDLE = -11;
    private const int spaceBeforeParam = 2;
    private ArrayList arguments;
    private Hashtable argumentMap;
    private Parser.Argument defaultArgument;
    private ErrorReporter reporter;

    public bool HasDefaultArgument
    {
      get
      {
        return this.defaultArgument != null;
      }
    }

    private Parser()
    {
    }

    public Parser(Type argumentSpecification, ErrorReporter reporter)
    {
      this.reporter = reporter;
      this.arguments = new ArrayList();
      this.argumentMap = new Hashtable();
      foreach (FieldInfo field in argumentSpecification.GetFields())
      {
        if (!field.IsStatic && !field.IsInitOnly && !field.IsLiteral)
        {
          ArgumentAttribute attribute = Parser.GetAttribute(field);
          if (attribute is DefaultArgumentAttribute)
            this.defaultArgument = new Parser.Argument(attribute, field, reporter);
          else
            this.arguments.Add((object) new Parser.Argument(attribute, field, reporter));
        }
      }
      foreach (Parser.Argument obj in this.arguments)
      {
        this.argumentMap[(object) obj.LongName] = (object) obj;
        if (obj.ExplicitShortName)
        {
          if (obj.ShortName != null && obj.ShortName.Length > 0)
            this.argumentMap[(object) obj.ShortName] = (object) obj;
          else
            obj.ClearShortName();
        }
      }
      foreach (Parser.Argument obj in this.arguments)
      {
        if (!obj.ExplicitShortName)
        {
          if (obj.ShortName != null && obj.ShortName.Length > 0 && !this.argumentMap.ContainsKey((object) obj.ShortName))
            this.argumentMap[(object) obj.ShortName] = (object) obj;
          else
            obj.ClearShortName();
        }
      }
    }

    public static bool ParseArgumentsWithUsage(string[] arguments, object destination)
    {
      if (!Parser.ParseHelp(arguments) && Parser.ParseArguments(arguments, destination))
        return true;
      Console.Write(Parser.ArgumentsUsage(destination.GetType()));
      return false;
    }

    public static bool ParseArguments(string[] arguments, object destination)
    {
      return Parser.ParseArguments(arguments, destination, new ErrorReporter(Console.Error.WriteLine));
    }

    public static bool ParseArguments(string[] arguments, object destination, ErrorReporter reporter)
    {
      return new Parser(destination.GetType(), reporter).Parse(arguments, destination);
    }

    private static void NullErrorReporter(string message)
    {
    }

    public static bool ParseHelp(string[] args)
    {
      Parser parser = new Parser(typeof (Parser.HelpArgument), new ErrorReporter(Parser.NullErrorReporter));
      Parser.HelpArgument helpArgument = new Parser.HelpArgument();
      parser.Parse(args, (object) helpArgument);
      return helpArgument.help;
    }

    public static string ArgumentsUsage(Type argumentType)
    {
      int columns = Parser.GetConsoleWindowWidth();
      if (columns == 0)
        columns = 80;
      return Parser.ArgumentsUsage(argumentType, columns);
    }

    public static string ArgumentsUsage(Type argumentType, int columns)
    {
      return new Parser(argumentType, (ErrorReporter) null).GetUsageString(columns);
    }

    [DllImport("kernel32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
    private static extern int GetStdHandle(int nStdHandle);

    [DllImport("kernel32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall, SetLastError = true)]
    private static extern int GetConsoleScreenBufferInfo(int hConsoleOutput, ref Parser.CONSOLE_SCREEN_BUFFER_INFO lpConsoleScreenBufferInfo);

    public static int GetConsoleWindowWidth()
    {
      Parser.CONSOLE_SCREEN_BUFFER_INFO lpConsoleScreenBufferInfo = new Parser.CONSOLE_SCREEN_BUFFER_INFO();
      Parser.GetConsoleScreenBufferInfo(Parser.GetStdHandle(-11), ref lpConsoleScreenBufferInfo);
      return (int) lpConsoleScreenBufferInfo.dwSize.x;
    }

    public static int IndexOf(StringBuilder text, char value, int startIndex)
    {
      for (int index = startIndex; index < text.Length; ++index)
      {
        if ((int) text[index] == (int) value)
          return index;
      }
      return -1;
    }

    public static int LastIndexOf(StringBuilder text, char value, int startIndex)
    {
      for (int index = Math.Min(startIndex, text.Length - 1); index >= 0; --index)
      {
        if ((int) text[index] == (int) value)
          return index;
      }
      return -1;
    }

    private static ArgumentAttribute GetAttribute(FieldInfo field)
    {
      object[] customAttributes = field.GetCustomAttributes(typeof (ArgumentAttribute), false);
      if (customAttributes.Length == 1)
        return (ArgumentAttribute) customAttributes[0];
      return (ArgumentAttribute) null;
    }

    private void ReportUnrecognizedArgument(string argument)
    {
      this.reporter(string.Format("Unrecognized command line argument '{0}'", (object) argument));
    }

    private bool ParseArgumentList(string[] args, object destination)
    {
      bool flag = false;
      if (args != null)
      {
        foreach (string str1 in args)
        {
          if (str1.Length > 0)
          {
            switch (str1[0])
            {
              case '-':
              case '/':
                int num = str1.IndexOfAny(new char[3]
                {
                  ':',
                  '+',
                  '-'
                }, 1);
                string str2 = str1.Substring(1, num == -1 ? str1.Length - 1 : num - 1);
                string str3 = str2.Length + 1 != str1.Length ? (str1.Length <= 1 + str2.Length || (int) str1[1 + str2.Length] != 58 ? str1.Substring(str2.Length + 1) : str1.Substring(str2.Length + 2)) : (string) null;
                Parser.Argument obj = (Parser.Argument) this.argumentMap[(object) str2];
                if (obj == null)
                {
                  this.ReportUnrecognizedArgument(str1);
                  flag = true;
                  continue;
                }
                flag |= !obj.SetValue(str3, destination);
                continue;
              case '@':
                string[] arguments;
                flag = flag | this.LexFileArguments(str1.Substring(1), out arguments) | this.ParseArgumentList(arguments, destination);
                continue;
              default:
                if (this.defaultArgument != null)
                {
                  flag |= !this.defaultArgument.SetValue(str1, destination);
                  continue;
                }
                this.ReportUnrecognizedArgument(str1);
                flag = true;
                continue;
            }
          }
        }
      }
      return flag;
    }

    public bool Parse(string[] args, object destination)
    {
      bool flag = this.ParseArgumentList(args, destination);
      foreach (Parser.Argument obj in this.arguments)
        flag |= obj.Finish(destination);
      if (this.defaultArgument != null)
        flag |= this.defaultArgument.Finish(destination);
      return !flag;
    }

    public string GetUsageString(int screenWidth)
    {
      Parser.ArgumentHelpStrings[] allHelpStrings = this.GetAllHelpStrings();
      int val1 = 0;
      foreach (Parser.ArgumentHelpStrings argumentHelpStrings in allHelpStrings)
        val1 = Math.Max(val1, argumentHelpStrings.syntax.Length);
      int num1 = val1 + 2;
      screenWidth = Math.Max(screenWidth, 15);
      int num2 = screenWidth >= num1 + 10 ? num1 : 5;
      StringBuilder builder = new StringBuilder();
      foreach (Parser.ArgumentHelpStrings argumentHelpStrings in allHelpStrings)
      {
        int length = argumentHelpStrings.syntax.Length;
        builder.Append(argumentHelpStrings.syntax);
        int currentColumn = length;
        if (length >= num2)
        {
          builder.Append("\n");
          currentColumn = 0;
        }
        int val2 = screenWidth - num2;
        int startIndex = 0;
label_14:
        while (startIndex < argumentHelpStrings.help.Length)
        {
          builder.Append(' ', num2 - currentColumn);
          currentColumn = num2;
          int num3 = startIndex + val2;
          int num4;
          if (num3 >= argumentHelpStrings.help.Length)
          {
            num4 = argumentHelpStrings.help.Length;
          }
          else
          {
            num4 = argumentHelpStrings.help.LastIndexOf(' ', num3 - 1, Math.Min(num3 - startIndex, val2));
            if (num4 <= startIndex)
              num4 = startIndex + val2;
          }
          builder.Append(argumentHelpStrings.help, startIndex, num4 - startIndex);
          startIndex = num4;
          Parser.AddNewLine("\n", builder, ref currentColumn);
          while (true)
          {
            if (startIndex < argumentHelpStrings.help.Length && (int) argumentHelpStrings.help[startIndex] == 32)
              ++startIndex;
            else
              goto label_14;
          }
        }
        if (argumentHelpStrings.help.Length == 0)
          builder.Append("\n");
      }
      return builder.ToString();
    }

    private static void AddNewLine(string newLine, StringBuilder builder, ref int currentColumn)
    {
      builder.Append(newLine);
      currentColumn = 0;
    }

    private Parser.ArgumentHelpStrings[] GetAllHelpStrings()
    {
      Parser.ArgumentHelpStrings[] argumentHelpStringsArray1 = new Parser.ArgumentHelpStrings[this.NumberOfParametersToDisplay()];
      int index1 = 0;
      foreach (Parser.Argument obj in this.arguments)
      {
        argumentHelpStringsArray1[index1] = Parser.GetHelpStrings(obj);
        ++index1;
      }
      Parser.ArgumentHelpStrings[] argumentHelpStringsArray2 = argumentHelpStringsArray1;
      int index2 = index1;
      int num1 = 1;
      int num2 = index2 + num1;
      argumentHelpStringsArray2[index2] = new Parser.ArgumentHelpStrings("@<file>", "Read response file for more options");
      if (this.defaultArgument != null)
      {
        Parser.ArgumentHelpStrings[] argumentHelpStringsArray3 = argumentHelpStringsArray1;
        int index3 = num2;
        int num3 = 1;
        int num4 = index3 + num3;
        argumentHelpStringsArray3[index3] = Parser.GetHelpStrings(this.defaultArgument);
      }
      return argumentHelpStringsArray1;
    }

    private static Parser.ArgumentHelpStrings GetHelpStrings(Parser.Argument arg)
    {
      return new Parser.ArgumentHelpStrings(arg.SyntaxHelp, arg.FullHelpText);
    }

    private int NumberOfParametersToDisplay()
    {
      int num = this.arguments.Count + 1;
      if (this.HasDefaultArgument)
        ++num;
      return num;
    }

    private bool LexFileArguments(string fileName, out string[] arguments)
    {
      string str = (string) null;
      try
      {
        using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
          str = new StreamReader((Stream) fileStream).ReadToEnd();
      }
      catch (Exception ex)
      {
        this.reporter(string.Format("Error: Can't open command line argument file '{0}' : '{1}'", (object) fileName, (object) ex.Message));
        arguments = (string[]) null;
        return false;
      }
      bool flag1 = false;
      ArrayList arrayList = new ArrayList();
      StringBuilder stringBuilder = new StringBuilder();
      bool flag2 = false;
      int index = 0;
      try
      {
label_10:
        while (true)
        {
          while (char.IsWhiteSpace(str[index]))
            ++index;
          if ((int) str[index] != 35)
          {
            do
            {
              if ((int) str[index] == 92)
              {
                int repeatCount = 1;
                ++index;
                while (index == str.Length && (int) str[index] == 92)
                  ++repeatCount;
                if (index == str.Length || (int) str[index] != 34)
                {
                  stringBuilder.Append('\\', repeatCount);
                }
                else
                {
                  stringBuilder.Append('\\', repeatCount >> 1);
                  if ((repeatCount & 1) != 0)
                    stringBuilder.Append('"');
                  else
                    flag2 = !flag2;
                }
              }
              else if ((int) str[index] == 34)
              {
                flag2 = !flag2;
                ++index;
              }
              else
              {
                stringBuilder.Append(str[index]);
                ++index;
              }
            }
            while (!char.IsWhiteSpace(str[index]) || flag2);
            arrayList.Add((object) stringBuilder.ToString());
            stringBuilder.Length = 0;
          }
          else
            break;
        }
        ++index;
        while ((int) str[index] != 10)
          ++index;
        goto label_10;
      }
      catch (IndexOutOfRangeException ex)
      {
        if (flag2)
        {
          this.reporter(string.Format("Error: Unbalanced '\"' in command line argument file '{0}'", (object) fileName));
          flag1 = true;
        }
        else if (stringBuilder.Length > 0)
          arrayList.Add((object) stringBuilder.ToString());
      }
      arguments = (string[]) arrayList.ToArray(typeof (string));
      return flag1;
    }

    private static string LongName(ArgumentAttribute attribute, FieldInfo field)
    {
      if (attribute != null && !attribute.DefaultLongName)
        return attribute.LongName;
      return field.Name;
    }

    private static string ShortName(ArgumentAttribute attribute, FieldInfo field)
    {
      if (attribute is DefaultArgumentAttribute)
        return (string) null;
      if (!Parser.ExplicitShortName(attribute))
        return Parser.LongName(attribute, field).Substring(0, 1);
      return attribute.ShortName;
    }

    private static string HelpText(ArgumentAttribute attribute, FieldInfo field)
    {
      if (attribute == null)
        return (string) null;
      return attribute.HelpText;
    }

    private static bool HasHelpText(ArgumentAttribute attribute)
    {
      if (attribute != null)
        return attribute.HasHelpText;
      return false;
    }

    private static bool ExplicitShortName(ArgumentAttribute attribute)
    {
      if (attribute != null)
        return !attribute.DefaultShortName;
      return false;
    }

    private static object DefaultValue(ArgumentAttribute attribute, FieldInfo field)
    {
      if (attribute != null && attribute.HasDefaultValue)
        return attribute.DefaultValue;
      return (object) null;
    }

    private static Type ElementType(FieldInfo field)
    {
      if (Parser.IsCollectionType(field.FieldType))
        return field.FieldType.GetElementType();
      return (Type) null;
    }

    private static ArgumentType Flags(ArgumentAttribute attribute, FieldInfo field)
    {
      if (attribute != null)
        return attribute.Type;
      return Parser.IsCollectionType(field.FieldType) ? ArgumentType.MultipleUnique : ArgumentType.AtMostOnce;
    }

    private static bool IsCollectionType(Type type)
    {
      return type.IsArray;
    }

    private static bool IsValidElementType(Type type)
    {
      if (!(type != (Type) null))
        return false;
      if (!(type == typeof (int)) && !(type == typeof (uint)) && (!(type == typeof (string)) && !(type == typeof (bool))))
        return type.IsEnum;
      return true;
    }

    private class HelpArgument
    {
      [HibernatingRhinos.Profiler.Client.Startup.Argument(ArgumentType.AtMostOnce, ShortName = "?")]
      public bool help;
    }

    private struct COORD
    {
      internal short x;
      internal short y;
    }

    private struct SMALL_RECT
    {
      internal short Left;
      internal short Top;
      internal short Right;
      internal short Bottom;
    }

    private struct CONSOLE_SCREEN_BUFFER_INFO
    {
      internal Parser.COORD dwSize;
      internal Parser.COORD dwCursorPosition;
      internal short wAttributes;
      internal Parser.SMALL_RECT srWindow;
      internal Parser.COORD dwMaximumWindowSize;
    }

    private struct ArgumentHelpStrings
    {
      public string syntax;
      public string help;

      public ArgumentHelpStrings(string syntax, string help)
      {
        this.syntax = syntax;
        this.help = help;
      }
    }

    private class Argument
    {
      private string longName;
      private string shortName;
      private string helpText;
      private bool hasHelpText;
      private bool explicitShortName;
      private object defaultValue;
      private bool seenValue;
      private FieldInfo field;
      private Type elementType;
      private ArgumentType flags;
      private ArrayList collectionValues;
      private ErrorReporter reporter;
      private bool isDefault;

      public Type ValueType
      {
        get
        {
          if (!this.IsCollection)
            return this.Type;
          return this.elementType;
        }
      }

      public string LongName
      {
        get
        {
          return this.longName;
        }
      }

      public bool ExplicitShortName
      {
        get
        {
          return this.explicitShortName;
        }
      }

      public string ShortName
      {
        get
        {
          return this.shortName;
        }
      }

      public bool HasShortName
      {
        get
        {
          return this.shortName != null;
        }
      }

      public bool HasHelpText
      {
        get
        {
          return this.hasHelpText;
        }
      }

      public string HelpText
      {
        get
        {
          return this.helpText;
        }
      }

      public object DefaultValue
      {
        get
        {
          return this.defaultValue;
        }
      }

      public bool HasDefaultValue
      {
        get
        {
          return null != this.defaultValue;
        }
      }

      public string FullHelpText
      {
        get
        {
          StringBuilder builder = new StringBuilder();
          if (this.HasHelpText)
            builder.Append(this.HelpText);
          if (this.HasDefaultValue)
          {
            if (builder.Length > 0)
              builder.Append(" ");
            builder.Append("Default value:'");
            this.AppendValue(builder, this.DefaultValue);
            builder.Append('\'');
          }
          if (this.HasShortName)
          {
            if (builder.Length > 0)
              builder.Append(" ");
            builder.Append("(short form /");
            builder.Append(this.ShortName);
            builder.Append(")");
          }
          return builder.ToString();
        }
      }

      public string SyntaxHelp
      {
        get
        {
          StringBuilder stringBuilder = new StringBuilder();
          if (this.IsDefault)
          {
            stringBuilder.Append("<");
            stringBuilder.Append(this.LongName);
            stringBuilder.Append(">");
          }
          else
          {
            stringBuilder.Append("/");
            stringBuilder.Append(this.LongName);
            Type valueType = this.ValueType;
            if (valueType == typeof (int))
              stringBuilder.Append(":<int>");
            else if (valueType == typeof (uint))
              stringBuilder.Append(":<uint>");
            else if (valueType == typeof (bool))
              stringBuilder.Append("[+|-]");
            else if (valueType == typeof (string))
            {
              stringBuilder.Append(":<string>");
            }
            else
            {
              stringBuilder.Append(":{");
              bool flag = true;
              foreach (FieldInfo fieldInfo in valueType.GetFields())
              {
                if (fieldInfo.IsStatic)
                {
                  if (flag)
                    flag = false;
                  else
                    stringBuilder.Append('|');
                  stringBuilder.Append(fieldInfo.Name);
                }
              }
              stringBuilder.Append('}');
            }
          }
          return stringBuilder.ToString();
        }
      }

      public bool IsRequired
      {
        get
        {
          return ArgumentType.AtMostOnce != (this.flags & ArgumentType.Required);
        }
      }

      public bool SeenValue
      {
        get
        {
          return this.seenValue;
        }
      }

      public bool AllowMultiple
      {
        get
        {
          return ArgumentType.AtMostOnce != (this.flags & ArgumentType.Multiple);
        }
      }

      public bool Unique
      {
        get
        {
          return ArgumentType.AtMostOnce != (this.flags & ArgumentType.Unique);
        }
      }

      public Type Type
      {
        get
        {
          return this.field.FieldType;
        }
      }

      public bool IsCollection
      {
        get
        {
          return Parser.IsCollectionType(this.Type);
        }
      }

      public bool IsDefault
      {
        get
        {
          return this.isDefault;
        }
      }

      public Argument(ArgumentAttribute attribute, FieldInfo field, ErrorReporter reporter)
      {
        this.longName = Parser.LongName(attribute, field);
        this.explicitShortName = Parser.ExplicitShortName(attribute);
        this.shortName = Parser.ShortName(attribute, field);
        this.hasHelpText = Parser.HasHelpText(attribute);
        this.helpText = Parser.HelpText(attribute, field);
        this.defaultValue = Parser.DefaultValue(attribute, field);
        this.elementType = Parser.ElementType(field);
        this.flags = Parser.Flags(attribute, field);
        this.field = field;
        this.seenValue = false;
        this.reporter = reporter;
        this.isDefault = attribute != null && attribute is DefaultArgumentAttribute;
        if (!this.IsCollection)
          return;
        this.collectionValues = new ArrayList();
      }

      public bool Finish(object destination)
      {
        if (!this.SeenValue && this.HasDefaultValue)
          this.field.SetValue(destination, this.DefaultValue);
        if (this.IsCollection)
          this.field.SetValue(destination, (object) this.collectionValues.ToArray(this.elementType));
        return this.ReportMissingRequiredArgument();
      }

      private bool ReportMissingRequiredArgument()
      {
        if (!this.IsRequired || this.SeenValue)
          return false;
        if (this.IsDefault)
          this.reporter(string.Format("Missing required argument '<{0}>'.", (object) this.LongName));
        else
          this.reporter(string.Format("Missing required argument '/{0}'.", (object) this.LongName));
        return true;
      }

      private void ReportDuplicateArgumentValue(string value)
      {
        this.reporter(string.Format("Duplicate '{0}' argument '{1}'", (object) this.LongName, (object) value));
      }

      public bool SetValue(string value, object destination)
      {
        if (this.SeenValue && !this.AllowMultiple)
        {
          this.reporter(string.Format("Duplicate '{0}' argument", (object) this.LongName));
          return false;
        }
        this.seenValue = true;
        object obj;
        if (!this.ParseValue(this.ValueType, value, out obj))
          return false;
        if (this.IsCollection)
        {
          if (this.Unique && this.collectionValues.Contains(obj))
          {
            this.ReportDuplicateArgumentValue(value);
            return false;
          }
          this.collectionValues.Add(obj);
        }
        else
          this.field.SetValue(destination, obj);
        return true;
      }

      private void ReportBadArgumentValue(string value)
      {
        this.reporter(string.Format("'{0}' is not a valid value for the '{1}' command line option", (object) value, (object) this.LongName));
      }

      private bool ParseValue(Type type, string stringData, out object value)
      {
        if (stringData != null || type == typeof (bool))
        {
          if (stringData != null)
          {
            if (stringData.Length <= 0)
              goto label_16;
          }
          try
          {
            if (type == typeof (string))
            {
              value = (object) stringData;
              return true;
            }
            if (type == typeof (bool))
            {
              if (stringData == null || stringData == "+")
              {
                value = (object) true;
                return true;
              }
              if (stringData == "-")
              {
                value = (object) false;
                return true;
              }
            }
            else
            {
              if (type == typeof (int))
              {
                value = (object) int.Parse(stringData);
                return true;
              }
              if (type == typeof (uint))
              {
                value = (object) int.Parse(stringData);
                return true;
              }
              value = Enum.Parse(type, stringData, true);
              return true;
            }
          }
          catch
          {
          }
        }
label_16:
        this.ReportBadArgumentValue(stringData);
        value = (object) null;
        return false;
      }

      private void AppendValue(StringBuilder builder, object value)
      {
        if (value is string || value is int || (value is uint || value.GetType().IsEnum))
          builder.Append(value.ToString());
        else if (value is bool)
        {
          builder.Append((bool) value ? "+" : "-");
        }
        else
        {
          bool flag = true;
          foreach (object obj in (Array) value)
          {
            if (!flag)
              builder.Append(", ");
            this.AppendValue(builder, obj);
            flag = false;
          }
        }
      }

      public void ClearShortName()
      {
        this.shortName = (string) null;
      }
    }
  }
}
