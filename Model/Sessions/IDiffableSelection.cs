﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sessions.IDiffableSelection
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model;

namespace HibernatingRhinos.Profiler.Client.Model.Sessions
{
  public interface IDiffableSelection
  {
    bool CanDiff { get; }

    ISessionPresenterItem First { get; }

    ISessionPresenterItem Second { get; }
  }
}
