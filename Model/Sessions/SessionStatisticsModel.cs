﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sessions.SessionStatisticsModel
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client.Model;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Model.Sessions
{
  public class SessionStatisticsModel : PropertyChangedBase
  {
    public int NumberOfStatements { get; private set; }

    public int NumberOfCachedStatements { get; private set; }

    public int NumberOfTransactionsStatements { get; private set; }

    public string ShortDescription
    {
      get
      {
        if (this.NumberOfCachedStatements == 0)
          return "[" + (object) this.NumberOfStatements + "]";
        return "[" + (object) this.NumberOfCachedStatements + " / " + (string) (object) this.NumberOfStatements + "]";
      }
    }

    public string LongDescription
    {
      get
      {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("Database Statements: ").Append(this.NumberOfStatements).AppendLine();
        stringBuilder.Append("Transaction Statements: ").Append(this.NumberOfTransactionsStatements).AppendLine();
        if (this.NumberOfCachedStatements != 0)
          stringBuilder.Append("Cached Statements: ").Append(this.NumberOfCachedStatements).AppendLine();
        return stringBuilder.Remove(stringBuilder.Length - 2, 2).ToString();
      }
    }

    public void UpdateFrom(SessionStatistics statistics, SessionModel model)
    {
      this.NumberOfStatements = statistics.NumberOfStatements;
      this.NumberOfCachedStatements = statistics.NumberOfCachedStatements;
      this.NumberOfTransactionsStatements = statistics.NumberOfTransactionsStatements;
      model.DisplayName = statistics.Name;
      model.SetWarnings(statistics.HasWarnings);
      model.SetSuggestions(statistics.HasSuggestions);
      this.NotifyOfPropertyChange("");
    }
  }
}
