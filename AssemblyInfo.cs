﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyDelaySign(false)]
[assembly: NeutralResourcesLanguage("en-US")]
[assembly: AssemblyConfiguration("Release")]
[assembly: InternalsVisibleTo("HibernatingRhinos.Profiler.IntegrationTests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c12d33fea1ddb56449d7dd65e0655457d5a750407bac61ac2532b078327cf9bc7dc67329cb6f80af1bbcf08c9898b22b7fc60376f1599e7902793d275c5c7d5a079e109e6e1331af8771117172718b4107f3c0a6bfad022e7e443e0eed6111135945d111b2cff0a5b9f217699c36c9860dad3d63ec74dbae0b2cafec2b8488a2")]
[assembly: AssemblyCompany("Hibernating Rhinos")]
[assembly: CLSCompliant(true)]
//[assembly: Extension]
[assembly: InternalsVisibleTo("HibernatingRhinos.Profiler.Client, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c12d33fea1ddb56449d7dd65e0655457d5a750407bac61ac2532b078327cf9bc7dc67329cb6f80af1bbcf08c9898b22b7fc60376f1599e7902793d275c5c7d5a079e109e6e1331af8771117172718b4107f3c0a6bfad022e7e443e0eed6111135945d111b2cff0a5b9f217699c36c9860dad3d63ec74dbae0b2cafec2b8488a2")]
[assembly: AssemblyFileVersion("2.0.2239.0")]
[assembly: SuppressIldasm]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: AssemblyCopyright("© Hibernating Rhinos 2004 - 2012. All rights reserved.")]
[assembly: AssemblyInformationalVersion("2.0.0 / dcb27d3")]
[assembly: AssemblyProduct("LinqToSqlProf")]
[assembly: AssemblyDescription("Speed Up Your Application!")]
[assembly: AssemblyVersion("2.0.0.0")]
