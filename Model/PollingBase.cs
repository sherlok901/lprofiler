﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.PollingBase
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Infrastructure;
using System;
using System.Windows.Forms;

namespace HibernatingRhinos.Profiler.Client.Model
{
  public abstract class PollingBase : SelectionBase
  {
    [CLSCompliant(false)]
    protected static readonly Timer Timer = new Timer()
    {
      Interval = 250
    };
    [CLSCompliant(false)]
    protected PollingState state;

    public static void Start()
    {
      PollingBase.Timer.Start();
    }

    public static void Stop()
    {
      PollingBase.Timer.Stop();
    }

    protected virtual void OnActivate()
    {
    }

    public virtual void Activate()
    {
      if (this.IsSelected)
        return;
      this.IsSelected = true;
      PollingBase.Timer.Tick += new EventHandler(this.TimerTicked);
      this.OnActivate();
    }

    public virtual void Deactivate()
    {
      if (!this.IsSelected)
        return;
      this.IsSelected = false;
      PollingBase.Timer.Tick -= new EventHandler(this.TimerTicked);
    }

    public abstract void TimerTicked(object sender, EventArgs e);

    public void DonePolling()
    {
      this.state = PollingState.NotFinalized;
    }
  }
}
