﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.ReportPresenter
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model.Reports;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HibernatingRhinos.Profiler.Client.Model
{
  public class ReportPresenter : Presenter, ISupportFiltering
  {
    private readonly ObservableCollection<Report> reports;
    private Report selectedReport;
    private Action whenFilteringSupportedChanged;

    public override string Name
    {
      get
      {
        return "Analysis";
      }
    }

    public ICollection<Report> Reports
    {
      get
      {
        return (ICollection<Report>) this.reports;
      }
    }

    public Report SelectedReport
    {
      get
      {
        if (this.selectedReport == null)
        {
          this.selectedReport = this.reports[0];
          this.selectedReport.Activate();
        }
        return this.selectedReport;
      }
      set
      {
        if (this.selectedReport == value)
          return;
        if (this.selectedReport != null)
          this.selectedReport.Deactivate();
        this.selectedReport = value;
        if (this.selectedReport != null)
          this.selectedReport.Activate();
        if (this.whenFilteringSupportedChanged == null)
          return;
        this.whenFilteringSupportedChanged();
      }
    }

    public bool FilteringSupported
    {
      get
      {
        if (this.SelectedReport == null)
          return false;
        return this.SelectedReport.FilteringSupported;
      }
    }

    public ReportPresenter(params Report[] reports)
    {
      this.reports = new ObservableCollection<Report>((IEnumerable<Report>) reports);
    }

    public override void Clear()
    {
      foreach (Report report in (Collection<Report>) this.reports)
      {
        report.Clear();
        report.TimerTicked((object) this, EventArgs.Empty);
      }
    }

    public void CallWhenFilteringSupportedChanged(Action action)
    {
      this.whenFilteringSupportedChanged = action;
    }
  }
}
