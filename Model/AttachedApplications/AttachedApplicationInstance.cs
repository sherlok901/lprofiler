﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.AttachedApplications.AttachedApplicationInstance
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client.Model.AttachedApplications
{
  public class AttachedApplicationInstance
  {
    public Guid Id { get; set; }

    public override bool Equals(object obj)
    {
      AttachedApplicationInstance applicationInstance = obj as AttachedApplicationInstance;
      if (applicationInstance == null)
        return false;
      return this.Id == applicationInstance.Id;
    }

    public override int GetHashCode()
    {
      return this.Id.GetHashCode();
    }
  }
}
