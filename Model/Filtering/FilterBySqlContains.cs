﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Filtering.FilterBySqlContains
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using System;

namespace HibernatingRhinos.Profiler.Client.Model.Filtering
{
  [Serializable]
  public class FilterBySqlContains : AbstractFilter
  {
    private bool contains = true;
    private string text;

    public bool[] SupportedOperators
    {
      get
      {
        return new bool[2]
        {
          true,
          false
        };
      }
    }

    public override bool IsValid
    {
      get
      {
        return !string.IsNullOrEmpty(this.Text);
      }
    }

    public bool Contains
    {
      get
      {
        return this.contains;
      }
      set
      {
        this.contains = value;
      }
    }

    public string Text
    {
      get
      {
        return this.text;
      }
      set
      {
        this.text = value;
      }
    }

    public override string ToolTip
    {
      get
      {
        return string.Format("The raw sql {1}contains '{0}'.", (object) this.Text, this.contains ? (object) (string) null : (object) "not ");
      }
    }

    public override bool IsValidStatement(IFilterableStatementSnapshot snapshot)
    {
      return snapshot.RawSql.Contains(this.Text) == this.Contains;
    }
  }
}
