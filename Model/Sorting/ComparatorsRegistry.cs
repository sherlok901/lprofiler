﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sorting.ComparatorsRegistry
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;
using System.Collections;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Client.Model.Sorting
{
  public class ComparatorsRegistry
  {
    public IDictionary<string, IComparer> Comparators { get; private set; }

    public ComparatorsRegistry()
    {
      this.Comparators = (IDictionary<string, IComparer>) new Dictionary<string, IComparer>();
    }

    public IComparer Resolve(string name)
    {
      foreach (KeyValuePair<string, IComparer> keyValuePair in (IEnumerable<KeyValuePair<string, IComparer>>) this.Comparators)
      {
        if (name == keyValuePair.Key)
          return this.Comparators[name];
      }
      throw new InvalidOperationException(string.Format("Comparator for {0} does not exist", (object) name));
    }

    public void Register(string name, IComparer comparator)
    {
      if (this.Comparators.ContainsKey(name))
        return;
      this.Comparators.Add(name, comparator);
    }
  }
}
