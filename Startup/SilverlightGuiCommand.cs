﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.SilverlightGuiCommand
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Host;
using HibernatingRhinos.Profiler.Client.Host.Startup;
using log4net;
using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  public class SilverlightGuiCommand : IStartupCommand
  {
    private readonly ILog log = LogManager.GetLogger(typeof (SilverlightGuiCommand));

    public int Execute()
    {
      SilverlightLauncher silverlightLauncher = new SilverlightLauncher();
      silverlightLauncher.EnsureSilverlightInstalled();
      silverlightLauncher.OpenSilverlightInterface().ContinueWith((Action<Task<Process>>) (task =>
      {
        if (task.Result == null)
          return;
        task.Result.EnableRaisingEvents = true;
        task.Result.Exited += (EventHandler) ((sender, args) => Environment.Exit(0));
      }));
      try
      {
        Application.EnableVisualStyles();
        Application.SetCompatibleTextRenderingDefault(false);
        OutOfProcessClientService processClientService = new OutOfProcessClientService();
        processClientService.Initialize();
        using (TrayIcon trayIcon = new TrayIcon())
        {
          trayIcon.OnClose += new Action(((ClientService) processClientService).Close);
          trayIcon.Display();
          processClientService.StartWithOutOfProcessListener(StartupParser.CurrentArguments.ContextFile);
        }
      }
      catch (TypeLoadException ex)
      {
        this.log.Error((object) ".NET Framework 4.5 is required. Please install it.", (Exception) ex);
        this.log.Debug((object) ("Got TypeLoadException. Tried to load " + ex.TypeName));
        int num = (int) MessageBox.Show("Please install .NET 4.5 on this machine in order to run the profiler.", "", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        SilverlightGuiCommand.CloseTheSilverlightInterface();
        return 1;
      }
      catch (Exception ex)
      {
        this.log.Error((object) "An error occurred while starting the backend client service. Please send us the log.txt file to support@hibernatingrhinos.com", ex);
        int num = (int) MessageBox.Show(ex.ToString(), "Error in the profiler", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        SilverlightGuiCommand.CloseTheSilverlightInterface();
        return 1;
      }
      return 0;
    }

    private static void CloseTheSilverlightInterface()
    {
      IntPtr silverlightGuiHandle = SilverlightLauncher.GetSilverlightGuiHandle();
      if (!(silverlightGuiHandle != IntPtr.Zero))
        return;
      Win32.CloseWindow(silverlightGuiHandle);
    }
  }
}
