﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.ProfilerLicenseValidator
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using Rhino.Licensing;
using Rhino.Licensing.Discovery;
using System;

namespace HibernatingRhinos.Profiler.Client.Model
{
  internal sealed class ProfilerLicenseValidator : LicenseValidator
  {
    private const string SubscriptionEndpointUrl = "http://uberprof.com/Subscriptions.svc";

    public ProfilerLicenseValidator(string publicKey, string licensePath)
      : base(publicKey, licensePath)
    {
      this.Initialize();
    }

    public ProfilerLicenseValidator(string publicKey, string licensePath, string licenseServerUrl, Guid clientId)
      : base(publicKey, licensePath, licenseServerUrl, clientId)
    {
      this.Initialize();
    }

    private void Initialize()
    {
      this.SubscriptionEndpoint = "http://uberprof.com/Subscriptions.svc";
      this.LicenseInvalidated += new Action<InvalidationType>(this.OnLicenseInvalidated);
      this.MultipleLicensesWereDiscovered += new EventHandler<DiscoveryHost.ClientDiscoveredEventArgs>(this.OnMultipleLicensesWereDiscovered);
    }

    private void OnMultipleLicensesWereDiscovered(object sender, DiscoveryHost.ClientDiscoveredEventArgs clientDiscoveredEventArgs)
    {
      AbstractLicenseValidator.Logger.ErrorFormat("A duplicate license was found at {0} for user {1}. User Id: {2}. Both licenses were disabled!", (object) clientDiscoveredEventArgs.MachineName, (object) clientDiscoveredEventArgs.UserName, (object) clientDiscoveredEventArgs.UserId);
    }

    private void OnLicenseInvalidated(InvalidationType invalidationType)
    {
      AbstractLicenseValidator.Logger.Error((object) "The license have expired and can no longer be used");
    }

    public override void AssertValidLicense()
    {
      //base.AssertValidLicense();
      //string licenseProfile;
      //this.LicenseAttributes.TryGetValue("prof", out licenseProfile);
      //if (licenseProfile == "Uber")
      //  return;
      //if (!Profile.Current.IsMatch(licenseProfile))
      //{
      //  AbstractLicenseValidator.Logger.WarnFormat("Not accepting '{0}' license for '{1}'", (object) licenseProfile, (object) Profile.CurrentProfileDisplayName);
      //  throw new NotValidLicenseFileException("The license you provided isn't valid for this profile of the application." + Environment.NewLine + "License is for: '" + licenseProfile + "' but the current application profile is: " + Profile.CurrentProfileDisplayName);
      //}
      //AbstractLicenseValidator.Logger.DebugFormat("License accepted for {0}", (object) Profile.CurrentProfileDisplayName);
    }
  }
}
