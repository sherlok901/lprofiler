﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Reports.UrlQuery
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client.Infrastructure;
using HibernatingRhinos.Profiler.Client.Model.Filtering;
using HibernatingRhinos.Profiler.Client.Model.Sorting;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Reports
{
  public class UrlQuery : SelectionBase
  {
    private readonly FilterServiceModel filterService;
    private QueryAggregation currentStatement;
    private readonly Sorter<QueryAggregation> sorter;
    private readonly ColumnResolversRegistry<QueryAggregation> columnResolvers;
    private ObservableCollection<QueryAggregation> unsortedStatements;
    private ObservableCollection<QueryAggregation> unfilteredStatements;

    public SortSpecification<QueryAggregation> CurrentSort { get; set; }

    public string Url { get; set; }

    public ObservableCollection<QueryAggregation> Statements
    {
      get
      {
        return new ObservableCollection<QueryAggregation>(Enumerable.Where<QueryAggregation>((IEnumerable<QueryAggregation>) this.unfilteredStatements, (Func<QueryAggregation, bool>) (x => this.filterService.FilterStatement((IFilterableStatementSnapshot) x))));
      }
    }

    public QueryAggregation CurrentStatement
    {
      get
      {
        if (Enumerable.Count<QueryAggregation>((IEnumerable<QueryAggregation>) this.Statements) == 0)
          return new QueryAggregation();
        if (this.currentStatement == null)
          this.CurrentStatement = Enumerable.FirstOrDefault<QueryAggregation>((IEnumerable<QueryAggregation>) this.Statements);
        return this.currentStatement;
      }
      set
      {
        if (value == null || value == this.currentStatement)
          return;
        if (this.currentStatement != null)
          this.currentStatement.IsSelected = false;
        this.currentStatement = value;
        if (this.currentStatement == null)
          return;
        this.currentStatement.IsSelected = true;
      }
    }

    public UrlQuery(FilterServiceModel filterService, ITrackingService trackingService)
    {
      this.filterService = filterService;
      this.unfilteredStatements = new ObservableCollection<QueryAggregation>();
      this.unfilteredStatements.CollectionChanged += new NotifyCollectionChangedEventHandler(this.UnfilteredStatementsOnCollectionChanged);
      this.unsortedStatements = new ObservableCollection<QueryAggregation>();
      this.sorter = new Sorter<QueryAggregation>();
      this.columnResolvers = new ColumnResolversRegistry<QueryAggregation>();
      this.columnResolvers.Register("ShortSql", (Func<QueryAggregation, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("ShortSql", (object) x.ShortSql)));
      this.columnResolvers.Register("AvgDuration", (Func<QueryAggregation, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("AvgDuration", (object) x.AverageDuration)));
      this.columnResolvers.Register("QueryCount", (Func<QueryAggregation, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("QueryCount", (object) x.Count)));
      this.CurrentSort = new SortSpecification<QueryAggregation>(trackingService)
      {
        Direction = ListSortDirection.Descending
      };
    }

    private void UnfilteredStatementsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
    {
      this.SortBySelectedColumn();
    }

    public void UpdateFrom(UrlQueriesSnapshot snapshot)
    {
      foreach (QueryAggregationSnapshot aggregationSnapshot in snapshot.Statements)
      {
        QueryAggregationSnapshot statement = aggregationSnapshot;
        QueryAggregation queryAggregation = Enumerable.FirstOrDefault<QueryAggregation>(Enumerable.Where<QueryAggregation>((IEnumerable<QueryAggregation>) this.Statements, (Func<QueryAggregation, bool>) (item => item.RawSql == statement.RawSql)));
        if (queryAggregation == null)
        {
          QueryAggregation from = QueryAggregation.CreateFrom(statement);
          this.unfilteredStatements.Add(from);
          this.unsortedStatements.Add(from);
        }
        else
        {
          int count = queryAggregation.Count;
          double? averageDuration1 = queryAggregation.AverageDuration;
          queryAggregation.UpdateFrom(statement);
          if (count == queryAggregation.Count)
          {
            double? nullable = averageDuration1;
            double? averageDuration2 = queryAggregation.AverageDuration;
            if ((nullable.GetValueOrDefault() != averageDuration2.GetValueOrDefault() ? 1 : (nullable.HasValue != averageDuration2.HasValue ? 1 : 0)) == 0)
              continue;
          }
          this.SortBySelectedColumn();
        }
      }
    }

    public static UrlQuery CreateFrom(UrlQueriesSnapshot snapshot, FilterServiceModel filterService, ITrackingService trackingService)
    {
      UrlQuery urlQuery = new UrlQuery(filterService, trackingService)
      {
        Url = snapshot.Url
      };
      foreach (QueryAggregationSnapshot statement in snapshot.Statements)
      {
        QueryAggregation from = QueryAggregation.CreateFrom(statement);
        urlQuery.unfilteredStatements.Add(from);
        urlQuery.unsortedStatements.Add(from);
      }
      return urlQuery;
    }

    public void Sort(string columnName)
    {
      this.CurrentSort.SortColumnResolver = this.columnResolvers.Resolve(columnName);
      this.CurrentSort.ColumnName = columnName;
      this.unfilteredStatements = new ObservableCollection<QueryAggregation>(this.sorter.Sort((IList<QueryAggregation>) this.unfilteredStatements, this.CurrentSort));
      this.unfilteredStatements.CollectionChanged += new NotifyCollectionChangedEventHandler(this.UnfilteredStatementsOnCollectionChanged);
    }

    public void SortBySelectedColumn()
    {
      if (this.CurrentSort.IsUndefined)
        return;
      this.unfilteredStatements = new ObservableCollection<QueryAggregation>(this.sorter.Sort((IList<QueryAggregation>) this.unfilteredStatements, this.CurrentSort));
      this.unfilteredStatements.CollectionChanged += new NotifyCollectionChangedEventHandler(this.UnfilteredStatementsOnCollectionChanged);
    }

    public void CancelSorting()
    {
      this.CurrentSort.IsUndefined = true;
      this.CurrentSort.ColumnName = "";
      this.unfilteredStatements = this.unsortedStatements;
      this.unfilteredStatements.CollectionChanged += new NotifyCollectionChangedEventHandler(this.UnfilteredStatementsOnCollectionChanged);
    }

    public void Clear()
    {
      this.unfilteredStatements.Clear();
      this.unsortedStatements.Clear();
    }
  }
}
