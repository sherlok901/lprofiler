﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.ProfArguments
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  public class ProfArguments
  {
    public string File = Profile.CurrentProfile + " Report.xml";
    public HibernatingRhinos.Profiler.Client.Startup.ReportFormat[] ReportFormat = new HibernatingRhinos.Profiler.Client.Startup.ReportFormat[1];
    public bool CmdLineMode;
    public int Port;
    public string InputFile;
    public string LicensePath;
    public bool Shutdown;
    public string ContextFile;

    public int SpecifiedPortOrDefaultPort
    {
      get
      {
        if (this.Port != 0)
          return this.Port;
        return UserPreferencesHolder.UserSettings.ListenPort;
      }
    }

    public bool GuiMode
    {
      get
      {
        if (!this.CmdLineMode)
          return !this.Shutdown;
        return false;
      }
    }
  }
}
