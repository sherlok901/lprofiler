﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Visitors.AbstractVisitor
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Reports;
using HibernatingRhinos.Profiler.Client.Model.Sessions;
using HibernatingRhinos.Profiler.Client.Model.Statements;
using System;
using System.Collections.ObjectModel;

namespace HibernatingRhinos.Profiler.Client.Model.Visitors
{
  public abstract class AbstractVisitor
  {
    public virtual void Visit(ApplicationModel model)
    {
      model.TimerTicked((object) model, EventArgs.Empty);
      this.Visit(model.RecentStatements);
      foreach (SessionModel session in (Collection<SessionModel>) model.Sessions)
        this.Visit(session);
      this.Visit(model.Statistics);
      this.Visit((Report) model.UniqueQueries);
      this.Visit((Report) model.OverallUsage);
      this.Visit((Report) model.ExpensiveQueries);
      this.Visit((Report) model.QueriesByIsolationLevel);
      this.Visit((Report) model.QueriesByUrl);
      this.Visit((Report) model.QueriesByMethod);
    }

    protected virtual void Visit(Report report)
    {
    }

    protected virtual void Visit(StatisticsModel statistics)
    {
    }

    protected virtual void Visit(SessionModel session)
    {
      foreach (IStatementModel statement in session.SessionStatements.Statements)
        this.Visit(statement);
    }

    protected virtual void Visit(RecentStatementsModel recentStatements)
    {
      foreach (IStatementModel statement in recentStatements.Statements)
        this.Visit(statement);
    }

    protected virtual void Visit(IStatementModel statement)
    {
      foreach (StatementAlert alert in (Collection<StatementAlert>) statement.Alerts)
        this.Visit(alert);
    }

    protected virtual void Visit(StatementAlert alert)
    {
    }
  }
}
