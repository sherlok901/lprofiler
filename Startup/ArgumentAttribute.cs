﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Startup.ArgumentAttribute
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client.Startup
{
  [AttributeUsage(AttributeTargets.Field)]
  public class ArgumentAttribute : Attribute
  {
    private string shortName;
    private string longName;
    private string helpText;
    private object defaultValue;
    private ArgumentType type;

    public ArgumentType Type
    {
      get
      {
        return this.type;
      }
    }

    public bool DefaultShortName
    {
      get
      {
        return null == this.shortName;
      }
    }

    public string ShortName
    {
      get
      {
        return this.shortName;
      }
      set
      {
        this.shortName = value;
      }
    }

    public bool DefaultLongName
    {
      get
      {
        return null == this.longName;
      }
    }

    public string LongName
    {
      get
      {
        return this.longName;
      }
      set
      {
        this.longName = value;
      }
    }

    public object DefaultValue
    {
      get
      {
        return this.defaultValue;
      }
      set
      {
        this.defaultValue = value;
      }
    }

    public bool HasDefaultValue
    {
      get
      {
        return null != this.defaultValue;
      }
    }

    public bool HasHelpText
    {
      get
      {
        return null != this.helpText;
      }
    }

    public string HelpText
    {
      get
      {
        return this.helpText;
      }
      set
      {
        this.helpText = value;
      }
    }

    public ArgumentAttribute(ArgumentType type)
    {
      this.type = type;
    }
  }
}
