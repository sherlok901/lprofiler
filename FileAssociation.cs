﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.FileAssociation
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.Client.Host;
using log4net;
using Microsoft.Win32;
using System;
using System.Reflection;

namespace HibernatingRhinos.Profiler.Client
{
  public class FileAssociation
  {
    private readonly ILog log = LogManager.GetLogger(typeof (FileAssociation));
    private readonly IProfile profile;
    private readonly string profilerShortName;
    private readonly string extension;
    private readonly string progId;
    private readonly string currentBuild;

    private FileAssociation(IProfile profile)
    {
      this.profile = profile;
      this.profilerShortName = profile.ShortName;
      this.extension = "." + this.profilerShortName;
      this.progId = this.profilerShortName;
      this.currentBuild = BuildInfo.GetCurrentBuild();
    }

    private void Associate()
    {
      string str1 = ProfileExtensions.Translate(this.profile, "[[profile]] Profiler");
      string location = Assembly.GetEntryAssembly().Location;
      string str2 = string.Format("\"{0}\",0", (object) location);
      string str3 = string.Format("\"{0}\" /ContextFile:\"%1\"", (object) location);
      try
      {
        using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software\\Classes", true))
        {
          using (RegistryKey subKey1 = registryKey.CreateSubKey(this.extension))
          {
            using (RegistryKey subKey2 = registryKey.CreateSubKey(this.progId))
            {
              using (RegistryKey subKey3 = subKey2.CreateSubKey("DefaultIcon"))
              {
                using (RegistryKey subKey4 = subKey2.CreateSubKey("shell\\open\\command"))
                {
                  using (RegistryKey subKey5 = subKey2.CreateSubKey("build"))
                  {
                    subKey1.SetValue("", (object) this.progId);
                    subKey2.SetValue("", (object) str1);
                    subKey3.SetValue("", (object) str2);
                    subKey4.SetValue("", (object) str3);
                    subKey5.SetValue("", (object) this.currentBuild);
                  }
                }
              }
            }
          }
        }
      }
      catch (Exception ex)
      {
        this.log.Error((object) ex);
      }
    }

    private bool IsNeedToBeAssociated()
    {
      try
      {
        using (RegistryKey registryKey1 = Registry.CurrentUser.OpenSubKey("Software\\Classes", true))
        {
          using (RegistryKey registryKey2 = registryKey1.OpenSubKey(this.extension))
          {
            using (RegistryKey progIdKey = registryKey1.OpenSubKey(this.progId))
              return registryKey2 == null || progIdKey == null || this.NotTheSameBuild(progIdKey);
          }
        }
      }
      catch
      {
        return true;
      }
    }

    private bool NotTheSameBuild(RegistryKey progIdKey)
    {
      using (RegistryKey registryKey = progIdKey.OpenSubKey("build"))
      {
        string str = registryKey == null ? (string) null : registryKey.GetValue("") as string;
        return str == "PRIVATE-BUILD" || str != this.currentBuild;
      }
    }

    public static void SetupAssociation()
    {
      FileAssociation fileAssociation = new FileAssociation(Profile.Current);
      if (!fileAssociation.IsNeedToBeAssociated())
        return;
      fileAssociation.Associate();
    }
  }
}
