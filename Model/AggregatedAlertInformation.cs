﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.AggregatedAlertInformation
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Client.Model
{
  public class AggregatedAlertInformation : IAggregatedAlertSnapshot
  {
    public AlertInformation Alert { get; set; }

    public int Count { get; set; }

    public AggregatedAlertInformation(KeyValuePair<AlertInformation, int> pair)
    {
      this.Alert = pair.Key;
      this.Count = pair.Value;
    }
  }
}
