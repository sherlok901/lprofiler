﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sessions.StatementsExtensions
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.Client.Model.Statements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HibernatingRhinos.Profiler.Client.Model.Sessions
{
  public static class StatementsExtensions
  {
    public static string Aggregate(this IEnumerable<IStatementModel> self, Func<IStatementModel, string> func)
    {
      StringBuilder stringBuilder = new StringBuilder();
      int num = 1;
      foreach (IStatementModel statementModel in self)
      {
        stringBuilder.AppendLine("-- statement #" + (object) num).AppendLine(func(statementModel)).AppendLine();
        ++num;
      }
      return stringBuilder.ToString();
    }

    public static QueryDuration Aggregate(this IEnumerable<IStatementModel> self, Func<IStatementModel, QueryDuration> func)
    {
      QueryDuration queryDuration1 = new QueryDuration()
      {
        InDatabase = new int?(0),
        InNHibernate = new int?(0)
      };
      foreach (IStatementModel statementModel in self)
      {
        QueryDuration queryDuration2 = queryDuration1;
        int? inDatabase = queryDuration2.InDatabase;
        int num1 = func(statementModel).InDatabase ?? 0;
        int? nullable1 = inDatabase.HasValue ? new int?(inDatabase.GetValueOrDefault() + num1) : new int?();
        queryDuration2.InDatabase = nullable1;
        QueryDuration queryDuration3 = queryDuration1;
        int? inNhibernate = queryDuration3.InNHibernate;
        int num2 = func(statementModel).InNHibernate ?? 0;
        int? nullable2 = inNhibernate.HasValue ? new int?(inNhibernate.GetValueOrDefault() + num2) : new int?();
        queryDuration3.InNHibernate = nullable2;
      }
      if (Enumerable.Count<IStatementModel>(Enumerable.Where<IStatementModel>(self, (Func<IStatementModel, bool>) (x => !x.Duration.InDatabase.HasValue))) == Enumerable.Count<IStatementModel>(self))
        queryDuration1.InDatabase = new int?();
      if (Enumerable.Count<IStatementModel>(Enumerable.Where<IStatementModel>(self, (Func<IStatementModel, bool>) (x => !x.Duration.InNHibernate.HasValue))) == Enumerable.Count<IStatementModel>(self))
        queryDuration1.InNHibernate = new int?();
      return queryDuration1;
    }

    public static int[] Aggregate(this IEnumerable<IStatementModel> self, Func<IStatementModel, int[]> func)
    {
      int length = Enumerable.Aggregate<IStatementModel, int>(self, 0, (Func<int, IStatementModel, int>) ((current, model) =>
      {
        if (model.CountOfRows.Length <= current)
          return current;
        return model.CountOfRows.Length;
      }));
      int[] numArray = new int[length];
      using (IEnumerator<IStatementModel> enumerator = self.GetEnumerator())
      {
label_5:
        while (enumerator.MoveNext())
        {
          IStatementModel current = enumerator.Current;
          int index = 0;
          while (true)
          {
            if (index < length && index < Enumerable.Count<int>((IEnumerable<int>) func(current)))
            {
              numArray[index] += func(current)[index];
              ++index;
            }
            else
              goto label_5;
          }
        }
      }
      return numArray;
    }
  }
}
