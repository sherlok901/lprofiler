﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Statements.RecentStatementsModel
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.Client.Model;
using HibernatingRhinos.Profiler.Client.Model.Filtering;
using HibernatingRhinos.Profiler.Client.Model.Sorting;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace HibernatingRhinos.Profiler.Client.Model.Statements
{
  public class RecentStatementsModel : PollingBase, ISessionPresenterItem, IExposeSelectableStatements
  {
    private const int MaxStatements = 100;
    private readonly IBackendBridge backend;
    private readonly FilterServiceModel filter;
    private readonly Sorter<IStatementModel> sorter;
    private readonly ColumnResolversRegistry<IStatementModel> columnResolvers;
    private IStatementModel selectedStatement;
    private readonly ObservableCollection<IStatementModel> unfilteredStatements;
    private readonly ApplicationModel applicationModel;

    public DateTime? Duration { get; set; }

    public Uri Url { get; set; }

    public ObservableCollection<IStatementModel> UnfilteredStatements
    {
      get
      {
        return this.unfilteredStatements;
      }
    }

    public bool HasSuggestions
    {
      get
      {
        return Enumerable.Any<IStatementModel>((IEnumerable<IStatementModel>) this.Statements, (Func<IStatementModel, bool>) (x => x.HasSuggestions));
      }
    }

    public bool HasWarnings
    {
      get
      {
        return Enumerable.Any<IStatementModel>((IEnumerable<IStatementModel>) this.Statements, (Func<IStatementModel, bool>) (x => x.HasWarnings));
      }
    }

    public int StatementCount
    {
      get
      {
        return Enumerable.Count<IStatementModel>(Enumerable.Where<IStatementModel>((IEnumerable<IStatementModel>) this.Statements, (Func<IStatementModel, bool>) (x => !x.IsTransaction)));
      }
    }

    public List<IStatementModel> Statements
    {
      get
      {
        IEnumerable<IStatementModel> enumerable = Enumerable.Where<IStatementModel>((IEnumerable<IStatementModel>) this.unfilteredStatements, (Func<IStatementModel, bool>) (x => this.filter.FilterStatement((IFilterableStatementSnapshot) x)));
        if (this.CurrentSort.IsUndefined)
          return new List<IStatementModel>(enumerable);
        return new List<IStatementModel>((IEnumerable<IStatementModel>) this.sorter.Sort((IList<IStatementModel>) Enumerable.ToList<IStatementModel>(enumerable), this.CurrentSort));
      }
    }

    public SortSpecification<IStatementModel> CurrentSort { get; set; }

    public IStatementModel SelectedStatement
    {
      get
      {
        if (this.selectedStatement == null && this.Statements.Count > 0)
          this.SelectedStatement = this.Statements[0];
        return this.selectedStatement;
      }
      set
      {
        if (!this.IsSelected || this.selectedStatement == value)
          return;
        foreach (IStatementModel statementModel in this.Statements)
          statementModel.IsSelected = false;
        this.selectedStatement = value;
        if (this.selectedStatement == null)
          return;
        this.selectedStatement.IsSelected = true;
      }
    }

    public string Name
    {
      get
      {
        return "Recent Statements";
      }
    }

    public IList<PollingBase> Views
    {
      get
      {
        return (IList<PollingBase>) new List<PollingBase>()
        {
          (PollingBase) this
        };
      }
    }

    public PollingBase CurrentView
    {
      get
      {
        return (PollingBase) this;
      }
      set
      {
      }
    }

    public IExposeSelectableStatements StatementsModel
    {
      get
      {
        return (IExposeSelectableStatements) this;
      }
    }

    public DateTime Timestamp
    {
      get
      {
        return DateTime.Now;
      }
    }

    public bool CanBeStarred { get; set; }

    public event Action<List<IStatementModel>> StatementsChanged = param0 => {};

    public RecentStatementsModel(IBackendBridge backend, FilterServiceModel filter, ApplicationModel applicationModel)
    {
      this.backend = backend;
      this.applicationModel = applicationModel;
      this.filter = filter;
      this.unfilteredStatements = new ObservableCollection<IStatementModel>();
      this.unfilteredStatements.CollectionChanged += (NotifyCollectionChangedEventHandler) ((s, e) => this.RaiseNotifications());
      this.sorter = new Sorter<IStatementModel>();
      this.columnResolvers = new ColumnResolversRegistry<IStatementModel>();
      this.columnResolvers.Register("ShortSql", (Func<IStatementModel, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("ShortSql", (object) x.ShortText)));
      this.columnResolvers.Register("RowCount", (Func<IStatementModel, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("RowCount", (object) x.CountOfRows)));
      this.columnResolvers.Register("Duration", (Func<IStatementModel, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("Duration", (object) x.Duration)));
      this.columnResolvers.Register("Alerts", (Func<IStatementModel, KeyValuePair<string, object>>) (x => new KeyValuePair<string, object>("Alerts", (object) x.Alerts)));
      this.CurrentSort = new SortSpecification<IStatementModel>(applicationModel.TrackingService)
      {
        Direction = ListSortDirection.Descending
      };
      this.CanBeStarred = false;
    }

    protected override void OnActivate()
    {
      this.applicationModel.TrackingService.Track("Main", "Recent Statements", (string) null, new int?());
    }

    public ISessionPresenterItem RemoveFrom(IList<ISessionPresenterItem> items)
    {
      return (ISessionPresenterItem) null;
    }

    public void Clear()
    {
      this.unfilteredStatements.Clear();
      this.selectedStatement = (IStatementModel) null;
    }

    public override void TimerTicked(object sender, EventArgs e)
    {
      bool flag = false;
      foreach (StatementSnapshot statementSnapshot1 in this.applicationModel.RecentStatementSnapshots.Values)
      {
        StatementSnapshot statementSnapshot = statementSnapshot1;
        IStatementModel statementModel = Enumerable.FirstOrDefault<IStatementModel>((IEnumerable<IStatementModel>) this.unfilteredStatements, (Func<IStatementModel, bool>) (x => x.Id == statementSnapshot.StatementId));
        if (statementModel != null)
        {
          flag |= statementModel.UpdateWith(statementSnapshot);
        }
        else
        {
          flag = true;
          this.unfilteredStatements.Add((IStatementModel) new StatementModel(statementSnapshot)
          {
            ApplicationModel = this.applicationModel,
            CanJumpToSession = true
          });
          if (this.unfilteredStatements.Count > 100)
            this.unfilteredStatements.RemoveAt(0);
        }
      }
      if (!flag)
        return;
      this.RaiseNotifications();
    }

    private void RaiseNotifications()
    {
      this.StatementsChanged(this.Statements);
    }

    public void Sort(string columnName)
    {
      this.CurrentSort.SortColumnResolver = this.columnResolvers.Resolve(columnName);
      this.CurrentSort.ColumnName = columnName;
    }

    public void CancelSorting()
    {
      this.CurrentSort.IsUndefined = true;
      this.CurrentSort.ColumnName = "";
    }
  }
}
