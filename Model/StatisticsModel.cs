﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.StatisticsModel
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd.Bridge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace HibernatingRhinos.Profiler.Client.Model
{
  public class StatisticsModel : PollingBase
  {
    private static readonly Regex pascalCaseToWords = new Regex("([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))", RegexOptions.Compiled);
    private readonly Dictionary<string, string> keysToWordsCache = new Dictionary<string, string>();
    private Dictionary<string, object> currentStatisticsSet = new Dictionary<string, object>();
    private readonly Dictionary<string, StatisticEntry> statisticLookup = new Dictionary<string, StatisticEntry>();
    private readonly ApplicationModel model;
    private int currentIndex;
    private DateTime lastUpdate;
    private StatisticsSnapshot[] statisticsSet;

    public bool ShouldRemindToEnableStatistics
    {
      get
      {
        if (this.model.Sessions.Count <= 0)
          return false;
        if (this.currentStatisticsSet != null)
          return this.currentStatisticsSet.Count == 0;
        return true;
      }
    }

    public List<StatisticEntry> SortedStatistics { get; private set; }

    public bool CanNavigate
    {
      get
      {
        if (this.statisticsSet == null)
          return false;
        return this.statisticsSet.Length > 0;
      }
    }

    public StatisticsSnapshot[] AllStatistics
    {
      get
      {
        return this.statisticsSet;
      }
    }

    public StatisticsModel(ApplicationModel model)
    {
      this.model = model;
      this.SortedStatistics = new List<StatisticEntry>();
      this.Activate();
    }

    private string KeyToWordUsingCache(string key)
    {
      string str1;
      if (this.keysToWordsCache.TryGetValue(key, out str1))
        return str1;
      string str2 = StatisticsModel.pascalCaseToWords.Replace(key, "$1 ");
      this.keysToWordsCache[key] = str2;
      return str2;
    }

    public override void TimerTicked(object sender, EventArgs e)
    {
      if (DateTime.Now - this.lastUpdate < TimeSpan.FromSeconds(5.0))
        return;
      this.lastUpdate = DateTime.Now;
      this.statisticsSet = this.model.Backend.Notifications.GetStatisticsSnapshots();
      this.SetCurrentStatistics();
      this.NotifyOfPropertyChange("ShouldRemindToEnableStatistics");
      this.NotifyOfPropertyChange("CanNavigate");
    }

    public void Next()
    {
      if (this.statisticsSet.Length == 0)
        return;
      ++this.currentIndex;
      if (this.currentIndex >= this.statisticsSet.Length)
        this.currentIndex = 0;
      this.SetCurrentStatistics();
    }

    private void SetCurrentStatistics()
    {
      if (this.statisticsSet == null || this.statisticsSet.Length == 0 || this.statisticsSet[this.currentIndex] == null)
        return;
      this.currentStatisticsSet = this.statisticsSet[this.currentIndex].Statistics;
      this.DisplayName = this.statisticsSet[this.currentIndex].Name;
      if (this.SortedStatistics.Count != this.currentStatisticsSet.Count)
        this.InitializeStatistics();
      else
        this.UpdateExistingStatistics();
      this.NotifyOfPropertyChange((Func<object>) (() => (object) this.DisplayName));
    }

    private void InitializeStatistics()
    {
      this.SortedStatistics.Clear();
      this.statisticLookup.Clear();
      foreach (KeyValuePair<string, object> keyValuePair in (IEnumerable<KeyValuePair<string, object>>) Enumerable.OrderBy<KeyValuePair<string, object>, string>((IEnumerable<KeyValuePair<string, object>>) this.currentStatisticsSet, (Func<KeyValuePair<string, object>, string>) (x => x.Key)))
      {
        StatisticEntry statisticEntry = new StatisticEntry(this.KeyToWordUsingCache(keyValuePair.Key))
        {
          Value = keyValuePair.Value
        };
        this.SortedStatistics.Add(statisticEntry);
        this.statisticLookup.Add(keyValuePair.Key, statisticEntry);
      }
    }

    private void UpdateExistingStatistics()
    {
      foreach (KeyValuePair<string, object> keyValuePair in this.currentStatisticsSet)
        this.statisticLookup[keyValuePair.Key].Value = keyValuePair.Value;
    }

    public void AddStatistic(string label, string value)
    {
      this.currentStatisticsSet.Add(label, (object) value);
    }

    public void Previous()
    {
      if (this.statisticsSet.Length == 0)
        return;
      --this.currentIndex;
      if (this.currentIndex < 0)
        this.currentIndex = this.statisticsSet.Length - 1;
      this.SetCurrentStatistics();
    }
  }
}
