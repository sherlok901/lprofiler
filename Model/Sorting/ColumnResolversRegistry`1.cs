﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.Sorting.ColumnResolversRegistry`1
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;
using System.Collections.Generic;

namespace HibernatingRhinos.Profiler.Client.Model.Sorting
{
  public class ColumnResolversRegistry<T>
  {
    private readonly IDictionary<string, Func<T, KeyValuePair<string, object>>> columnResolvers;

    public ColumnResolversRegistry()
    {
      this.columnResolvers = (IDictionary<string, Func<T, KeyValuePair<string, object>>>) new Dictionary<string, Func<T, KeyValuePair<string, object>>>();
    }

    public void Register(string name, Func<T, KeyValuePair<string, object>> columnResolver)
    {
      this.columnResolvers.Add(name, columnResolver);
    }

    public Func<T, KeyValuePair<string, object>> Resolve(string name)
    {
      if (!this.columnResolvers.ContainsKey(name))
        throw new InvalidOperationException(string.Format("Column {0} has not assigned resolver", (object) name));
      return this.columnResolvers[name];
    }
  }
}
