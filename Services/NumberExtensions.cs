﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Services.NumberExtensions
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using System;

namespace HibernatingRhinos.Profiler.Client.Services
{
  public static class NumberExtensions
  {
    private const double BytesInMegabyte = 1048576.0;

    public static double Megabytes(this int integer)
    {
      return (double) integer / 1048576.0;
    }

    public static double Megabytes(this double @double)
    {
      return @double / 1048576.0;
    }

    public static double Percent(this double @double)
    {
      return Math.Round(@double * 100.0);
    }
  }
}
