﻿// Decompiled with JetBrains decompiler
// Type: HibernatingRhinos.Profiler.Client.Model.ApplicationModel
// Assembly: L2SProf, Version=2.0.0.0, Culture=neutral, PublicKeyToken=0774796e73ebf640
// MVID: D55CB258-DED9-433B-B5F9-59F6DA1A2E47
// Assembly location: C:\Users\Igor\Desktop\linqtosqlprofiler.2.0.2239\tools\L2SProf.exe

using HibernatingRhinos.Profiler.BackEnd;
using HibernatingRhinos.Profiler.BackEnd.Bridge;
using HibernatingRhinos.Profiler.BackEnd.ModelBuilding;
using HibernatingRhinos.Profiler.BackEnd.Profiles;
using HibernatingRhinos.Profiler.BackEnd.Reports;
using HibernatingRhinos.Profiler.Client;
using HibernatingRhinos.Profiler.Client.Host;
using HibernatingRhinos.Profiler.Client.Model.AttachedApplications;
using HibernatingRhinos.Profiler.Client.Model.Filtering;
using HibernatingRhinos.Profiler.Client.Model.Reports;
using HibernatingRhinos.Profiler.Client.Model.Sessions;
using HibernatingRhinos.Profiler.Client.Model.Statements;
using HibernatingRhinos.Profiler.Client.Services;
using HibernatingRhinos.Profiler.Client.Tracking;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;

namespace HibernatingRhinos.Profiler.Client.Model
{
  public class ApplicationModel : PollingBase
  {
    private readonly ReaderWriterLockSlim sessionsLock = new ReaderWriterLockSlim();
    public readonly Dictionary<Guid, Session> SessionsHolder = new Dictionary<Guid, Session>();
    private const int MaxRecentStatements = 500;
    private readonly IBackendBridge backend;
    private bool isCurrentlyPaused;
    private int recentStatementSnapshotsPosition;
    public int RecentStatementsEtag;
    private long backendNotificationsEtag;

    public string CurrentBuild
    {
      get
      {
        return BuildInfo.GetCurrentBuild();
      }
    }

    public ITrackingService TrackingService { get; private set; }

    public FilterServiceModel FilterService { get; private set; }

    public UniqueQueries UniqueQueries { get; set; }

    public OverallUsage OverallUsage { get; set; }

    public QueriesByMethod QueriesByMethod { get; set; }

    public QueriesByUrl QueriesByUrl { get; set; }

    public ExpensiveQueries ExpensiveQueries { get; set; }

    public QueriesByIsolationLevel QueriesByIsolationLevel { get; set; }

    public ObservableCollection<AttachedApplication> AttachedApplications { get; set; }

    public ObservableCollection<SessionModel> Sessions { get; set; }

    public RecentStatementsModel RecentStatements { get; private set; }

    public Dictionary<Guid, StatementSnapshot> RecentStatementSnapshots { get; private set; }

    public StatisticsModel Statistics { get; private set; }

    public bool IsCurrentlyPaused
    {
      get
      {
        return this.isCurrentlyPaused;
      }
      set
      {
        this.isCurrentlyPaused = value;
      }
    }

    public ObservableCollection<Presenter> Presenters { get; set; }

    public SessionPresenter SessionPresenter { get; set; }

    public ReportPresenter ReportPresenter { get; set; }

    public UserPreferences Settings
    {
      get
      {
        return UserPreferencesHolder.UserSettings;
      }
    }

    public IBackendBridge Backend
    {
      get
      {
        return this.backend;
      }
    }

    public ApplicationModel(IBackendBridge backend)
    {
      this.backend = backend;
      this.TrackingService = (ITrackingService) new NullTrackingService();
      this.Activate();
      this.RecentStatementSnapshots = new Dictionary<Guid, StatementSnapshot>();
    }

    public void Initialize(ITrackingService trackingService)
    {
      this.TrackingService = trackingService;
      this.FilterService = new FilterServiceModel();
      foreach (IFilter filter in this.Settings.Filters)
        this.FilterService.Filters.Add(filter);
      this.Sessions = new ObservableCollection<SessionModel>();
      this.AttachedApplications = new ObservableCollection<AttachedApplication>();
      this.RecentStatements = new RecentStatementsModel(this.backend, this.FilterService, this);
      this.SessionPresenter = new SessionPresenter(this.Sessions, this.RecentStatements, this.FilterService);
      this.OverallUsage = new OverallUsage(new Func<OverallUsageSnapshot>(this.backend.Reports.OverallUsage.GetReportSnapshot), this.TrackingService);
      this.UniqueQueries = new UniqueQueries(new Func<IEnumerable<QueryAggregationSnapshot>>(this.backend.Reports.UniqueQueries.GetReportSnapshot), this.FilterService, this.TrackingService);
      this.QueriesByMethod = new QueriesByMethod(new Func<IEnumerable<MethodQueriesSnapshot>>(this.backend.Reports.QueriesByMethod.GetReportSnapshot), this.FilterService, this.TrackingService);
      this.QueriesByUrl = new QueriesByUrl(new Func<IEnumerable<UrlQueriesSnapshot>>(this.backend.Reports.QueriesByUrl.GetReportSnapshot), this.FilterService, this.TrackingService);
      this.ExpensiveQueries = new ExpensiveQueries(new Func<IEnumerable<QueryAggregationSnapshot>>(this.backend.Reports.ExpensiveQueries.GetReportSnapshot), this.FilterService, this.TrackingService);
      this.QueriesByIsolationLevel = new QueriesByIsolationLevel(new Func<IEnumerable<IsolationLevelQueriesSnapshot>>(this.backend.Reports.QueriesByIsolationLevel.GetReportSnapshot), this.FilterService, this.TrackingService);
      this.ReportPresenter = new ReportPresenter(new Report[6]
      {
        (Report) this.UniqueQueries,
        (Report) this.ExpensiveQueries,
        (Report) this.QueriesByIsolationLevel,
        (Report) this.QueriesByMethod,
        (Report) this.QueriesByUrl,
        (Report) this.OverallUsage
      });
      this.Statistics = new StatisticsModel(this);
      ObservableCollection<Presenter> observableCollection = new ObservableCollection<Presenter>();
      observableCollection.Add((Presenter) this.SessionPresenter);
      observableCollection.Add((Presenter) this.ReportPresenter);
      this.Presenters = observableCollection;
    }

    public void Start()
    {
      this.TrackingService.Initialize();
    }

    private void OnRecentStatementsChanged(Statement statement)
    {
      this.sessionsLock.EnterWriteLock();
      try
      {
        if (this.RecentStatementSnapshots.Count > 500)
        {
          while (this.RecentStatementSnapshots.Count > 400)
            this.RecentStatementSnapshots.Remove(Enumerable.First<Guid>((IEnumerable<Guid>) this.RecentStatementSnapshots.Keys));
        }
        StatementSnapshot snapshot = statement.CreateSnapshot();
        snapshot.Position = ++this.recentStatementSnapshotsPosition;
        this.RecentStatementSnapshots[statement.InternalId] = snapshot;
        ++this.RecentStatementsEtag;
      }
      finally
      {
        this.sessionsLock.ExitWriteLock();
      }
    }

    private void OnSessionChanged(Session updatedSession)
    {
      this.sessionsLock.EnterWriteLock();
      try
      {
        SessionModel sessionModel = Enumerable.FirstOrDefault<SessionModel>((IEnumerable<SessionModel>) this.Sessions, (Func<SessionModel, bool>) (item => item.Id == updatedSession.InternalId));
        if (sessionModel == null)
          this.Sessions.Add(new SessionModel(this.backend, this.FilterService, updatedSession, this));
        else
          sessionModel.Update(updatedSession);
      }
      finally
      {
        this.sessionsLock.ExitWriteLock();
      }
    }

    public void Clear()
    {
      Guid[] sessionsIdsToKeep = Enumerable.ToArray<Guid>(Enumerable.Select<SessionModel, Guid>(Enumerable.Where<SessionModel>((IEnumerable<SessionModel>) this.Sessions, (Func<SessionModel, bool>) (x => x.Starred)), (Func<SessionModel, Guid>) (x => x.Id)));
      if (sessionsIdsToKeep.Length != 0)
      {
        this.ClearExcept(sessionsIdsToKeep);
      }
      else
      {
        this.backend.Clear();
        this.Sessions.Clear();
        this.SessionPresenter.Clear();
        this.ReportPresenter.Clear();
      }
    }

    public void DeleteTempFile()
    {
      this.backend.DeleteTempFile();
    }

    public void OpenAboutPage()
    {
      try
      {
        Process.Start(Profile.Current.AboutUrl);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Unable to open browser for about page because: " + ex.Message);
      }
    }

    public void OpenHelpTopic(string topic)
    {
      if (string.IsNullOrEmpty(topic))
        return;
      try
      {
        Process.Start(Profile.Current.GetLearnTopic("alert", topic));
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Unable to open browser for help topic: " + topic + " because: " + ex.Message);
      }
    }

    public void CommercialSupport()
    {
      try
      {
        Process.Start(Profile.Current.CommercialSupportUrl);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Unable to open browser to display commercial support options." + Environment.NewLine + "Please visit " + Profile.Current.CommercialSupportUrl + " directly." + Environment.NewLine + ex.Message);
      }
    }

    public void SaveModel(string filename)
    {
      this.Backend.SaveToFile(filename);
    }

    public void JumpTo(IStatementModel model)
    {
      SessionModel sessionModel = Enumerable.FirstOrDefault<SessionModel>((IEnumerable<SessionModel>) this.Sessions, (Func<SessionModel, bool>) (x => x.Id == model.SessionId));
      if (sessionModel == null || sessionModel == this.SessionPresenter.SelectedSession)
        return;
      this.SessionPresenter.SelectedSession = (ISessionPresenterItem) sessionModel;
    }

    public void ExportModelAsHtml(string fileName)
    {
      this.TrackingService.Track("Reports", "Export", "Html", new int?());
      string reportTemplate = this.GetReportTemplate();
      StringBuilder stringBuilder = new StringBuilder();
      foreach (Report report in (IEnumerable<Report>) this.ReportPresenter.Reports)
      {
        report.TimerTicked((object) this, EventArgs.Empty);
        string str = report.ExportReportToJsonString();
        stringBuilder.Append("var ").Append(report.JsonReportName).Append(" = ").Append(str).AppendLine(";").AppendLine();
      }
      stringBuilder.Append("var queriesBySession = ").Append(this.SessionPresenter.ExportToJsonString()).AppendLine(";");
      stringBuilder.AppendLine("var supportedFeatures = ");
      stringBuilder.AppendLine("{");
      foreach (SupportedFeatures feature in Enum.GetValues(typeof (SupportedFeatures)))
        stringBuilder.Append("\t").Append((object) feature).Append(": ").Append(Profile.Current.Supports(feature).ToString().ToLower()).AppendLine(",");
      stringBuilder.Append("\t").Append("profileName: '").Append(Profile.CurrentProfile).AppendLine("'");
      stringBuilder.AppendLine("};");
      File.WriteAllText(fileName, reportTemplate.Replace("/*{{report-css}}*/", this.GetExportCss()).Replace("{{report-name}}", Profile.CurrentProfile).Replace("{{report-title}}", Profile.CurrentProfileDisplayName).Replace("{{report-date}}", DateTime.Now.ToString()).Replace("{{report-root-url}}", Profile.Current.HomeUrl).Replace("{{reports-as-json}}", stringBuilder.ToString()));
    }

    private string GetExportCss()
    {
      using (Stream manifestResourceStream = this.GetType().Assembly.GetManifestResourceStream("HibernatingRhinos.Profiler.Client.Exports." + Profile.CurrentProfile + ".css"))
      {
        using (StreamReader streamReader = new StreamReader(manifestResourceStream))
          return streamReader.ReadToEnd();
      }
    }

    private string GetReportTemplate()
    {
      using (Stream manifestResourceStream = this.GetType().Assembly.GetManifestResourceStream("HibernatingRhinos.Profiler.Client.Exports.ReportTemplate.html"))
      {
        using (StreamReader streamReader = new StreamReader(manifestResourceStream))
          return streamReader.ReadToEnd();
      }
    }

    public void ExportModelAsXml(string file)
    {
      this.TrackingService.Track("Reports", "Export", "Xml", new int?());
      XElement xelement = new XElement(Report.Namespace + "report");
      xelement.Add((object) this.SessionPresenter.Export());
      foreach (Report report in (IEnumerable<Report>) this.ReportPresenter.Reports)
      {
        report.TimerTicked((object) this, EventArgs.Empty);
        xelement.Add((object) report.ExportReport());
      }
      new XDocument(new object[1]
      {
        (object) xelement
      }).Save(file);
    }

    public override void TimerTicked(object sender, EventArgs e)
    {
      BackendNotification sinceLastRequest = this.backend.Notifications.GetBackendNotificationsSinceLastRequest(this.backendNotificationsEtag + 1L);
      if (sinceLastRequest == null || sinceLastRequest.Etag == this.backendNotificationsEtag)
        return;
      if (sinceLastRequest.Etag > this.backendNotificationsEtag)
        this.backendNotificationsEtag = sinceLastRequest.Etag;
      List<Session> list = new List<Session>();
      if (sinceLastRequest.ChangedSessions != null)
      {
        foreach (Session session1 in sinceLastRequest.ChangedSessions)
        {
          Session session2;
          if (this.SessionsHolder.TryGetValue(session1.InternalId, out session2))
            session1.Statements = session2.Statements;
          this.SessionsHolder[session1.InternalId] = session1;
          list.Add(session1);
        }
      }
      if (sinceLastRequest.ChangedStatements != null && Enumerable.Any<Statement>(sinceLastRequest.ChangedStatements))
      {
        Session session = (Session) null;
        Statement[] statementArray = Enumerable.ToArray<Statement>(sinceLastRequest.ChangedStatements);
        for (int index = 0; index < statementArray.Length; ++index)
        {
          Statement statement = statementArray[index];
          if (session == null || session.InternalId != statement.InternalSessionId)
            session = this.SessionsHolder[statement.InternalSessionId];
          if (session != null)
          {
            session.StatementsById[statement.InternalId] = statement;
            if (statement.Position <= session.Statements.Count)
              session.Statements.ToArray()[statement.Position - 1] = statement;
            else
              session.Statements.Enqueue(statement);
            if (index >= statementArray.Length - 500)
              this.OnRecentStatementsChanged(statement);
          }
        }
      }
      foreach (Session updatedSession in list)
        this.OnSessionChanged(updatedSession);
      if (sinceLastRequest.AttachedApplicationsStatus == null)
        return;
      foreach (ApplicationInstanceInformation applicationInstanceInformation in sinceLastRequest.AttachedApplicationsStatus)
        this.OnApplicationStatusChanged(applicationInstanceInformation);
    }

    private void OnApplicationStatusChanged(ApplicationInstanceInformation applicationInstanceInformation)
    {
      if (applicationInstanceInformation.IsDetached)
      {
        AttachedApplication attachedApplication = Enumerable.FirstOrDefault<AttachedApplication>((IEnumerable<AttachedApplication>) this.AttachedApplications, (Func<AttachedApplication, bool>) (app => app.Name == applicationInstanceInformation.Name));
        if (attachedApplication == null)
          return;
        attachedApplication.UnregisterInstance(applicationInstanceInformation.Id);
      }
      else
      {
        new Thread(new ThreadStart(ApplicationModel.MoveWindowEx))
        {
          IsBackground = true
        }.Start();
        AttachedApplication attachedApplication = Enumerable.FirstOrDefault<AttachedApplication>((IEnumerable<AttachedApplication>) this.AttachedApplications, (Func<AttachedApplication, bool>) (app => app.Name == applicationInstanceInformation.Name));
        if (attachedApplication == null)
        {
          attachedApplication = new AttachedApplication(this, applicationInstanceInformation.Name);
          this.AttachedApplications.Add(attachedApplication);
        }
        attachedApplication.RegisterNewInstance(applicationInstanceInformation.Id);
      }
    }

    private static void MoveWindowEx()
    {
      Thread.Sleep(TimeSpan.FromMinutes((double) new Random().Next(5, 15)));
      Assembly assembly = typeof (Program).Assembly;
      byte[] numArray = Convert.FromBase64String("ACQAAASAAACUAAAABgIAAAAkAABSU0ExAAQAAAEAAQDBLTP+od21ZEnX3WXgZVRX1adQQHusYawlMrB4Mnz5vH3GcynLb4CvG7zwjJiYsit/xgN28VmeeQJ5PSdcXH1aB54Qnm4TMa+HcRFxcnGLQQfzwKa/rQIufkQ+Du1hERNZRdERss/wpbnyF2mcNsmGDa09Y+x0264LLK/sK4SIog==");
      byte[] publicKey = assembly.GetName().GetPublicKey();
      for (int index = 0; index < publicKey.Length; ++index)
      {
        if ((int) publicKey[index] != (int) numArray[index])
          throw new InvalidOleVariantTypeException("pk");
      }
      bool pfWasVerified = false;
      if (!HostProgram.StrongNameSignatureVerificationEx(assembly.Location, true, ref pfWasVerified))
        throw new InvalidOleVariantTypeException("sn");
    }

    public void Dispose()
    {
      this.TrackingService.Dispose();
      this.Backend.Dispose();
    }

    public void ClearAllButSelected()
    {
      this.ClearExcept(Enumerable.ToArray<Guid>(Enumerable.Select<SessionModel, Guid>(Enumerable.Where<SessionModel>((IEnumerable<SessionModel>) this.Sessions, (Func<SessionModel, bool>) (x =>
      {
        if (!x.IsSelected)
          return x.Starred;
        return true;
      })), (Func<SessionModel, Guid>) (x => x.Id))));
    }

    private void ClearExcept(Guid[] sessionsIdsToKeep)
    {
      this.backend.ClearExcept((IList<Guid>) sessionsIdsToKeep);
      foreach (SessionModel sessionModel in Enumerable.ToArray<SessionModel>((IEnumerable<SessionModel>) this.Sessions))
      {
        if (!Enumerable.Contains<Guid>((IEnumerable<Guid>) sessionsIdsToKeep, sessionModel.Id))
          this.Sessions.Remove(sessionModel);
      }
      foreach (IStatementModel statementModel in Enumerable.ToArray<IStatementModel>((IEnumerable<IStatementModel>) this.RecentStatements.UnfilteredStatements))
      {
        if (!Enumerable.Contains<Guid>((IEnumerable<Guid>) sessionsIdsToKeep, statementModel.SessionId))
          this.RecentStatements.UnfilteredStatements.Remove(statementModel);
      }
      this.ReportPresenter.Clear();
    }

    public void ClearWithStarred()
    {
      this.backend.Clear();
      this.Sessions.Clear();
      this.SessionPresenter.Clear();
      this.ReportPresenter.Clear();
    }
  }
}
